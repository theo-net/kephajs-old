KephaJS
=======

Fourni un framework javascript pour le navigateur et pour le serveur NodeJS.



Philosophie
-----------

Certains reprocheront plusieurs choses, contraires à la philosophie de nombreux
projets open source :

 - Une application pour une fonctionnalité, alors que nous réunissons au
   contraire de nombreux éléments en un seul projet afin d'avoir la plus grande
   cohérence possible. D'autant plus, que nous mettons dans un seul et même
   projet du code à la fois pour NodeJs et à la fois pour le navigateur, car
   pour nous, un des grands avantage de choisir Javascript est de pouvoir
   mutualiser le développement. Nous avons ainsi une grande cohérence de
   structure et de méthodes disponibles.
 - Ne pas réinventer la roue, nous aurions pu nous contenter d'assembler des
   éléments déjà disponible, mais dans un but d'apprentissage, parce que nous
   voulions comprendre comment ces briques sont réalisées, nous avons tout
   recodé ou repris des portions entières après les avoir décortiquées.



Structuration
-------------

Le framework est scindé en quatres parties :

 - core : toutes les bibliothèques communes aux différentes parties du framework.
 - server : ce qui concerne la partie serveur (application s'exécutant avec
    NodeJS et fournissant un service HTTP)
 - cli : ce qui concerne la ligne de commande (application s'exécutant avec
    NodeJS  mais étant lancée dans un terminal). L'ORM, par exemple, pouvant
    être utile à une commande ou au serveur, se retrouvera ici
 - browser : partie s'exécutant dans un navigateur internet



CLI
---

Pour cette partie, nous nous sommes appuyés très fortement sur quatre projets,
en reprenant leur code, pour les combiner entre eux :

  - `chalk 1.1.3` de Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)
    pour tout ce qui concerne l'affichage en couleur dans le terminal.
  - `cli-table2 0.2.0` de James Talmage <james.talmage@jrtechnical.com> pour le
    rendu de tableaux dans le terminal
  - `minimist 1.20` de James Halliday <mail@substack.net> pour la gestion des
    paramètres et options transmises à la commande
  - `Caporal.js 0.3.0` de Matthias Etienne <matthias@etienne.in> pour la
    génération d'application en ligne de commande avec la gestion des arguments
    et des options passés en paramètres et la génération d'aide personnalisée.

Ceci nous a permis de nous séparer d'éléments non utiles pour notre projet, mais
aussi de lier finement ces projets : ainsi, l'utilisateur peut ajouter un style
personnalisé à `Color` et celui-ci sera disponnible pour le rendu du tableau
avec `Table`. De même, `Caporal.js` et `minimist` ont certaines fonctionnalités
redondantes, on a pu ainsi les combiner.



Browser
-------

Nous nous sommes inspiré du projet AngularJS et notamment de l'ebook « Build
your own AngularJS » pour la partie navigateur. Ce qui pouvait aussi servir
pour les autres parties a été intégré au Core, les reste pour le Browser.

Tout ce système est aujourd'hui dépassé, mais nous trouvions que cela était une
bonne base pour nos applications.



Server
------

Nous avons implémenté, from scratch, un système permettant de servir du
contenu via le portocole HTTP, idéal pour des API REST et des sites web. Nous
avons choisis un modèle MVC (Modèle - Vue - Controller).



Licence
-------

Regardez le fichier [LICENCE](/LICENCE).

