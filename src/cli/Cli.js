'use strict' ;

/**
 * src/cli/Cli.js
 */

/**
 * Inspiré par Caporal.js Matthias ETIENNE <matthias@etienne.in>
 */

const path = require('path') ;
const Color       = require('./Color'),
      Table       = require('./Table/Table'),
      Application = require('../core/Application'),
      Command     = require('./Cli/Command'),
      Help        = require('./Cli/Help'),
      ParseArgs   = require('./Cli/ParseArgs'),
      K           = require('./Utils'),
      Logs        = require('../core/Services/Logs') ;


/**
 * Créé une application pour la ligne de commande avec la gestion
 *
 *    let cli = new Cli() ;
 *    cli
 *      .version('1.0.0')
 *      .command('hello', 'Affihce un Hello World!')
 *      .alias('salut')
 *      .argument('<nom>', 'Nom', 'string')
 *      .option('-e, --excla', 'Avec `!`')
 *      .action(function (args, options) {
 *        console.log('Hello ' + args.nom + (options.excla ? '!' : '.')) ;
 *      }) ;
 *    cli.run() ;
 *
 *    // nodejs monApp.js hello World -e
 *    // Hello World!
 *
 * NB: option('--no-color') sera récupérée dans options.color (avec false comme
 * valeur)
 */
class Cli extends Application {

  /**
   * Initialise la ligne de commande
   */
  constructor () {

    super() ;

    // Propriétés
    this._commands = [] ;
    this._defaultCommand = null ;

    // Accès au framework
    this.K = K ;

    // Défini des services
    this.kernel().container().set('$color', new Color()) ;
    this.kernel().container().set('$logs', new Logs()) ;

    // On récupère le nom de l'exécutable
    this.bin = path.basename(process.argv[1]) ;

    // Gestion de l'aide
    this._help = new Help(this) ;
    // Parser
    this.parseArgs = new ParseArgs() ;

    // On capture les signaux
    process.on('SIGINT', this._handleExit.bind(this)) ;
    process.on('SIGQUIT', this._handleExit.bind(this)) ;
    process.on('SIGTERM', this._handleExit.bind(this)) ;
  }


  /**
   * Renvoit le service color
   * @returns {Color}
   */
  color () {

    return this.kernel().get('$color') ;
  }


  /**
   * Renvoit un nouveau Table
   *
   * @param {Array} [options] Options du tableau
   * @param {String} [style] Fait appel directement à un style (`noBorder`)
   * @return {Table}
   */
  newTable (options, style = '') {

    if (style == 'noBorder')
      options = Table.getOptionsNoBorder(options) ;

    return new Table(options) ;
  }


  /**
   * Définit ou donne la description de notre application
   *
   * @param {String} [description] Description de l'applciation
   * @returns {*} La description, si aucun paramètre, `this` sinon
   */
  description (description) {

    if (description) {
      this._description = description ;
      return this ;
    }
    else
      return this._description ;
  }


  /**
   * Créé une nouvelle commande et la retourne
   *
   * @param {String} synopsis Synopsis de la commande
   * @param {String} description Description de la commande
   * @returns {Command}
   */
  command (synopsis, description) {

    const cmd = new Command(synopsis, description, this) ;
    this._commands.push(cmd) ;
    return cmd ;
  }


  /**
   * Définie une action pour le programme. Il s'agira de l'action par défaut si
   * aucune commande spécifique est appelée.
   *
   * @param {Function} action Action à exécuter
   * @returns {Program}
   */
  action (action) {

    if (!this._defaultCommand)
      this._defaultCommand = this.command('_default', 'Commande par défaut') ;

    this._defaultCommand.action(action) ;

    return this ;
  }


  /**
   * Lance l'application en parsant les arguments de la ligne de commande.
   * Pour accéder à l'aide, plusieurs manières :
   *   - aide spécifique à une commande
   *      prog help <nomCommand>
   *   - aide générale
   *      prog help
   *      prog --help
   *      prog -h
   *      ou si la commande demandée n'existe pas
   *
   * @param {Array} [argv] Arguments à traiter, prendra ceux de la ligne de
   *                       commande si le paramètre n'est pas défini.
   * @returns {*}
   */
  run (argv = undefined) {

    // Parse les arguments de la ligne de commande
    if (argv === undefined)
      argv = process.argv.slice(2) ;

    const args = this.parseArgs.parse(argv) ;
    let options = Object.assign({}, args) ;
    delete options._ ;

    // On désactive les couleurs
    if (options.color === false)
      this.color().enableColor(false) ;


    // L'utilisateur demande de l'aide, on lui envoit l'aide de la commande
    if (args._[0] === 'help')
      return this.help(args._.slice(1).join(' ')) ;

    // Sinon on part à la recherche de la commande
    let argsCopy = args._.slice() ;
    const commandArr = [] ;
    let cmd ;

    while(!cmd && argsCopy.length) {

      commandArr.push(argsCopy.shift()) ;
      const cmdStr = commandArr.join(' ') ;
      cmd = this._commands.filter(c => {
        return (c.name() === cmdStr || c.getAlias() === cmdStr) ;
      })[0] ;
    }

    // L'utilisateur demande la version
    if (options.V || options.version)
      return console.log(this.version()) ;

    // S'il n'y a pas de commande trouvée, on prend celle par défaut
    if (!cmd && this._defaultCommand) {
      cmd = this._defaultCommand ;
      argsCopy = args._.slice() ;
    }

    // Pas de commande trouvée ou aide demandée : on envoit l'aide par défaut
    if (!cmd || options.help || options.h)
      return this.help() ;


    // If quiet, only output warning & errors
    if (options.quiet)
      this.kernel().get('$config').set('logLevel', 'warn', true) ;
    // verbose mode
    else if (options.v || options.verbose)
      this.kernel().get('$config').set('logLevel', 'debug', true) ;

    // On vérifie les arguments et les options
    let validated ;
    try {
      validated = cmd._validateCall(argsCopy, options) ;
    } catch(err) {
      return this.fatalError(err) ;
    }

    return cmd._run(validated.args, validated.options) ;
  }


  /**
   * Affiche l'aide de la commande demandée
   *
   * @param {String} cmdStr Nom de la commande dont on cherche l'aide (si aucun
   *                        nom ne correspond, affichera l'aide par défaut
   */
  help (cmdStr) {

    const cmd = this._commands.filter(
        c => (c.name() === cmdStr || c.getAlias() === cmdStr)
    )[0] ;

    // On affiche l'aide
    console.log(this._help.display(cmd)) ;
  }


  /**
   * Renvoit une erreur fatale
   * @param {Object} errObj Erreur à transmettre
   */
  fatalError (errObj) {

    console.log('\n' + errObj.message) ;
    process.exit(2) ;
  }


  /**
   * Capture les signaux pour quitter correctement l'application
   * @param {Integer} signal Signal capturé
   */
  _handleExit (signal) {

    console.log('\nReceived ' + signal + '. Close the application') ;
    process.exit(0) ;
  }
}

module.exports = Cli ;

