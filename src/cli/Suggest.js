'use strict' ;

/**
 * src/cli/Suggest.js
 */

const levenshtein = require('../core/Utils').levenshtein ;
const Color = require('./Color') ;
const color = new Color() ;

/**
 * À partir  d'une liste de possibilités, suggère les éléments correspondant
 * à l'`input` (avec une distance de Levenshtein <= 2).
 * @param {String} input Entrée pour laquelle on cherche des suggestions
 * @param {String[]} possibilities  Possibilités
 * @returns {Array}
 */
exports.getSuggestions = function getSuggestions (input, possibilities) {

  return possibilities
    .map(p => {
      return {suggestion: p,  distance: levenshtein(input, p)} ;
    })
    .filter(p => p.distance <= 2)
    .sort((a, b) => a.distance - b.distance)
    .map(p => p.suggestion) ;
} ;


/**
 * Returne la différence, entre deux chaînes de caractères, en gras.
 *
 * @param {String} from Chaîne d'origine
 * @param {String} to Chaîne de destination
 * @returns {String}
 */
exports.getBoldDiffString = (from, to) => {

  return to.split('').map((chr, index) => {

    if (chr != from.charAt(index))
      return color.bold(chr) ;
    return chr ;
  }).join('') ;
} ;

