'use strict' ;

/*
 * src/cli/Error/BaseError.js
 */

const CoreBaseError = require('../../core/Error/BaseError') ;

class BaseError extends CoreBaseError {

  constructor (message, meta, cli) {

    const color = cli.color() ;
    const spaces = '   ' ;
    const msg = spaces + color.red('Error: ' + message) + '\n' + spaces
              + 'Type ' + color.bold(cli.bin + ' --help') + ' for help.\n' ;

    super(msg, meta, cli, message) ;
  }


  /**
   * Retourne le nom de l'option avec '-' ou '--' selon qu'il s'agit d'une
   * option courte ou longue.
   * @param {String} option Nom de l'option
   * @returns {String}
   * @static
   */
  static getDashedOption (option) {

    if (option.length === 1)
      return '-' + option ;
    return '--' + option ;
  }
}

module.exports = BaseError ;

