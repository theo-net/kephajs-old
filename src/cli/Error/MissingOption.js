'use strict' ;

/**
 * src/cli/Error/MissingOption.js
 */

const BaseError = require('./BaseError') ;

class MissingOptionError extends BaseError {

  constructor (option, command, cli) {

    let msg = 'Missing option '
            + cli.color().italic(BaseError.getDashedOption(option))
            + '.' ;

    super(msg, {option, command}, cli) ;
  }
}

module.exports = MissingOptionError ;

