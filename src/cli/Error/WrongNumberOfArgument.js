'use strict' ;

/**
 * src/cli/Error/WrongNumberOfArgument.js
 */

const BaseError = require('./BaseError') ;

class WrongNumberOfArgumentError extends BaseError {}

module.exports = WrongNumberOfArgumentError ;

