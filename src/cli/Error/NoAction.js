'use strict' ;

/**
 * src/cli/Error/NoAction.js
 */

const BaseError = require('./BaseError') ;

class NoActionError extends BaseError {}

module.exports = NoActionError ;

