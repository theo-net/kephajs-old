'use strict' ;

/**
 * src/cli/Table/Table.test.js
 */

const expect = require('chai').expect ;

const Table = require('./Table'),
      Color = require('../Color') ;

let color = new Color() ;


// buddy ignore:start
describe('Table', function () {

  it('étend un Array', function () {

    expect(Object.getPrototypeOf(Table)).to.equal(Array) ;
  }) ;

  it('wordWrap with colored text', function () {

    let table = new Table({
      style: {border: [], head: []},
      wordWrap: true,
      colWidths: [7, 9]
    }) ;

    table.push([
      color.red('Hello how are you?'), color.blue('I am fine thanks!')
    ]) ;

    let expected = [
      '┌───────┬─────────┐',
      '│ ' + color.red('Hello') + ' │ ' + color.blue('I am') + '    │',
      '│ ' + color.red('how') + '   │ ' + color.blue('fine') + '    │',
      '│ ' + color.red('are') + '   │ ' + color.blue('thanks!') + ' │',
      '│ ' + color.red('you?') + '  │         │',
      '└───────┴─────────┘'
    ] ;

    expect(table.toString()).to.equal(expected.join('\n')) ;
  }) ;

  it('allows numbers as `content` property of cells defined using object '
      + 'notation', function () {

    let table = new Table({
      style: {border: [], head: []}
    }) ;

    table.push([{content: 12}]) ;

    let expected = [
      '┌────┐',
      '│ 12 │',
      '└────┘'
    ] ;

    expect(table.toString()).to.equal(expected.join('\n')) ;
  }) ;

  it('throws if content is not a string or number', function () {

    let table = new Table({
      style: {border: [], head: []}}) ;

    expect(function () {
      table.push([{content: {a: 'b'}}]) ;
      table.toString() ;
    }).to.throw() ;

  }) ;

  it('works with CJK values', function () {

    let table = new Table({
      style: {border: [], head: []},
      colWidths: [5, 10, 5]
    }) ;

    table.push(
      ['foobar', 'English test', 'baz'],
      ['foobar', '中文测试', 'baz'],
      ['foobar', '日本語テスト', 'baz'],
      ['foobar', '한국어테스트', 'baz']
    ) ;

    let expected = [
      '┌─────┬──────────┬─────┐',
      '│ fo… │ English… │ baz │',
      '├─────┼──────────┼─────┤',
      '│ fo… │ 中文测试 │ baz │',
      '├─────┼──────────┼─────┤',
      '│ fo… │ 日本語…  │ baz │',
      '├─────┼──────────┼─────┤',
      '│ fo… │ 한국어…  │ baz │',
      '└─────┴──────────┴─────┘'
    ] ;

    expect(table.toString()).to.equal(expected.join('\n')) ;
  }) ;

  describe('@api original-cli-table index tests', function () {

    it('test complete table', function () {

      let table = new Table({
        head: ['Rel', 'Change', 'By', 'When'],
        style: {
          'padding-left': 1,
          'padding-right': 1,
          head: [],
          border: [],
        },
        colWidths: [6, 21, 25, 17]
      }) ;

      table.push(
        ['v0.1', 'Testing something cool',
          'rauchg@gmail.com', '7 minutes ago'],
        ['v0.1', 'Testing something cool',
          'rauchg@gmail.com', '8 minutes ago']
      ) ;

      /* eslint-disable */
      let expected = [
 '┌──────┬─────────────────────┬─────────────────────────┬─────────────────┐',
 '│ Rel  │ Change              │ By                      │ When            │',
 '├──────┼─────────────────────┼─────────────────────────┼─────────────────┤',
 '│ v0.1 │ Testing something … │ rauchg@gmail.com        │ 7 minutes ago   │',
 '├──────┼─────────────────────┼─────────────────────────┼─────────────────┤',
 '│ v0.1 │ Testing something … │ rauchg@gmail.com        │ 8 minutes ago   │',
 '└──────┴─────────────────────┴─────────────────────────┴─────────────────┘'
      ] ;
      /* eslint-enable */

      expect(table.toString()).to.equal(expected.join('\n')) ;
    }) ;

    it('test width property', function () {

      let table = new Table({
        head: ['Cool'],
        style: {
          head: [],
          border: []
        }
      }) ;

      expect(table.width).to.equal(8) ;
    }) ;

    it('test vertical table output', function () {

      let table = new Table({
        style: {'padding-left': 0, 'padding-right': 0, head: [], border: []}
      }) ; // clear styles to prevent color output

      table.push(
        {'v0.1': 'Testing something cool'},
        {'v0.1': 'Testing something cool'}
      ) ;

      let expected = [
        '┌────┬──────────────────────┐',
        '│v0.1│Testing something cool│',
        '├────┼──────────────────────┤',
        '│v0.1│Testing something cool│',
        '└────┴──────────────────────┘'
      ] ;

      expect(table.toString()).to.equal(expected.join('\n')) ;
    }) ;

    it('test cross table output', function () {

      let table = new Table({
        head: ['', 'Header 1', 'Header 2'],
        style: {'padding-left': 0, 'padding-right': 0, head: [], border: []}
      }) ;

      table.push(
        {'Header 3': ['v0.1', 'Testing something cool'] },
        {'Header 4': ['v0.1', 'Testing something cool'] }
      ) ;

      let expected = [
        '┌────────┬────────┬──────────────────────┐',
        '│        │Header 1│Header 2              │',
        '├────────┼────────┼──────────────────────┤',
        '│Header 3│v0.1    │Testing something cool│',
        '├────────┼────────┼──────────────────────┤',
        '│Header 4│v0.1    │Testing something cool│',
        '└────────┴────────┴──────────────────────┘'
      ] ;

      expect(table.toString()).to.equal(expected.join('\n')) ;
    }) ;

    it('test table colors', function () {

      let table = new Table({
        head: ['Rel', 'By'],
        style: {head: ['red'], border: ['grey']}
      }) ;

      let off = '\u001b[39m',
          red = '\u001b[31m',
          orange = '\u001b[38;5;221m',
          grey = '\u001b[90m',
          c256s = orange + 'v0.1' + off ;

      table.push(
        [c256s, 'rauchg@gmail.com']
      ) ;

      let expected = [
        grey + '┌──────' + off + grey + '┬──────────────────┐' + off,
        grey + '│' + off + red + ' Rel  ' + off + grey + '│' + off + red
          + ' By               ' + off + grey + '│' + off,
        grey + '├──────' + off + grey + '┼──────────────────┤' + off,
        grey + '│' + off + ' ' + c256s + ' ' + grey + '│' + off
          + ' rauchg@gmail.com ' + grey + '│' + off,
        grey + '└──────' + off + grey + '┴──────────────────┘' + off
      ] ;

      expect(table.toString()).to.equal(expected.join('\n')) ;
    }) ;

    it('test custom chars', function () {

      let table = new Table({
        chars: {
          'top': '═',
          'top-mid': '╤',
          'top-left': '╔',
          'top-right': '╗',
          'bottom': '═',
          'bottom-mid': '╧',
          'bottom-left': '╚',
          'bottom-right': '╝',
          'left': '║',
          'left-mid': '╟',
          'right': '║',
          'right-mid': '╢',
        },
        style: {
          head: [],
          border: []
        }
      }) ;

      table.push(
        ['foo', 'bar', 'baz'],
        ['frob', 'bar', 'quuz']
      ) ;

      let expected = [
        '╔══════╤═════╤══════╗',
        '║ foo  │ bar │ baz  ║',
        '╟──────┼─────┼──────╢',
        '║ frob │ bar │ quuz ║',
        '╚══════╧═════╧══════╝'
      ] ;

      expect(table.toString()).to.equal(expected.join('\n')) ;
    }) ;

    it('test compact shortand', function () {

      let table = new Table({
        style: {
          head: [],
          border: [],
          compact: true
        }
      }) ;

      table.push(
        ['foo', 'bar', 'baz'],
        ['frob', 'bar', 'quuz']
      ) ;

      let expected = [
        '┌──────┬─────┬──────┐',
        '│ foo  │ bar │ baz  │',
        '│ frob │ bar │ quuz │',
        '└──────┴─────┴──────┘'
      ] ;

      expect(table.toString()).to.equal(expected.join('\n')) ;
    }) ;

    it('test compact empty mid line', function () {

      let table = new Table({
        chars: {
          'mid': '',
          'left-mid': '',
          'mid-mid': '',
          'right-mid': ''
        },
        style: {
          head: [],
          border: []
        }
      }) ;

      table.push(
        ['foo', 'bar', 'baz'],
        ['frob', 'bar', 'quuz']
      ) ;

      let expected = [
        '┌──────┬─────┬──────┐',
        '│ foo  │ bar │ baz  │',
        '│ frob │ bar │ quuz │',
        '└──────┴─────┴──────┘'
      ] ;

      expect(table.toString()).to.equal(expected.join('\n')) ;
    }) ;

    it('test decoration lines disabled', function () {

      let table = new Table({
        chars: {
          'top': '',
          'top-mid': '',
          'top-left': '',
          'top-right': '',
          'bottom': '',
          'bottom-mid': '',
          'bottom-left': '',
          'bottom-right': '',
          'left': '',
          'left-mid': '',
          'mid': '',
          'mid-mid': '',
          'right': '',
          'right-mid': '',
          'middle': ' ' // a single space
        },
        style: {
          head: [],
          border: [],
          'padding-left': 0,
          'padding-right': 0
        }
      }) ;

      table.push(
        ['foo', 'bar', 'baz'],
        ['frobnicate', 'bar', 'quuz']
      ) ;

      let expected = [
        'foo        bar baz ',
        'frobnicate bar quuz'
      ] ;

      expect(table.toString()).to.equal(expected.join('\n')) ;
    }) ;

    it('test with null/undefined as values or column names', function () {

      let table = new Table({
        style: {
          head: [],
          border: []
        }
      }) ;

      table.push(
        [null, undefined, 0]
      ) ;

      let expected = [
        '┌──┬──┬───┐',
        '│  │  │ 0 │',
        '└──┴──┴───┘'
      ] ;

      expect(table.toString()).to.equal(expected.join('\n')) ;
    }) ;
  }) ;

  describe('@api original-cli-table newline tests', function () {

    it('test table with newlines in headers', function () {

      let table = new Table({
        head: ['Test', '1\n2\n3'],
        style: {
          'padding-left': 1,
          'padding-right': 1,
          head: [],
          border: []
        }
      }) ;

      let expected = [
        '┌──────┬───┐',
        '│ Test │ 1 │',
        '│      │ 2 │',
        '│      │ 3 │',
        '└──────┴───┘'
      ] ;

      expect(table.toString()).to.equal(expected.join('\n')) ;
    }) ;

    it('test column width is accurately reflected when newlines are present',
    function () {

      let table = new Table({
        head: ['Test\nWidth'],
        style: {head: [], border: []}
      }) ;
      expect(table.width).to.equal(9) ;
    }) ;

    it('test newlines in body cells', function () {

      let table = new Table({style: {head: [], border: []}}) ;

      table.push(['something\nwith\nnewlines']) ;

      let expected = [
        '┌───────────┐',
        '│ something │',
        '│ with      │',
        '│ newlines  │',
        '└───────────┘'
      ] ;

      expect(table.toString()).to.equal(expected.join('\n')) ;
    }) ;

    it('test newlines in vertical cell header and body', function () {

      let table = new Table({
        style: {
          'padding-left': 0,
          'padding-right': 0,
          head: [],
          border: []
        }
      }) ;

      table.push(
        {'v\n0.1': 'Testing\nsomething cool'}
      ) ;

      let expected = [
        '┌───┬──────────────┐',
        '│v  │Testing       │',
        '│0.1│something cool│',
        '└───┴──────────────┘'
      ] ;

      expect(table.toString()).to.equal(expected.join('\n')) ;
    }) ;

    it('test newlines in cross table header and body', function () {

      let table = new Table({
        head: ['', 'Header\n1'],
        style: {'padding-left': 0, 'padding-right': 0, head: [], border: []}
      }) ;

      table.push({ 'Header\n2': ['Testing\nsomething\ncool'] }) ;

      let expected = [
        '┌──────┬─────────┐',
        '│      │Header   │',
        '│      │1        │',
        '├──────┼─────────┤',
        '│Header│Testing  │',
        '│2     │something│',
        '│      │cool     │',
        '└──────┴─────────┘'
      ] ;

      expect(table.toString()).to.equal(expected.join('\n')) ;
    }) ;
  }) ;


  describe('strlen', function () {

    it('length of "hello" is 5', function () {

      expect(Table.strlen('hello')).to.equal(5) ;
    }) ;

    it('length of "hi" is 2', function () {

      expect(Table.strlen('hi')).to.equal(2) ;
    }) ;

    it('length of "hello" in red is 5', function () {

      expect(Table.strlen(color.red('hello'))).to.equal(5) ;
    }) ;

    it('length of "hello" underlined is 5', function () {

      expect(Table.strlen(color.underline('hello'))).to.equal(5) ;
    }) ;

    it('length of "hello\\nhi\\nheynow" is 6', function () {

      expect(Table.strlen('hello\nhi\nheynow')).to.equal(6) ;
    }) ;

    it('length of "中文字符" is 8', function () {

      expect(Table.strlen('中文字符')).to.equal(8) ;
    }) ;

    it('length of "日本語の文字" is 12', function () {

      expect(Table.strlen('日本語の文字')).to.equal(12) ;
    }) ;

    it('length of "한글" is 4', function () {

      expect(Table.strlen('한글')).to.equal(4) ;
    }) ;
  }) ;

  describe('truncate', function () {

    it('truncate("hello", 5) === "hello"', function () {

      expect(Table.truncate('hello',5)).to.equal('hello') ;
    }) ;

    it('truncate("hello sir", 7, "…") == "hello …"', function () {

      expect(Table.truncate('hello sir', 7, '…')).to.equal('hello …') ;
    }) ;

    it('truncate("hello sir", 6, "…") == "hello…"', function () {

      expect(Table.truncate('hello sir', 6, '…')).to.equal('hello…') ;
    }) ;

    it('truncate("goodnight moon", 8, "…") == "goodnig…"', function () {

      expect(Table.truncate('goodnight moon', 8, '…')).to.equal('goodnig…') ;
    }) ;

    it('truncate(color.red("goodnight moon"), 15, "…") == '
        + 'color.red("goodnight moon")', function () {

      let original = color.red('goodnight moon') ;
      expect(Table.truncate(original, 15, '…')).to.equal(original) ;
    }) ;

    it('truncate(color.red("goodnight moon"), 8, "…") == '
        + 'color.red("goodnig") + "…"', function () {

      let original = color.red('goodnight moon') ;
      let expected = color.red('goodnig') + '…' ;
      expect(Table.truncate(original, 8, '…')).to.equal(expected) ;
    }) ;

    it('truncate(color.red("goodnight moon"), 9, "…") == '
        + 'color.red("goodnig") + "…"', function () {

      let original = color.red('goodnight moon') ;
      let expected = color.red('goodnigh') + '…' ;
      expect(Table.truncate(original, 9, '…')).to.equal(expected) ;
    }) ;

    it('red(hello) + green(world) truncated to 9 chars', function () {

      let original = color.red('hello') + color.green(' world') ;
      let expected = color.red('hello') + color.green(' wo') + '…' ;
      expect(Table.truncate(original, 9)).to.equal(expected) ;
    }) ;

    it('red-on-green(hello) + green-on-red(world) truncated to 9 chars',
    function () {

      let original = color.red.bgGreen('hello') + color.green.bgRed(' world') ;
      let expected = color.red.bgGreen('hello')
        + color.green.bgRed(' wo') + '…' ;
      expect(Table.truncate(original,9)).to.equal(expected) ;
    }) ;

    it('red-on-green(hello) + green-on-red(world) truncated to 10 chars '
        + '- using inverse', function () {

      let original = color.red.bgGreen('hello' + color.inverse(' world')) ;
      let expected = color.red.bgGreen('hello' + color.inverse(' wor')) + '…' ;
      expect(Table.truncate(original,10)).to.equal(expected) ;
    }) ;

    it('red-on-green( italic(hello world) ) truncated to 11 chars',
    function () {

      let original = color.red.bgGreen(color.italic('hello world')) ;
      let expected = color.red.bgGreen(color.italic('hello world')) ;
      expect(Table.truncate(original,11)).to.equal(expected) ;
    }) ;

    it('red-on-green( italic (hello world) ) truncated to 10 chars ',
    function () {

      let original = color.red.bgGreen(color.italic('hello world')) ;
      let expected = color.red.bgGreen(color.italic('hello wor')) + '…' ;
      expect(Table.truncate(original,10)).to.equal(expected) ;
    }) ;

    it('handles reset code', function () {

      let original = '\x1b[31mhello\x1b[0m world' ;
      let expected = '\x1b[31mhello\x1b[0m wor…' ;
      expect(Table.truncate(original,10)).to.equal(expected) ;
    }) ;

    it('handles reset code (EMPTY VERSION)', function () {

      let original = '\x1b[31mhello\x1b[0m world' ;
      let expected = '\x1b[31mhello\x1b[0m wor…' ;
      expect(Table.truncate(original,10)).to.equal(expected) ;
    }) ;

    it('truncateWidth("漢字テスト", 15) === "漢字テスト"', function () {

      expect(Table.truncate('漢字テスト',15)).to.equal('漢字テスト') ;
    }) ;

    it('truncateWidth("漢字テスト", 6) === "漢字…"', function () {

      expect(Table.truncate('漢字テスト',6)).to.equal('漢字…') ;
    }) ;

    it('truncateWidth("漢字テスト", 5) === "漢字…"', function () {

      expect(Table.truncate('漢字テスト',5)).to.equal('漢字…') ;
    }) ;

    it('truncateWidth("漢字testてすと", 12) === "漢字testて…"', function () {

      expect(Table.truncate('漢字testてすと',12)).to.equal('漢字testて…') ;
    }) ;

    it('handles color code with CJK chars', function () {

      let original = '漢字\x1b[31m漢字\x1b[0m漢字' ;
      let expected = '漢字\x1b[31m漢字\x1b[0m漢…' ;
      expect(Table.truncate(original,11)).to.equal(expected) ;
    }) ;
  }) ;

  describe('_mergeOptions', function () {

    function defaultOptions () {

      return {
        chars: {
          'top': '─',
          'top-mid': '┬',
          'top-left': '┌',
          'top-right': '┐',
          'bottom': '─',
          'bottom-mid': '┴',
          'bottom-left': '└',
          'bottom-right': '┘',
          'left': '│',
          'left-mid': '├',
          'mid': '─',
          'mid-mid': '┼',
          'right': '│',
          'right-mid': '┤',
          'middle': '│'
        },
        truncate: '…',
        colWidths: [],
        rowHeights: [],
        colAligns: [],
        rowAligns: [],
        style: {
          'padding-left': 1,
          'padding-right': 1,
          head: ['red'],
          border: ['grey'],
          compact: false
        },
        head: []
      } ;
    }

    it('allows you to override chars', function () {

      expect(Table._mergeOptions()).to.eql(defaultOptions()) ;
    }) ;

    it('chars will be merged deeply', function () {

      let expected = defaultOptions() ;
      expected.chars.left = 'L' ;
      expect(Table._mergeOptions({chars: {left: 'L'}})).to.eql(expected) ;
    }) ;

    it('style will be merged deeply', function () {

      let expected = defaultOptions() ;
      expected.style['padding-left'] = 2 ;
      expect(Table._mergeOptions({style: {'padding-left': 2}}))
        .to.eql(expected) ;
    }) ;

    it('head will be overwritten', function () {

      let expected = defaultOptions() ;
      expected.style.head = [] ;
      expect(Table._mergeOptions({style: {'head': []}})).to.eql(expected) ;
    }) ;

    it('border will be overwritten', function () {

      let expected = defaultOptions() ;
      expected.style.border = [] ;
      expect(Table._mergeOptions({style: {'border': []}})).to.eql(expected) ;
    }) ;
  }) ;

  describe('wordWrap', function () {

    it('length',function () {

      let input = 'Hello, how are you today? I am fine, thank you!' ;
      let expected = 'Hello, how\nare you\ntoday? I\nam fine,\nthank you!' ;

      expect(Table.wordWrap(10, input).join('\n')).to.equal(expected) ;
    }) ;

    it('length with colors', function () {

      let input = color.red('Hello, how are')
                + color.blue(' you today? I')
                + color.green(' am fine, thank you!') ;

      let expected = color.red('Hello, how\nare')
                   + color.blue(' you\ntoday? I')
                   + color.green('\nam fine,\nthank you!') ;

      expect(Table.wordWrap(10, input).join('\n')).to.equal(expected) ;
    }) ;

    it('will not create an empty last line', function () {

      let input = 'Hello Hello ' ;
      let expected = 'Hello\nHello' ;

      expect(Table.wordWrap(5, input).join('\n')).to.equal(expected) ;
    }) ;

    it('will handle color reset code', function () {

      let input = '\x1b[31mHello\x1b[0m Hello ' ;
      let expected = '\x1b[31mHello\x1b[0m\nHello' ;

      expect(Table.wordWrap(5, input).join('\n')).to.equal(expected) ;
    }) ;

    it('will handle color reset code (EMPTY version)', function () {

      let input = '\x1b[31mHello\x1b[m Hello ' ;
      let expected = '\x1b[31mHello\x1b[m\nHello' ;

      expect(Table.wordWrap(5, input).join('\n')).to.equal(expected) ;
    }) ;

    it('words longer than limit will not create extra newlines', function () {

      let input = 'disestablishment is a multiplicity someotherlongword' ;
      let expected =
        'disestablishment\nis a\nmultiplicity\nsomeotherlongword' ;

      expect(Table.wordWrap(7,input).join('\n')).to.equal(expected) ;
    }) ;

    it('multiple line input', function () {

      let input = 'a\nb\nc d e d b duck\nm\nn\nr' ;
      let expected = ['a', 'b', 'c d', 'e d', 'b', 'duck', 'm', 'n', 'r'] ;

      expect(Table.wordWrap(4,input)).to.eql(expected) ;
    }) ;

    it('will not start a line with whitespace', function () {

      let input = 'ab cd  ef gh  ij kl' ;
      let expected = ['ab cd','ef gh', 'ij kl'] ;
      expect(Table.wordWrap(7, input)).to.eql(expected) ;
    }) ;

    it('wraps CJK chars', function () {

      let input = '漢字 漢\n字 漢字' ;
      let expected = ['漢字 漢','字 漢字'] ;
      expect(Table.wordWrap(7, input)).to.eql(expected) ;
    }) ;

    it('wraps CJK chars with colors', function () {

      let input = '\x1b[31m漢字\x1b[0m\n 漢字' ;
      let expected = ['\x1b[31m漢字\x1b[0m', ' 漢字'] ;
      expect(Table.wordWrap(5, input)).to.eql(expected) ;
    }) ;
  }) ;

  describe('getOptionsNoBorder', function () {

    it('Définit des styles, sans bordures', function () {

      let noBorder = Table.getOptionsNoBorder() ;
      let expected = {
        chars: {
          bottom: '', 'bottom-left': '', 'bottom-mid': '', 'bottom-right': '',
          left: '', 'left-mid': '', mid: '', 'mid-mid': '', middle: '',
          right: '', 'right-mid': '',
          top: '', 'top-left': '', 'top-mid': '', 'top-right': ''
        },
        colAligns: [], colWidths: [], head: [], rowAligns: [], rowHeights: [],
        style: {
          border: ['grey'],
          compact: false,
          head: ['red'],
          'padding-left': 1,
          'padding-right': 1
        },
        truncate: '…'
      } ;

      expect(noBorder).to.be.deep.equal(expected) ;
    }) ;

    it('Applique les options passées en paramètres', function () {

      let noBorder = Table.getOptionsNoBorder({
        chars: {bottom: 'b'},
        style: {compact: true},
        truncate: '!'
      }) ;
      let expected = {
        chars: {
          bottom: 'b', 'bottom-left': '', 'bottom-mid': '', 'bottom-right': '',
          left: '', 'left-mid': '', mid: '', 'mid-mid': '', middle: '',
          right: '', 'right-mid': '',
          top: '', 'top-left': '', 'top-mid': '', 'top-right': ''
        },
        colAligns: [], colWidths: [], head: [], rowAligns: [], rowHeights: [],
        style: {
          border: ['grey'],
          compact: true,
          head: ['red'],
          'padding-left': 1,
          'padding-right': 1
        },
        truncate: '!'
      } ;

      expect(noBorder).to.be.deep.equal(expected) ;
    }) ;
  }) ;
}) ;
// buddy ignore:end

