'use strict' ;

/**
 * src/cli/Table/ColSpanCell.js
 */


/**
 * Cellule ne faisaint rien à part tracer des lignes vides. Sert pour le
 * spanning des colonnes
 */
class ColSpanCell {

  /**
   * Trace l'espace : renvoit donc une chaîne vide.
   * @returns {String}
   */
  draw () {

    return '' ;
  }

  /**
   * Initialise la cellule
   */
  init () {}

  /**
   * Prépare les options. Ne fait rien
   */
  mergeTableOptions () {}
}

module.exports = ColSpanCell ;

