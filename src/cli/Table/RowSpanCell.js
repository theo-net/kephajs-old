'use strict' ;

/**
 * src/cli/Table/RowSpanCell.js
 */


/**
 * Gère les espacement des cellules avec les bordures supérieures et inférieures
 * en ajoutant des lignes vides. Délègue le rendu à la cellule original, mais
 * celle classe ajoute le bon offset.
 */
class RowSpanCell {

  /**
   * Renseigne la cellule originale
   * @param {Cell} originalCell Cellule originale
   * @constructor
   */
  constructor (originalCell) {

    this.originalCell = originalCell ;
  }

  /**
   * Initialise l'espacement à partir des informations du tableau
   * @param {Object} tableOptions Options du tableau
   */
  init (tableOptions) {

    let originalY = this.originalCell.y ;
    this.cellOffset = this.y - originalY ;

    this.offset = tableOptions.rowHeights[originalY] ;
    for (let i = 1 ; i < this.cellOffset ; i++)
      this.offset += 1 + tableOptions.rowHeights[originalY + i] ;
  }

  /**
   * Dessine le spanning
   * @param {Number} lineNum Numéro de la ligne
   * @returns {String}
   */
  draw (lineNum) {

    if (lineNum == 'top')
      return this.originalCell.draw(this.offset, this.cellOffset) ;

    if (lineNum == 'bottom')
      return this.originalCell.draw('bottom') ;

    return this.originalCell.draw(this.offset + 1 + lineNum) ;
  }

  /**
   * Prépare les options. Ne fait rien
   */
  mergeTableOptions () {}
}

module.exports = RowSpanCell ;

