'use strict' ;

/**
 * src/cli/Utils
 */

const fs    = require('fs'),
      npath = require('path') ;

const Framework    = require('../core/Framework') ;


/**
 * Outils pour la partie CLI du framework
 */
class Utils extends Framework {


  /**
   * Test l'existence d'un fichier ou d'un répertoire
   *
   * @param {String} path Fichier/dossier à tester
   * @returns {Boolean}
   */
  static fileExists (path) {

    try {
      fs.statSync(path) ;
      return true ;
    }
    catch (e) {
      return false ;
    }
  }


  /**
   * Copie un fichier. S'il s'agit d'un répertoire, la copie sera récursive
   * @param {String} source Fichier à copier
   * @param {String} dest   Emplacement de destination
   * @param {Function} callback Fonction appelée après la copie `(err){}`
   */
  static cpr (source, dest, callback) {

    if (callback === undefined)
      callback = () => {} ;

    fs.stat(source, (err, stat) => {

      if (err) throw new Error(err) ;

      if (stat.isFile()) {

        let write = fs.createWriteStream(dest)
          .on('error', (err) => { if (err) throw err ; })
          .on('close', callback) ;
        fs.createReadStream(source)
          .on('error', (err) => { if (err) throw err ; })
          .pipe(write) ;
      }
      else if (stat.isDirectory()) {

        fs.readdir(source, (err, files) => {

          if (err) throw new Error(err) ;

          let n = files.length ;

          fs.mkdir(dest, (err) => {

            if (err) throw new Error(err) ;

            if (n === 0) callback() ;

            else {
              files.forEach(file => {
                this.cpr(npath.join(source, file),
                         npath.join(dest, file), () => {
                           if (--n === 0) callback() ;
                         }) ;
              }) ;
            }
          }) ;
        }) ;
      }
      else
        throw new Error(source + ' isn\'t file or dir!') ;
    }) ;
  }


  /**
   * Supprime un fichier ou un dossier et tout son contenu
   * @param {String} path Fichier ou dossier à supprimer
   * @param {Function} [callback] Function à exécuter après la suppression
   *                              `(err){}`
   */
  static rmrf (path, callback = () => {}) {

    fs.stat(path, (err, stat) => {

      if (err && callback)
        return callback(err) ;
      else if (err)
        throw new Error(err) ;

      if (stat.isFile())
        fs.unlink(path, callback) ;
      else if (stat.isDirectory()) {

        fs.readdir(path, (err, files) => {

          if (err && callback)
            return callback(err) ;
          else if (err)
            throw new Error(err) ;

          let n = files.length ;

          if (n === 0)
            fs.rmdir(path, callback) ;

          else {
            files.forEach(file => {

              this.rmrf(npath.join(path, file), (err) => {
                if (err && callback)
                  return callback(err) ;
                else if (--n === 0)
                  fs.rmdir(path, callback) ;
              }) ;
            }) ;
          }

        }) ;
      }
      else
        throw new Error(path + ' isn\'t file or dir!') ;
    }) ;
  }
}

module.exports = Utils ;

