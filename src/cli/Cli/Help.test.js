'use strict' ;

/**
 * src/cli/Cli/Help.test.js
 */

const expect = require('chai').expect ;

const Help  = require('./Help'),
      Cli   = require('../Cli'),
      Table = require('../Table/Table') ;

let cli = null ;

const globalOpts = `\u001b[1mGLOBAL OPTIONS\u001b[22m

    \u001b[32m -h\u001b[39m,\u001b[32m --help\u001b[39m        Display help`
    + '                                      ' + `
    \u001b[32m -V\u001b[39m,\u001b[32m --version\u001b[39m     Display version`
    + '                                   ' + `
    \u001b[32m --no-color\u001b[39m        Disable colors`
    + '                                    ' + `
    \u001b[32m --quiet\u001b[39m           Quiet mode - `
    + `only displays warn and error messages
    \u001b[32m -v\u001b[39m,\u001b[32m --verbose\u001b[39m     `
    + 'Verbose mode - will also output debug messages    ' ;

describe('Help', function () {

  it('Enregistre l\'instance de l\'application', function () {

    let a = {} ;
    let help = new Help(a) ;
    expect(help._cli).to.be.equal(a) ;
  }) ;

  describe('display', function () {

    beforeEach(function () {

      cli = new Cli() ;
      cli.name('myApp').version('0.0.1a') ;
    }) ;

    it('Aide pour un programme vide', function () {

      cli = new Cli() ;
      expect(cli._help.display())
        .to.be.equal(
          '\n   \u001b[36mmocha\u001b[39m \n\n'
        + '   \u001b[1mUSAGE\u001b[22m\n\n     '
        + '\u001b[3mmocha\u001b[23m '
        + '\u001b[34m\u001b[35m<command>\u001b[39m\u001b[39m '
        + '\u001b[33m[options]\u001b[39m\n\n   '
        + globalOpts + '\n'
        ) ;
    }) ;

    it('Aide pour un programme avec un nom', function () {

      cli = new Cli() ;
      cli.name('myAppCool') ;
      expect(cli._help.display())
        .to.be.equal('\n   \u001b[36mmyAppCool\u001b[39m \n\n'
        + '   \u001b[1mUSAGE\u001b[22m\n\n     '
        + '\u001b[3mmocha\u001b[23m '
        + '\u001b[34m\u001b[35m<command>\u001b[39m\u001b[39m '
        + '\u001b[33m[options]\u001b[39m\n\n   '
        + globalOpts + '\n'
        ) ;
    }) ;

    it('Aide pour un programme avec une version', function () {

      cli = new Cli() ;
      cli.name('myApp').version('0.0.1a') ;
      expect(cli._help.display())
        .to.be.equal(
          '\n   \u001b[36mmyApp\u001b[39m \u001b[2m0.0.1a\u001b[22m\n\n'
        + '   \u001b[1mUSAGE\u001b[22m\n\n     '
        + '\u001b[3mmocha\u001b[23m '
        + '\u001b[34m\u001b[35m<command>\u001b[39m\u001b[39m '
        + '\u001b[33m[options]\u001b[39m\n\n   '
        + globalOpts + '\n'
        ) ;
    }) ;

    it('Aide pour un programme avec une description', function () {

      cli = new Cli() ;
      cli.name('myApp').version('0.0.1a').description('Une app trop cool') ;
      expect(cli._help.display())
        .to.be.equal(
          '\n   \u001b[36mmyApp\u001b[39m \u001b[2m0.0.1a\u001b[22m'
          + ' - Une app trop cool\n\n'
        + '   \u001b[1mUSAGE\u001b[22m\n\n     '
        + '\u001b[3mmocha\u001b[23m '
        + '\u001b[34m\u001b[35m<command>\u001b[39m\u001b[39m '
        + '\u001b[33m[options]\u001b[39m\n\n   '
        + globalOpts + '\n'
        ) ;
    }) ;
  }) ;

  describe('_colorize', function () {

    let help = new Help(new Cli()) ;

    it('Ne fait rien pour un texte basique', function () {

      expect(help._colorize('Un texte')).to.be.equal('Un texte') ;
    }) ;

    it('Met en bleu les arguments (entre `<>`)', function () {

      expect(help._colorize('Un texte <arg> ou <arg2>'))
        .to.be.equal(
          'Un texte \u001b[34m<arg>\u001b[39m ou \u001b[34m<arg2>\u001b[39m'
        ) ;
    }) ;

    it('Met en magenta `<command>`', function () {

      expect(help._colorize('Une <command> dans un texte'))
        .to.be.equal(
          'Une \u001b[34m\u001b[35m<command>\u001b[39m\u001b[39m dans un texte'
        ) ;
    }) ;

    it('Met en jaune les options (entre `[]`', function () {

      expect(help._colorize('Une [opt] et une autre [opt2]'))
        .to.be.equal(
        'Une \u001b[33m[opt]\u001b[39m et une autre \u001b[33m[opt2]\u001b[39m'
        ) ;
    }) ;

    it('Met en vert les options que l\'on détaille ` - ...`', function () {

      expect(help._colorize('Un texte -h, --help'))
        .to.be.equal(
          'Un texte\u001b[32m -h\u001b[39m,\u001b[32m --help\u001b[39m'
        ) ;
    }) ;
  }) ;

  describe('_getGlobalOptions', function () {

    it('Renvoit la description des options globales', function () {

      let help = new Help(new Cli()) ;
      expect(help._getGlobalOptions()).to.be.equal(globalOpts) ;
    }) ;
  }) ;

  describe('_getSimpleTable', function () {

    it('Retourne un nouveau tableau', function () {

      let help = new Help(new Cli()) ;
      expect(help._getSimpleTable() instanceof Table).to.be.true ;
    }) ;

    it('Définit des styles, sans bordures', function () {

      let help = new Help(new Cli()) ;
      let noBorder = help._getSimpleTable() ;
      let expected = {
        chars: {
          bottom: '', 'bottom-left': '', 'bottom-mid': '', 'bottom-right': '',
          left: '', 'left-mid': '', mid: '', 'mid-mid': '', middle: '',
          right: '', 'right-mid': '',
          top: '', 'top-left': '', 'top-mid': '', 'top-right': ''
        },
        colAligns: [], colWidths: [], head: [], rowAligns: [], rowHeights: [],
        style: {
          border: ['grey'],
          compact: false,
          head: ['red'],
          'padding-left': 5,
          'padding-right': 0
        },
        truncate: '…'
      } ;

      expect(noBorder._options).to.be.deep.equal(expected) ;
    }) ;
  }) ;

  describe('_getPredefinedOptions', function () {

    it('retourne la liste des options par défaut', function () {

      let help = new Help() ;
      let expected = [
        ['-h, --help', 'Display help'],
        ['-V, --version', 'Display version'],
        ['--no-color', 'Disable colors'],
        ['--quiet', 'Quiet mode - only displays warn and error messages'],
        ['-v, --verbose', 'Verbose mode - will also output debug messages']
      ] ;

      expect(help._getPredefinedOptions()).to.be.deep.equal(expected) ;
    }) ;

  }) ;
}) ;

