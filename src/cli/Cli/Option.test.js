'use strict' ;

/**
 * src/cli/Cli/Option.test.js
 */

const expect = require('chai').expect ;

const Option = require('./Option'),
      Cli    = require('../Cli'),
      OptionSyntaxError = require('../Error/OptionSyntax') ;

describe('Option', function () {

  let app = new Cli() ;

  describe('constructor', function () {

    let opt = null ;

    beforeEach(function () {

      opt = new Option('-c', 'une description', /[0-9]/, 0, true, app) ;
    }) ;

    it('détermine si l\'option peut contenir un nombre variable d\'éléments',
    function () {

      expect(opt._variadic).to.be.equal(undefined) ;
      opt = new Option('-test [lol...]', 'une description') ;
      expect(opt._variadic).to.be.true ;
      opt = new Option('-test [lol]', 'une description') ;
      expect(opt._variadic).to.be.false ;
    }) ;

    it('détermine si l\'option doit avoir une valeur ou non', function () {

      expect(opt._valueType).to.be.equal(undefined) ;
      opt = new Option('-test [lol]', 'une description') ;
      expect(opt._valueType).to.be.equal(Option.OPTIONAL_VALUE) ;
      opt = new Option('-test <lol>', 'une description') ;
      expect(opt._valueType).to.be.equal(Option.REQUIRED_VALUE) ;
    }) ;

    it('définit le nom de l\'option', function () {

      expect(opt._name).to.be.equal('c') ;
      opt = new Option('--test...', 'une description') ;
      expect(opt._name).to.be.equal('test') ;
      opt = new Option('--salut-toi>', 'une description') ;
      expect(opt._name).to.be.equal('salutToi') ;
      opt = new Option('--HelloWorld3 <path>', 'une description') ;
      expect(opt._name).to.be.equal('helloWorld3') ;
      opt = new Option('--my-HTTP [file]', 'une description') ;
      expect(opt._name).to.be.equal('myHttp') ;
    }) ;

    it('enregistre si l\'option est obligatoire ou facultative', function () {

      expect(opt._required).to.be.true ;
      opt = new Option('-c', 'une description', undefined, 0, false, app) ;
      expect(opt._required).to.be.false ;
    }) ;
  }) ;


  describe('_analyseSynopsis', function () {

    it('long option', function () {

      let opt = new Option('--force') ;
      let expected = {
        long: '--force',
        longCleanName: 'force'
      } ;

      expect(opt._analyseSynopsis()).to.be.deep.equal(expected) ;
    }) ;

    it('short option', function () {

      let opt = new Option('-f') ;
      let expected = {
        short: '-f',
        shortCleanName: 'f'
      } ;

      expect(opt._analyseSynopsis()).to.be.deep.equal(expected) ;
    }) ;

    it('constantes OPTIONAL_VALUE et REQUIRED_VALUE définies', function () {

      expect(Option.OPTIONAL_VALUE).to.be.not.equal(Option.REQUIRED_VALUE) ;
    }) ;

    it('optional value', function () {

      let opt = new Option('-f [valeur]') ;
      let expected = {
        short: '-f',
        shortCleanName: 'f',
        valueType: Option.OPTIONAL_VALUE,
        variadic: false
      } ;

      expect(opt._analyseSynopsis()).to.be.deep.equal(expected) ;
    }) ;

    it('required value', function () {

      let opt = new Option('-f <valeur>') ;
      let expected = {
        short: '-f',
        shortCleanName: 'f',
        valueType: Option.REQUIRED_VALUE,
        variadic: false
      } ;

      expect(opt._analyseSynopsis()).to.be.deep.equal(expected) ;
    }) ;

    it('l\'option peut avoir un nombre variable de valeurs', function () {

      let opt = new Option('-f <valeur...>') ;
      let expected = {
        short: '-f',
        shortCleanName: 'f',
        valueType: Option.REQUIRED_VALUE,
        variadic: true
      } ;

      expect(opt._analyseSynopsis()).to.be.deep.equal(expected) ;
    }) ;

    it('envoit une erreur s\'il y a un problème de syntaxe', function () {

      let opt = new Option('-f <valeur...>', '', /a/, 0, true, app) ;
      opt.synopsis = 'f <valeur...>' ;

      expect(opt._analyseSynopsis.bind(opt)).to.throw(OptionSyntaxError) ;
      expect(opt._analyseSynopsis.bind(opt))
        .to.throw('Syntax error in option synopsis: f <valeur...>') ;
    }) ;
  }) ;


  describe('isRequired', function () {

    it('return true if option is required', function () {

      let opt = new Option('-f <value>', 'desc', /[0-9]/, 'truc', true, app) ;
      expect(opt.isRequired()).to.be.true ;
    }) ;

    it('return false if option is not required', function () {

      let opt = new Option('-f <value>', 'desc', /[0-9]/, 'truc', false, app) ;
      expect(opt.isRequired()).to.be.false ;
    }) ;
  }) ;
}) ;

