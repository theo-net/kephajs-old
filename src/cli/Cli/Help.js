'use strict' ;

/**
 * src/cli/Cli/Help.js
 */


/**
 * Gère l'affiche de l'aide
 */
class Help {

  /**
   * Associe l'aide avec l'application
   * @param {Cli} cli Instance de l'application
   */
  constructor (cli) {

    this._cli = cli ;
  }

  /**
   * Affiche l'aide de `command`
   *
   * @param {Command|null} command Commande dont on veut l'aide
   * @returns {String}
   */
  display (command) {

    if (!command && this._cli._commands.length === 1)
      command = this._cli._commands[0] ;

    let help = '\n   '
             + this._cli.color().cyan(this._cli.name() || this._cli.bin) + ' '
             + this._cli.color().dim(
                this._cli.version() ? this._cli.version() : '')
             + (this._cli.description() ? ' - ' + this._cli.description() : '')
             + '\n' + '\n'
             + '   ' + this._getUsage(command)
             + '' ;

    if (!command && this._cli._commands.length > 1)
      help += '\n\n   ' + this._getCommands() ;

    help += '\n\n   ' + this._getGlobalOptions() + '\n' ;

    return help ;
  }


  /**
   * Colorise un texte pour le rendu de l'aide
   * @param {String} text Texte à formater
   * @return {String}
   * @private
   */
  _colorize (text) {

    return text.replace(/<([a-z0-9-_.]+)>/gi, (match) => {
      return this._cli.color().blue(match) ;
    }).replace(/<command>/gi, (match) => {
      return this._cli.color().magenta(match) ;
    }).replace(/\[([a-z0-9-_.]+)\]/gi, (match) => {
      return this._cli.color().yellow(match) ;
    }).replace(/ --?([a-z0-9-_.]+)/gi, (match) => {
      return this._cli.color().green(match) ;
    }) ;
  }


  /**
   * Retourne la liste des commandes
   *
   * @returns {String}
   * @private
   */
  _getCommands () {

    const commandTable = this._getSimpleTable() ;

    this._cli._commands.forEach(cmd => {
      commandTable.push(
        [this._cli.color().magenta(cmd.getSynopsis()), cmd.description]
      ) ;
    }) ;

    commandTable.push(
      [this._cli.color().magenta('help <command>'),
        'Display help for a specific command']
    ) ;

    return this._cli.color().bold('COMMANDS') + '\n\n'
         + this._colorize(commandTable.toString()) ;
  }


  /**
   *
   * @returns {String}
   * @private
   */
  _getGlobalOptions () {

    const optionsTable = this._getSimpleTable() ;

    this._getPredefinedOptions().forEach(opt => optionsTable.push(opt)) ;

    return this._cli.color().bold('GLOBAL OPTIONS') + '\n\n'
           + this._colorize(optionsTable.toString()) ;
  }


  /**
   * Retourne l'aide d'une commande
   * @param {Command} cmd Commande
   * @returns {String}
   * @private
   */
  _getCommandHelp (cmd) {

    const args = cmd.args() ;
    const options = cmd.getOptions() ;

    // Usage
    let help = (cmd.name() ? cmd.name() + ' ' : '')
             + args.map(a => a.synopsis).join(' ') ;

    // Arguments
    if(args.length) {

      help += '\n\n   ' + this._cli.color().bold('ARGUMENTS') + '\n\n' ;

      const argsTable = this._getSimpleTable() ;
      args.forEach(a => {

        const def = a.hasDefault() ?
          'default: ' + JSON.stringify(a.default) : '' ;
        const req = a.isRequired() ? this._cli.color().bold('required')
          : this._cli.color().grey('optional') ;
        argsTable.push([
          a.synopsis, a.description, req, this._cli.color().grey(def)
        ]) ;
      }) ;

      help += argsTable.toString() ;
    }

    // Options
    if (options.length) {

      help += '\n\n   ' + this._cli.color().bold('OPTIONS') + '\n\n' ;

      const optionsTable = this._getSimpleTable() ;
      options.forEach(o => {

        const def = o.hasDefault() ?
          'default: ' + JSON.stringify(o.default) : '' ;
        const req = o.isRequired() ? this._cli.color().bold('required')
          : this._cli.color().grey('optional') ;
        optionsTable.push([
          o.synopsis, o.description, req, this._cli.color().grey(def)
        ]) ;
      }) ;

      help += optionsTable.toString() ;
    }

    return help ;
  }


  /**
   * Retourne l'usage d'une commande
   * @param {Command} cmd Commande
   * @returns {String}
   * @private
   */
  _getUsage (cmd) {

    let help = this._cli.color().bold('USAGE')
             + '\n\n     '
             + this._cli.color().italic(this._cli.bin) + ' ' ;

    if (cmd)
      help += this._colorize(this._getCommandHelp(cmd)) ;
    else
      help += this._colorize('<command> [options]') ;

    return help ;
  }


  /**
   * Retourne un tableau sans bordure, formaté pour le rendu de l'aide
   * @returns {Table}
   * @private
   */
  _getSimpleTable () {

    return this._cli.newTable({
      style: {
        'padding-left': 5,
        'padding-right': 0
      }
    }, 'noBorder') ;
  }


  /**
   * Retourne la liste des options prédéfinies
   * @returns {Array}
   * @private
   */
  _getPredefinedOptions () {

    return [
      ['-h, --help', 'Display help'],
      ['-V, --version', 'Display version'],
      ['--no-color', 'Disable colors'],
      ['--quiet', 'Quiet mode - only displays warn and error messages'],
      ['-v, --verbose', 'Verbose mode - will also output debug messages']
    ] ;
  }
}

module.exports = Help ;

