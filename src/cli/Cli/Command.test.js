'use strict' ;

/**
 * src/cli/Cli/Command.test.js
 */

const expect = require('chai').expect ;

const Command   = require('./Command'),
      Validator = require('../../core/Validate/Validator'),
      Cli       = require('../Cli'),
      InvalidArgumentValueError  = require('../Error/InvalidArgumentValue'),
      InvalidOptionValueError    = require('../Error/InvalidOptionValue'),
      MissingOptionError         = require('../Error/MissingOption'),
      UnknownOptionError         = require('../Error/UnknownOption'),
      WrongNumberOfArgumentError = require('../Error/WrongNumberOfArgument') ;

describe('Command', function () {

  let application = new Cli() ;
  let command = null ;

  beforeEach(function () {

    command = new Command('test', 'un test', application) ;
  }) ;

  it('save name, description and application\'s instance', function () {

    expect(command._name).to.be.equal('test') ;
    expect(command._description).to.be.equal('un test') ;
    expect(command._application).to.be.equal(application) ;
  }) ;

  describe('alias', function () {

    it('enregistre un alias', function () {

      command.alias('alias') ;
      expect(command._alias).to.be.equal('alias') ;
    }) ;

    it('retourne la commande', function () {

      expect(command.alias('alias')).to.be.equal(command) ;
    }) ;
  }) ;


  describe('argument', function () {

    it('enregistre un argument', function () {

      command.argument('arg', 'un argument', /[0-9]/, 0) ;
      let arg = command._args.slice()[0] ;

      expect(arg.synopsis).to.be.equal('arg') ;
      expect(arg.description).to.be.equal('un argument') ;
      expect(arg._validator).to.be.instanceOf(Validator) ;
      expect(arg._validator._validators[0]._validatorArguments.regExp)
        .to.be.deep.equal(/[0-9]/) ;
      expect(arg.default).to.be.equal(0) ;
      expect(arg._application).to.be.equal(command._application) ;
    }) ;

    it('retourne la commande', function () {

      expect(command.argument('rien', 'ne fais rien')).to.be.equal(command) ;
    }) ;
  }) ;


  describe('option', function () {

    it('enregistre une option', function () {

      command.option('-c', 'active les couleurs', /[0-9]/, 0, true) ;
      let opt = command._options.slice()[0] ;

      expect(opt.synopsis).to.be.equal('-c') ;
      expect(opt.description).to.be.equal('active les couleurs') ;
      expect(opt._validator).to.be.instanceOf(Validator) ;
      expect(opt._validator._validators[0]._validatorArguments.regExp)
        .to.be.deep.equal(/[0-9]/) ;
      expect(opt.default).to.be.equal(0) ;
      expect(opt._required).to.be.true ;
      expect(opt._application).to.be.equal(command._application) ;
    }) ;

    it('retourne la commande', function () {

      expect(command.option('--rien', 'ne fais rien')).to.be.equal(command) ;
    }) ;
  }) ;


  describe('getOptions', function () {

    it('retourne la liste des options', function () {

      expect(command.getOptions()).to.be.equal(command._options) ;
    }) ;
  }) ;


  describe('action', function () {

    it('enregistre l\'action associée à la commande', function () {

      let fct = function () {} ;
      command.action(fct) ;

      expect(command._action).to.be.equal(fct) ;
    }) ;

    it('retourne la commande', function () {

      expect(command.action(function () {})).to.be.equal(command) ;
    }) ;
  }) ;


  describe('command', function () {

    it('créé une nouvelle commande', function () {

      let cli = new Cli() ;
      let cmd1 = cli.command('test', 'un test') ;

      // on espionne Application.command pour voir si la méthode est appelée
      let cmd2 = cmd1.command('lol', 'desc') ;
      expect(cli._commands[1]).to.be.equal(cmd2) ;
    }) ;
  }) ;

  describe('name', function () {

    it('retourne le nom de la commande', function () {

      command = new Command('unNom', 'un test', application) ;
      expect(command.name()).to.be.equal('unNom') ;
    }) ;

    it('retourne une chaîne vide s\'il s\'agit de la commande par défaut',
    function () {

      command = new Command('_default', 'un test', application) ;
      expect(command.name()).to.be.equal('') ;
    }) ;
  }) ;


  describe('getAlias', function () {

    it('retourne le nom de l\'alias', function () {

      command = new Command('unNom', 'un test', application) ;
      command.alias('unAlias') ;
      expect(command.getAlias()).to.be.equal('unAlias') ;
    }) ;
  }) ;


  describe('getSynopsis', function () {

    it('retourne le synopsis de la commande (sans arguments)', function () {

      command = new Command('unNom', 'description', application) ;
      expect(command.getSynopsis()).to.be.equal('unNom ') ;
    }) ;

    it('retourne le synopsis de la commande (avec arguments)', function () {

      command = new Command('unNom', 'description', application) ;
      command.argument('[truc]', 'lol', /a/)
             .argument('[foo]', 'bar', /a/) ;
      expect(command.getSynopsis()).to.be.equal('unNom [truc] [foo]') ;
    }) ;
  }) ;


  describe('args', function () {

    beforeEach(function () {

      command = new Command('unNom', 'description', application) ;
      command.argument('[truc]', 'lol', /a/)
             .argument('[foo]', 'bar', /a/) ;
    }) ;

    it('renvoit tous les arguments', function () {

      expect(command.args()).to.be.equal(command._args) ;
    }) ;

    it('renvoit un argument selon l\'index', function () {

      expect(command.args(1)).to.be.equal(command._args[1]) ;
    }) ;
  }) ;

  describe('_validateCall', function () {

    beforeEach(function () {

      command = new Command('unNom', 'un test', application) ;
      command.argument('<aa>', '', /a/, 'a')
             .argument('<ab>', '', /b/, 'b')
             .argument('[ac]', '', /c/, 'c')
             .argument('[ad]', '', /d/, 'd')
             .option('-m <num>', '', 'integer', undefined, true)
             .option('-n <num>', '', 'integer')
             .option('-k, --koala-lol', '')
             .option('-l, --list <what>', '', 'string') ;
    }) ;

    describe('_checkArgsRange', function () {

      it('pas d\'erreur si nombre d\'arg correct', function () {

        expect(command._checkArgsRange.bind(command, ['a', 'b', 'c', 'd']))
          .to.not.throw(WrongNumberOfArgumentError) ;
        expect(command._checkArgsRange.bind(command, ['a', 'b', 'c']))
          .to.not.throw(WrongNumberOfArgumentError) ;
        expect(command._checkArgsRange.bind(command, ['a', 'b']))
          .to.not.throw(WrongNumberOfArgumentError) ;
      }) ;

      it('erreur si nombre d\'arg insuffisant', function () {

        expect(command._checkArgsRange.bind(command, ['a']))
          .to.throw(WrongNumberOfArgumentError) ;
        expect(command._checkArgsRange.bind(command, ['a']))
          .to.throw('Wrong number of argument(s) for command unNom. Got 1, '
                  + 'expected between 2 and 4.') ;
      }) ;

      it('erreur si nombre d\'arg trop grand', function () {

        expect(command._checkArgsRange.bind(command, ['a', 'b', 'c', 'd', 'e']))
          .to.throw(WrongNumberOfArgumentError) ;
        expect(command._checkArgsRange.bind(command, ['a', 'b', 'c', 'd', 'e']))
          .to.throw('Wrong number of argument(s) for command unNom. Got 5, '
                  + 'expected between 2 and 4.') ;
      }) ;
    }) ;

    describe('_validateArgs', function () {

      it('erreur si un arg est invalide', function () {

        expect(command._validateArgs.bind(command, {aa: 'b', ab: 'a'}))
          .to.throw(InvalidArgumentValueError) ;
        expect(command._validateArgs.bind(command, {aa: 'b', ab: 'a'}))
          .to.throw('   \u001b[31mError: Invalid value \'b\' '
            + 'for argument \u001b[3maa\u001b[23m.\n  '
            + '- La valeur n\'est pas valide.\u001b[39m\n   '
            + 'Type \u001b[1mmocha --help\u001b[22m for help.\n') ;
      }) ;
    }) ;

    describe('_checkRequiredOptions', function () {

      it('pas d\'erreur si options requises présentes', function () {

        expect(command._checkRequiredOptions.bind(command, {m: 1}))
          .to.not.throw(MissingOptionError) ;
        expect(command._checkRequiredOptions.bind(command, {m: 1, n: 1}))
          .to.not.throw(MissingOptionError) ;
      }) ;

      it('erreur si option requise absente', function () {

        expect(command._checkRequiredOptions.bind(command, {n: 1}))
          .to.throw(MissingOptionError) ;
        expect(command._checkRequiredOptions.bind(command, {n: 1}))
          .to.throw('Missing option \u001b[3m-m\u001b[23m.\u001b[39m') ;
      }) ;
    }) ;

    describe('_validateOptions', function () {

      it('erreur si option inconnue transmise', function () {

        expect(command._validateOptions.bind(command, {b: 'a'}))
          .to.throw(UnknownOptionError) ;
        expect(command._validateOptions.bind(command, {b: 'a'}))
          .to.throw('Error: Unknown option \u001b[3m-b\u001b[23m.\u001b[39m') ;
      }) ;

      it('erreur si une option a une valeur invalide', function () {

        expect(command._validateOptions.bind(command, {m: 'a'}))
          .to.throw(InvalidOptionValueError) ;
        expect(command._validateOptions.bind(command, {m: 'a'}))
          .to.throw('   \u001b[31mError: Invalid value \'a\' '
            + 'for option \u001b[3m-m\u001b[23m.\n  '
            + '- Doit être un entier.\u001b[39m\n   '
            + 'Type \u001b[1mmocha --help\u001b[22m for help.\n') ;
      }) ;
    }) ;

    describe('_addLongNotationToOptions', function () {

      it('ajoute la version longue des arguments si besoin', function () {

        expect(command._addLongNotationToOptions({m: 1, l: 'test'}))
          .to.be.deep.equal({m: 1, l: 'test', list: 'test'}) ;
        expect(command._addLongNotationToOptions({k: true}))
          .to.be.deep.equal({k: true, 'koala-lol': true}) ;
      }) ;
    }) ;

    describe('_camelCaseOptions', function () {

      it('On nettoie la liste d\'options', function () {

        expect(command._camelCaseOptions({
          m: 1, l: 'test', list: 'test', k: true, 'koala-lol': true
        })).to.be.deep.equal({
          m: 1, list: 'test', koalaLol: true
        }) ;
      }) ;
    }) ;
  }) ;

}) ;

