'use strict' ;

/**
 * src/cli/Cli/ArgOpt.test.js
 */

const expect = require('chai').expect ;

const ArgOpt           = require('./ArgOpt'),
      Cli              = require('../Cli'),
      Validator        = require('../../core/Validate/Validator'),
      IntegerValidator = require('../../core/Validate/Integer') ;


// buddy ignore:start
describe('ArgOpt', function () {

  let app = new Cli() ;

  describe('constructor', function () {

    let arg = null ;

    beforeEach(function () {

      arg = new ArgOpt('<test>', 'une description', /[0-9]/, 0, app) ;
    }) ;

    it('sauve le synopsis', function () {

      expect(arg.synopsis).to.be.equal('<test>') ;
    }) ;

    it('enregistre le validateur', function () {

      expect(arg._validator).to.be.instanceOf(Validator) ;
      expect(arg._validator._validators[0]._validatorArguments.regExp)
        .to.be.deep.equal(/[0-9]/) ;
    }) ;

    it('si le validateur est un objet, on récupère les args', function () {

      arg = new ArgOpt('<test>', 'une description',
                       {integer: {min: 0}}, 0, app) ;
      expect(arg._validator._validators[0]).to.be.instanceOf(IntegerValidator) ;
      expect(arg._validator._validators[0]._validatorArguments.min)
        .to.be.equal(0) ;
    }) ;

    it('null si aucun validateur', function () {

      arg = new ArgOpt('[my-HTTP]', 'une description', undefined, 0, app) ;
      expect(arg._validator).to.be.equal(null) ;
    }) ;

    it('sauve la valeur par défaut', function () {

      expect(arg.default).to.be.equal(0) ;
    }) ;

    it('enregistre l\'instance de l\'application', function () {

      expect(arg._application).to.be.equal(app) ;
    }) ;
  }) ;


  describe('hasDefault', function () {

    it('return true if argument have default', function () {

      let arg = new ArgOpt('<test>', 'une description', /[0-9]/, 2, app) ;
      expect(arg.hasDefault()).to.be.true ;
    }) ;

    it('return false if argument have not default', function () {

      let arg = new ArgOpt(
        '<test>', 'une description', /[0-9]/, undefined, app) ;
      expect(arg.hasDefault()).to.be.false ;
    }) ;
  }) ;


  describe('name', function () {

    it('returns the name of Argument', function () {

      let arg = new ArgOpt('<test>', '', /a/, 'a', app) ;
      expect(arg.name()).to.be.equal(arg._name) ;
    }) ;
  }) ;

  describe('isValid', function () {

    it('returns `true` if the value is good', function () {

      let arg = new ArgOpt('<test>', '', /a/, 'a', app) ;
      expect(arg.isValid('a')).to.be.equal(true) ;
    }) ;

    it('returns `false` if the value isn\'t good', function () {

      let arg = new ArgOpt('<test>', '', /a/, 'a', app) ;
      expect(arg.isValid('b')).to.be.equal(false) ;
    }) ;
  }) ;
}) ;
// buddy ignore:end

