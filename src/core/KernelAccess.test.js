'use strict' ;

/**
 * src/core/KernelAccess.test.js
 */

let expect = require('chai').expect ;

const KernelAccess = require('./KernelAccess'),
      Kernel = require('./Kernel') ;

describe('KernelAccess kernel', function () {

  it('retourne une instance de Kernel', function () {

    let kernelAccess = new KernelAccess() ;
    expect(kernelAccess.kernel() instanceof Kernel).to.be.equal(true) ;
  }) ;

  it('retourne toujours la même instance', function () {

    let kernelAccess = new KernelAccess() ;
    let kernel1 = kernelAccess.kernel() ;
    let kernel2 = kernelAccess.kernel() ;

    expect(kernel1).to.be.equal(kernel2) ;
  }) ;

  it('plusieurs KernelAccess retournent toujours la même instance',
  function () {

    let kernelAccess = new KernelAccess() ;
    let kernelAccess2 = new KernelAccess() ;
    let kernel1 = kernelAccess.kernel() ;
    let kernel2 = kernelAccess2.kernel() ;

    expect(kernel1).to.be.equal(kernel2) ;
  }) ;
}) ;

