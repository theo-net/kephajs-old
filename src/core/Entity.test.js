'use strict' ;

/**
 * src/core/Entity.test.js
 */

const expect = require('chai').expect ;

const Entity = require('./Entity'),
      Utils  = require('./Utils') ;

class testEntity extends Entity {

  _setProperties () {

    this.a = true ;
    this._setProperty('b', null, 'c') ;
    this._setProperty('d', {integer: {min: 1, max: 5}}) ;
    this._setProperty('e', null, null, 'foobar') ;
  }
}

describe('core/Entity', function () {

  it('appelle _setProperties lors de l\'initialisation', function () {

    let entity = new testEntity() ;

    expect(entity.a).to.be.true ;
  }) ;

  it('isNew true par défaut', function () {

    let entity = new testEntity() ;

    expect(entity.isNew()).to.be.true ;
  }) ;

  it('On peut lui transmettre des variables', function () {

    let entity = new testEntity({c: 1, d: 2}) ;

    expect(entity.b).to.be.equal(1) ;
    expect(entity.d).to.be.equal(2) ;
  }) ;

  it('Si on lui transmet variables, isNew false', function () {

    let entity = new testEntity({c: 1, d: 2}) ;

    expect(entity.isNew()).to.be.false ;
  }) ;

  it('Si on lui transmet variables, on peut forcer isNew true', function () {

    let entity = new testEntity({c: 1, d: 2}, true) ;

    expect(entity.isNew()).to.be.true ;
  }) ;

  it('hasModifications false par défaut', function () {

    let entity = new testEntity({c: 1, d: 2}) ;

    expect(entity.hasModifications()).to.be.false ;
  }) ;

  it('hasModifications true si modifs', function () {

    let entity = new testEntity({c: 1, d: 2}) ;

    entity.b = 2 ;

    expect(entity.hasModifications()).to.be.true ;
  }) ;

  it('hasModifications true si modifs (même si valeur ==)', function () {

    let entity = new testEntity({c: 1, d: 2}) ;

    entity.b = 1 ;

    expect(entity.hasModifications()).to.be.true ;
  }) ;

  it('saved met isNew false et hasModifications true', function () {

    let entity1 = new testEntity({c: 1, d: 2}) ;
    entity1.b = 1 ;
    entity1.saved() ;
    let entity2 = new testEntity({c: 1, d: 2}, true) ;
    entity2.saved() ;

    expect(entity1.hasModifications()).to.be.false ;
    expect(entity1.isNew()).to.be.false ;
    expect(entity2.hasModifications()).to.be.false ;
    expect(entity2.isNew()).to.be.false ;
  }) ;

  it('alias for properties', function () {

    let entity = new testEntity({c: 1}) ;

    expect(entity.b).to.be.equal(1) ;
  }) ;

  it('properties enumerable', function () {

    let entity = new testEntity({c: 1, d: 2}) ;
    let properties = [] ;

    Utils.forEach(entity, (value, name) => {
      properties.push(name) ;
    }) ;

    expect(properties).to.deep.equal([
      '_isNew', '_hasModifications', '_properties', '_validators', '_alias',
      'a', 'b', 'd', 'e', 'toString'
    ]) ;
  }) ;

  it('lance une erreur si la valeur n\'est pas valide', function () {

    let entity = new testEntity() ;

    expect(() => {
      entity.d = 6 ;
    }).throws(Error) ;
  }) ;

  it('une propriété peut avoir une valeur par défaut', function () {

    let entity = new testEntity() ;

    expect(entity.e).to.be.equal('foobar') ;
  }) ;

  it('valeur par défaut utilisée si `set(undefined)`', function () {

    let entity = new testEntity() ;
    entity.e = undefined ;

    expect(entity.e).to.be.equal('foobar') ;
  }) ;
}) ;

