'use strict' ;

/**
 * src/core/Application.test.js
 */

const expect = require('chai').expect ;

const Application  = require('./Application'),
      KernelAccess = require('./KernelAccess') ;

describe('Application', function () {

  it('étend KernelAccess', function () {

    let app = new Application() ;
    expect(app).to.be.instanceof(KernelAccess) ;
  }) ;

  it('s\'enregistre comme service', function () {

    let app = new Application() ;
    expect(app.kernel().get('$application')).to.be.equal(app) ;
  }) ;

  it('fournit une serie d\'outils: Utils', function () {

    let app = new Application() ;
    let Framework = require('./Framework') ;
    expect(app.kjs).to.be.deep.equal(Framework) ;
  }) ;


  describe('name', function () {

    let app = new Application() ;

    it('définit le nom de l\'application', function () {

      app.name('app') ;
      expect(app._name).to.be.equal('app') ;
    }) ;

    it('si un nom est définit, retourne l\'app', function () {

      expect(app.name('appCool')).to.be.equal(app) ;
    }) ;

    it('retourne le nom de l\'application', function () {

      expect(app.name()).to.be.equal('appCool') ;
    }) ;
  }) ;

  describe('version', function () {

    let app = new Application() ;

    it('définit la version de l\'application', function () {

      app.version('0.1.2') ;
      expect(app._version).to.be.equal('0.1.2') ;
    }) ;

    it('si une version est définit, retourne l\'app', function () {

      expect(app.version('0.2.3')).to.be.equal(app) ;
    }) ;

    it('retourne la version de l\'application', function () {

      expect(app.version()).to.be.equal('0.2.3') ;
    }) ;
  }) ;
}) ;

