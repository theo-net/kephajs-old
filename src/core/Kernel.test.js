'use strict' ;

/**
 * src/core/Kernel.test.js
 */

const expect = require('chai').expect ;

const Kernel           = require('./Kernel'),
      ServiceContainer = require('./Services/ServiceContainer'),
      Config           = require('./Services/Config'),
      Filter           = require('./Services/Filter'),
      Validate         = require('./Services/Validate'),
      Scope            = require('./Services/Scope'),
      Events           = require('./Services/Events'),
      Cache            = require('./Services/Cache'),
      Tmp              = require('./Services/Tmp') ;

describe('Kernel', function () {

  let kernel = new Kernel() ;

  describe('init', function () {

    beforeEach(function () {

      kernel = new Kernel(true) ;
    }) ;

    it('Si déjà initialisé, renvoit toujours la même instance', function () {

      let kernel2 = new Kernel() ;
      expect(kernel2).to.be.equal(kernel) ;
    }) ;

    it('On peut forcer un réinitialisation', function () {

      let kernel2 = new Kernel(true) ;
      expect(kernel2).to.be.not.equal(kernel) ;
    }) ;

    it('initialise les services', function () {

      // config, validate, ...
      expect(kernel.container()._kernel).to.be.equal(kernel) ;
      expect(kernel.container().has('$config')).to.be.true ;
      expect(kernel.get('$config')).to.be.instanceOf(Config) ;
      expect(kernel.get('$validate')).to.be.instanceOf(Validate) ;
      expect(kernel.container().has('$parser')).to.be.true ;
      expect(kernel.container().has('$filter')).to.be.true ;
      expect(kernel.get('$filter')).to.be.instanceOf(Filter) ;
      expect(kernel.container().has('$interpolate')).to.be.true ;
      expect(kernel.container().has('$rootScope')).to.be.true ;
      expect(kernel.get('$rootScope')).to.be.instanceOf(Scope) ;
      expect(kernel.container().has('$events')).to.be.true ;
      expect(kernel.get('$events')).to.be.instanceOf(Events) ;
      expect(kernel.container().has('$cache')).to.be.true ;
      expect(kernel.get('$cache')).to.be.instanceOf(Cache) ;
      expect(kernel.container().has('$tmp')).to.be.true ;
      expect(kernel.get('$tmp')).to.be.instanceOf(Tmp) ;
    }) ;

    it('initialise l\'environnement', function () {

      expect(kernel.get('$config').get('logLevel')).to.be.equal('normal') ;
    }) ;
  }) ;


  describe('container', function () {

    it('retourne le service container', function () {

      expect(kernel.container() instanceof ServiceContainer).to.be.equal(true) ;
    }) ;
  }) ;


  describe('get', function () {

    it('retourne un service', function () {

      let service = {} ;
      kernel.container().set('test', service) ;

      expect(kernel.get('test')).to.be.equal(service) ;
    }) ;
  }) ;


  describe('events', function () {

    it('retourne le service $events', function () {

      expect(kernel.events()).to.be.instanceof(Events) ;
    }) ;
  }) ;
}) ;

