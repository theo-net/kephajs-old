'use strict' ;

/**
 * src/core/KernelAccess.js
 */

const Kernel = require('./Kernel') ;

/**
 * En étendant cette classe, on permet aux enfants d'avoir une méthode
 * `kernel()` retournant le Kernel de l'Application.
 */
class KernelAccess {

  /**
   * Retourne l'instance de Kernel, ce dernier étant un singleton
   * @returns {Kernel}
   */
  kernel () {

    return new Kernel() ;
  }
}

module.exports = KernelAccess ;

