'use strict' ;

/**
 * src/core/Validate/Mail.js
 */

const Validator = require('./Validator') ;

/**
 * Teste si la valeur est une adresse mail
 */
class MailValidator extends Validator {

  isValid (value = null) {

    this.reset() ;

    let test = true ;

    if (!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)) // eslint-disable-line max-len
      test = this._addOccurredError('Doit être une adresse mail.') ;

    return test ;
  }
}

module.exports = MailValidator ;

