'use strict' ;

/**
 * src/core/Validate/NotEmpty.js
 */

const Validator = require('./Validator'),
      Utils     = require('../Utils') ;

/**
 * Teste si la valeur est n'est pas vide
 */
class NotEmptyValidator extends Validator {

  isValid (value = null) {

    this.reset() ;

    let test = true ;

    if (Utils.isEmpty(value))
      test = this._addOccurredError('Ne doit pas être vide.') ;

    return test ;
  }
}

module.exports = NotEmptyValidator ;

