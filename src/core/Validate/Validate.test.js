'use strict' ;

/**
 * src/core/Validate/Validate.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon') ;

const ValidateValidator = require('./Validate'),
      ValidateService   = require('../Services/Validate'),
      FloatValidator    = require('./Float'),
      FunctionValidator = require('./Function'),
      IntegerValidator  = require('./Integer'),
      ListValidator     = require('./List'),
      RegExpValidator   = require('./RegExp') ;

// buddy ignore:start
describe('ValidateValidator', function () {

  let validate ;

  beforeEach(function () {

    validate = new ValidateValidator() ;
  }) ;


  describe('addValidator', function () {

    it('retourne l\'objet', function () {

      expect(validate.addValidator(new IntegerValidator()))
        .to.be.equal(validate) ;
    }) ;

    it('ajoute le validateur', function () {

      let validator1 = new IntegerValidator(),
          validator2 = new IntegerValidator() ;

      validate.addValidator(validator1) ;
      validate.addValidator(validator2) ;

      expect(validate._validators[0]).to.be.equal(validator1) ;
      expect(validate._validators[1]).to.be.equal(validator2) ;
    }) ;

    it('Lance une erreur si on lui transmet pas un Validator', function () {

      let error = false ;

      try {
        validate.addValidator('lol') ;
      }
      catch (e) {
        error = true ;
      }

      expect(error).to.be.true ;
    }) ;
  }) ;


  describe('add', function () {

    it('returns self', function () {

      validate = new ValidateValidator({service: new ValidateService}) ;
      expect(validate.add('integer')).to.be.equal(validate) ;
    }) ;

    it('si on donne une RegExp, construit un RegExpValidator', function () {

      validate.add(/a/, 'un message') ;

      let validator = validate._validators[0] ;

      expect(validator).to.be.instanceOf(RegExpValidator) ;
      expect(validator._validatorArguments.regExp).to.be.deep.equal(/a/) ;
      expect(validator._validatorArguments.message())
        .to.be.equal('un message') ;
    }) ;

    it('si on donne une fonction, construit un FunctionValidator', function () {

      let fct = function () { return true ; } ;
      validate.add(fct, 'un message') ;

      let validator = validate._validators[0] ;

      expect(validator).to.be.instanceOf(FunctionValidator) ;
      expect(validator._validatorArguments.fct).to.be.equal(fct) ;
      expect(validator._validatorArguments.message())
        .to.be.equal('un message') ;
    }) ;

    it('on peut récupérer un validateur à partir du service', function () {

      validate = new ValidateValidator({service: new ValidateService}) ;
      validate.add('integer', {max: 5}) ;

      let validator = validate._validators[0] ;

      expect(validator).to.be.instanceOf(IntegerValidator) ;
      expect(validator._validatorArguments.max).to.be.equal(5) ;
    }) ;

    it('on récupère, grâce à un objet, le validateur depuis le service',
    function () {

      validate = new ValidateValidator({service: new ValidateService}) ;
      validate.add({integer: {max: 5}}) ;

      let validator = validate._validators[0] ;

      expect(validator).to.be.instanceOf(IntegerValidator) ;
      expect(validator._validatorArguments.max).to.be.equal(5) ;
    }) ;

    it('avec un tableau, on peut ajouter plusieurs validateurs', function () {

      validate = new ValidateValidator({service: new ValidateService}) ;
      validate.add([
        {integer: {min: 0}},
        {integer: {max: 5}}
      ]) ;

      let validator1 = validate._validators[0],
          validator2 = validate._validators[1] ;

      expect(validator1).to.be.instanceOf(IntegerValidator) ;
      expect(validator2).to.be.instanceOf(IntegerValidator) ;
      expect(validator1._validatorArguments.min).to.be.equal(0) ;
      expect(validator2._validatorArguments.max).to.be.equal(5) ;
    }) ;

    it('avec un tableau, on créé un ListValidator', function () {

      validate = new ValidateValidator({service: new ValidateService}) ;
      validate.add(['a', 'b']) ;

      let validator = validate._validators[0] ;

      expect(validator).to.be.instanceOf(ListValidator) ;
      expect(validator._validatorArguments.list)
        .to.be.deep.equal(['a', 'b']) ;
    }) ;
  }) ;


  describe('isValid', function () {

    it('exécute tous les validateurs', function () {

      let validator1 = new IntegerValidator(),
          validator2 = new IntegerValidator() ;
      let spy1 = sinon.spy(validator1, 'isValid'),
          spy2 = sinon.spy(validator2, 'isValid') ;

      validate.addValidator(validator1) ;
      validate.addValidator(validator2) ;
      validate.isValid(3) ;

      expect(spy1.calledWith(3)).to.be.true ;
      expect(spy2.calledWith(3)).to.be.true ;
    }) ;

    it('exécute tous les validateurs, même si un renvoit `false`', function () {

      let validator1 = new IntegerValidator(),
          validator2 = new FloatValidator() ;
      let spy1 = sinon.spy(validator1, 'isValid'),
          spy2 = sinon.spy(validator2, 'isValid') ;

      validate.addValidator(validator1) ;
      validate.addValidator(validator2) ;
      validate.isValid(3.1) ;

      expect(spy1.calledWith(3.1)).to.be.true ;
      expect(spy2.calledWith(3.1)).to.be.true ;
    }) ;

    it('renvoit `false` si un des validateur retourne `false`', function () {


      let validator1 = new IntegerValidator(),
          validator2 = new IntegerValidator({max: 3}) ;

      validate.addValidator(validator1) ;
      validate.addValidator(validator2) ;

      expect(validate.isValid(4)).to.be.equal(false) ;
    }) ;

    it('renvoit `true` si tous les validateurs retournent `true`', function () {

      let validator1 = new IntegerValidator(),
          validator2 = new IntegerValidator() ;

      validate.addValidator(validator1) ;
      validate.addValidator(validator2) ;

      expect(validate.isValid(4)).to.be.equal(true) ;
    }) ;

    it('sauve les erreurs de tous les validateurs', function () {


      let validator1 = new IntegerValidator() ;
      let validator2 = new FloatValidator({max: 3}) ;

      validate.addValidator(validator1) ;
      validate.addValidator(validator2) ;

      validate.isValid(3.5) ;

      expect(validate.getOccurredErrors()[0])
        .to.be.equal('Doit être un entier.') ;
      expect(validate.getOccurredErrors()[1])
        .to.be.equal('Doit être inférieur à 3.') ;
    }) ;

    it('liste des erreurs remise à zéro avant validation', function () {


      let validator1 = new IntegerValidator() ;
      let validator2 = new FloatValidator({max: 3}) ;

      validate.addValidator(validator1) ;
      validate.addValidator(validator2) ;

      validate.isValid(3.5) ;
      validate.isValid(2) ;

      expect(validate.getOccurredErrors().length).to.be.equal(0) ;
    }) ;
  }) ;

  describe('_getFromService', function () {

    it('retourne un validateur depuis le service', function () {

      validate = new ValidateValidator({service: new ValidateService}) ;
      let validator = validate._getFromService('integer', {max: 5}) ;

      expect(validator).to.be.instanceOf(IntegerValidator) ;
      expect(validator._validatorArguments.max).to.be.equal(5) ;
    }) ;

    it('envoit une erreur si n\'est pas lié au service', function () {

      let error = false ;
      validate = new ValidateValidator() ;

      try {
        validate._getFromService('truc', {max: 5}) ;
      } catch (e) {
        error = e.message ;
      }

      expect(error)
        .to.be.equal('Le validateur n\'est pas lié au service `validate` !') ;
    }) ;

    it('envoit une erreur si le validateur n\'existe pas dans le service',
    function () {

      let error = false ;
      validate = new ValidateValidator({service: new ValidateService}) ;

      try {
        validate._getFromService('truc', {max: 5}) ;
      } catch (e) {
        error = e.message ;
      }

      expect(error).to.be.equal(
        'Le service `validate` ne possède pas de validateur truc !') ;
    }) ;
  }) ;
}) ;
// buddy ignore:end

