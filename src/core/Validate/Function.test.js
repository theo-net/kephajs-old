'use strict' ;

/**
 * src/core/Validate/Function.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon') ;

const FunctionValidator = require('./Function') ;

describe('FunctionValidator', function () {

  it('execute the function to test the value', function () {

    let valid = sinon.spy() ;
    let validator = new FunctionValidator({fct: valid}) ;

    validator.isValid('a') ;

    expect(valid.calledWith('a')).to.be.true ;
  }) ;

  it('execute the function to generate the message', function () {

    let msg   = sinon.spy() ;
    let validator = new FunctionValidator({
      fct: () => { return 'return' ; }, message: msg
    }) ;

    validator.isValid('a') ;

    expect(msg.calledWith('a', 'return')).to.be.true ;
  }) ;

  it('return `true` si la fonction retourne `true`', function () {

    let validator = new FunctionValidator({
      fct: () => { return true ; }
    }) ;

    expect(validator.isValid('a')).to.be.equal(true) ;
  }) ;

  it('return `false` si la fonction retourne `false`', function () {

    let validator = new FunctionValidator({
      fct: () => { return false ; }
    }) ;

    expect(validator.isValid('a')).to.be.equal(false) ;
  }) ;

  it('remise à zéro des erreurs', function () {

    let validator = new FunctionValidator({
      fct: (val) => { return val > 1 ; }
    }) ;

    validator.isValid(0) ;
    validator.isValid(2) ;
    expect(validator.getOccurredErrors().length).to.be.equal(0) ;
  }) ;

}) ;

