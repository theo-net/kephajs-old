'use strict' ;

/**
 * src/core/Validate/List.js
 */

const Validator = require('./Validator'),
      Utils     = require('../Utils') ;

/**
 * Teste si la valeur est un élément de la liste
 *
 *    list: Liste des valeurs possibles, peut-être un `Array` ou une chaîne de
 *          caractères, dans ce cas, elle sera découpée selon les ','
 */
class ListValidator extends Validator {

  _setArguments () {

    this._arguments = {
      list: 'Liste des valeurs'
    } ;
  }


  isValid (value = null) {

    this.reset() ;

    let test = true ;

    if (Utils.isString(this._validatorArguments.list))
      this._validatorArguments.list = this._validatorArguments.list.split(',') ;

    if (this._validatorArguments.list.indexOf(value) === -1) {
      test = this._addOccurredError('Doit avoir une des valeurs suivantes: '
           + this._validatorArguments.list + '.') ;
    }

    return test ;
  }
}

module.exports = ListValidator ;

