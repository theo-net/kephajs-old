'use strict' ;

/**
 * src/core/Validate/Integer.test.js
 */

const expect = require('chai').expect ;

const IntegerValidator = require('./Integer') ;

// buddy ignore:start
describe('IntegerValidator', function () {

  it('check if value is integer', function () {

    let validator = new IntegerValidator() ;

    expect(validator.isValid()).to.be.false ;
    expect(validator.isValid(-1)).to.be.true ;
    expect(validator.isValid(21)).to.be.true ;
    expect(validator.isValid(NaN)).to.be.false ;
    expect(validator.isValid(true)).to.be.false ;
    expect(validator.isValid('foobar')).to.be.false ;
    expect(validator.isValid(/a/)).to.be.false ;
    expect(validator.isValid({truc: 1})).to.be.false ;
    expect(validator.isValid([1, 2, 3])).to.be.false ;
    expect(validator.isValid(-1.1)).to.be.false ;
    expect(validator.isValid(2.1)).to.be.false ;
    expect(validator.getOccurredErrors()[0])
      .to.be.equal('Doit être un entier.') ;
    expect(validator.isValid(0)).to.be.true ;
    expect(validator.getOccurredErrors().length).to.be.equal(0) ;
  }) ;

  it('check min value', function () {

    let validator = new IntegerValidator({
      min: -6,
      max: 5
    }) ;

    expect(validator.isValid(0)).to.be.true ;
    expect(validator.isValid(-1)).to.be.true ;
    expect(validator.isValid(4)).to.be.true ;
    expect(validator.isValid(-19)).to.be.false ;
    expect(validator.getOccurredErrors()[0])
      .to.be.equal('Doit être supérieur à -6.') ;
  }) ;

  it('check max value', function () {

    let validator = new IntegerValidator({
      min: -6,
      max: 5
    }) ;

    expect(validator.isValid(0)).to.be.true ;
    expect(validator.isValid(-1)).to.be.true ;
    expect(validator.isValid(21)).to.be.false ;
    expect(validator.getOccurredErrors()[0])
      .to.be.equal('Doit être inférieur à 5.') ;
    expect(validator.isValid(4)).to.be.true ;
  }) ;
}) ;
// buddy ignore:end

