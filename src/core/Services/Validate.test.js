'use strict' ;

/**
 * src/core/Services/Validate.test.js
 */

const expect = require('chai').expect ;

const Validate          = require('./Validate'),
      ValidateValidator = require('../Validate/Validate'),
      BooleanValidator  = require('../Validate/Boolean'),
      FloatValidator    = require('../Validate/Float'),
      FunctionValidator = require('../Validate/Function'),
      IntegerValidator  = require('../Validate/Integer'),
      ListValidator     = require('../Validate/List'),
      NotEmptyValidator = require('../Validate/NotEmpty'),
      NullValidator     = require('../Validate/Null'),
      RegExpValidator   = require('../Validate/RegExp'),
      StringValidator   = require('../Validate/String'),
      MailValidator     = require('../Validate/Mail') ;

describe('Validate', function () {

  let validate = null ;

  describe('make', function () {

    it('Renvoit un `Validator`', function () {

      validate = new Validate() ;
      expect(validate.make()).to.be.instanceOf(ValidateValidator) ;
    }) ;
  }) ;


  describe('getWithName', function () {

    it('returns BooleanValidator', function () {

      expect(validate.getWithName('boolean'))
        .to.be.instanceOf(BooleanValidator) ;
    }) ;

    it('returns FloatValidator', function () {

      expect(validate.getWithName('float'))
        .to.be.instanceOf(FloatValidator) ;
    }) ;

    it('returns FunctionValidator', function () {

      expect(validate.getWithName('function', {fct: () => { return true ; }}))
        .to.be.instanceOf(FunctionValidator) ;
    }) ;

    it('returns IntegerValidator', function () {

      expect(validate.getWithName('integer'))
        .to.be.instanceOf(IntegerValidator) ;
    }) ;

    it('returns ListValidator', function () {

      expect(validate.getWithName('list', {list: 'a,b'}))
        .to.be.instanceOf(ListValidator) ;
    }) ;

    it('returns NotEmptyValidator', function () {

      expect(validate.getWithName('notEmpty'))
        .to.be.instanceOf(NotEmptyValidator) ;
    }) ;

    it('returns NullValidator', function () {

      expect(validate.getWithName('null'))
        .to.be.instanceOf(NullValidator) ;
    }) ;

    it('returns RegExpValidator', function () {

      expect(validate.getWithName('regExp', {regExp: /a/}))
        .to.be.instanceOf(RegExpValidator) ;
    }) ;

    it('returns StringValidator', function () {

      expect(validate.getWithName('string'))
        .to.be.instanceOf(StringValidator) ;
    }) ;

    it('returns MailValidator', function () {

      expect(validate.getWithName('mail'))
        .to.be.instanceOf(MailValidator) ;
    }) ;
  }) ;

  describe('setValidator', function () {

    it('on définit un validator en fournissant sa class', function () {

      validate.setValidator('test', {class: IntegerValidator}) ;
      expect(validate.getWithName('test'))
        .to.be.instanceOf(IntegerValidator) ;
    }) ;

    it('on définit un validator en fournissant un objet', function () {

      let validator = new IntegerValidator() ;
      validate.setValidator('test', {object: validator}) ;

      expect(validate.getWithName('test')).to.be.equal(validator) ;
    }) ;
  }) ;
}) ;

