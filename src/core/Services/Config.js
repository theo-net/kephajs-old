'use strict' ;

/**
 * src/core/Services/Config.js
 */

const Utils = require('../Utils') ;

/**
 * Service permettant d'accéder à la configuration de l'application
 *
 *    let config = new Config() ;
 *    config.add({
 *      'param1': 'bar',
 *      'param2': 'foo%param1%
 *    }) ;
 *    let param = config.get('param2') ;
 *    console.log(param2) ; // foobar
 */
class Config {

  constructor () {

    this._vars = {} ;
  }


  /**
   * Définit un paramètre
   *
   * @param {String} id Identifiant du paramètre
   * @param {*} value Valeur du paramètre
   * @param {Boolean} [ecrase] Écrase ou non un paramètre portant le même
   *                  identifiant (vaut `false` par défaut).
   */
  set (id, value, ecrase = false) {

    if (!ecrase && this._vars[id] !== undefined)
      return ;

    this._vars[id] = value ;
  }


  /**
   * Retourne un paramètre ou la valeur par défaut si celle-ci est spécifiée
   * et si le paramètre n'est pas initialisé.
   * En formatant le retour, on remplace une constante par sa valeur.
   *
   * @param {String} id Identifiant
   * @param {*} [dflt=null] Valeur par défaut
   * @param {Boolean} [format=false] Doit-on formater la sortie
   * @returns {*}
   */
  get (id, dflt = null, format = true) {

    if (/.+\.\*$/.test(id)) {

      let root    = id.substr(0, id.length - 1),
          results = {} ;

      Utils.forEach(this._vars, (value, id) => {

        if (id.substr(0, root.length) == root)
          results[id.substr(root.length)] = this.get(id) ;
      }) ;

      if (Utils.isObject(dflt))
        results = Utils.extend({}, dflt, results) ;

      return results ;
    }
    else {
      return this.has(id) ? (
          format && Utils.isString(this._vars[id]) ?
            this._vars[id].replace(
              /%([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]+)%/g,
              (match, p1) => { return this._vars[p1] ; }
            ) : this._vars[id]
          ) : (Utils.isFunction(dflt) ? dflt() : dflt) ;
    }
  }


  /**
   * Indique si un élément est présent ou non
   * @param {String} id Identifiant de l'élément à tester
   * @returns {Boolean}
   */
  has (id) {

    return Object.keys(this._vars).indexOf(id) > -1 ;
  }


  /**
   * Supprime un élément. On peut supprimer une série avec `foo.bar.*` comme
   * valeur d'identifiant.
   * @param {String} id Identifiant de l'élément à supprimer
   * @param {Function} [fct] Fonction de suppression (on a pas à l'utiliser)
   */
  delete (id, fct) {

    if (!Utils.isFunction(fct))
      fct = elmt => { delete this._vars[elmt] ; } ;

    if (/.+\.\*$/.test(id)) {

      let root = id.substr(0, id.length - 1) ;
      Utils.forEach(this._vars, (value, id) => {

        if (id.substr(0, root.length) == root)
          fct(id) ;
      }) ;
    }
    else
      fct(id) ;
  }


  /**
   * Initialise une série de paramètres
   * @param {Object} parameters Tableau de paramètres (les clés sont l'id)
   * @param {Boolean} [ecrase=false] Écrase ou non un élément précédent
   */
  add (parameters, ecrase = false) {

    Utils.forEach(parameters, (value, id) => {
      this.set(id, value, ecrase) ;
    }) ;
  }


  /**
   * Retourne tous les éléments de la configuration
   * @returns {Object}
   */
  getAll () {

    return this._vars ;
  }


  /**
   * Remet à zéro toute la configuration
   */
  reset () {

    delete this._vars ;
    this._vars = {} ;
  }
}

module.exports = Config ;

