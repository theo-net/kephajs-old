'use strict' ;

/**
 * src/core/Services/ServiceContainer.js
 */

const Utils = require('../Utils') ;

/**
 * Permet l'implémentation de l'injection de dépendances.
 * On parle de « service » pour tous les objets partagés dans l'application.
 *
 *    let services = new ServiceContainer() ;
 *    services.set('name', new AService()) ;
 *    services.setServices([
 *      {name: 'service2', class: 'ServiceB'},
 *      {name: 'service3', class: 'ServiceC'},
 *      // ...
 *    ]) ;
 *
 * Si le service est une fonction, celle-ci sera appelée à chaque `get` avec le
 * Kernel comme argument.
 */
class ServiceContainer {

  /**
   * Initialise le service container.
   * @param {Object} kernel Kernel de l'application
   */
  constructor (kernel) {

    this._kernel = kernel ;
    this._services = [] ;
  }


  /**
   * Défini un service
   *
   * @param {String} name Nom du service
   * @param {Object} service Service à initialiser
   */
  set (name, service) {

    this._services[name] = service ;
  }


  /**
   * Retourne un service. Si le service est une fonction, l'exécute avec le
   * Kernel comme argument et retourne le résultat.
   *
   * @param {String} name Nom du service
   * @return {Object|*}
   */
  get (name) {

    if (Utils.isFunction(this._services[name]))
      return this._services[name](this._kernel) ;

    return this._services[name] ;
  }


  /**
   * Indique si un service est défini
   *
   * @param {String} name Nom du service
   * @return {Boolean}
   */
  has (name) {

    return Object.keys(this._services).indexOf(name) > -1 ;
  }


  /**
   * Retourne tous les services
   *
   * @return {Array}
   */
  getAll () {

    return this._services ;
  }
}

module.exports = ServiceContainer ;

