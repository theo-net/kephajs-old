'use strict' ;

/**
 * src/core/Services/interpolate.js
 */

let Utils = require('../Utils') ;


// Symboles de début et de fin d'expression
const startSymbol = '{{',
      endSymbol   = '}}' ;

function createInterpolate (kernel) {

  // récupère le parser
  const parse = kernel.get('$parser') ;

  // Les helpers
  let helpers = {

    if: function (item, content, context) {

      if (item)
        return content(context) ;
      else
        return '' ;
    },
    each: function (items, content, context) {

      let out = '' ;

      Utils.forEach(items, item => {
        out += content(Utils.extend({}, context, item)) ;
      }) ;

      return out ;
    }
  } ;

  /**
   * Prend en entrée une chaîne de caractères text et retourne une fonction
   * qui retournera le résultat parsé. L'idée est de prendre en entrée une
   * chaîne de caractères et de parser toutes les expressions (qui sont par
   * défaut isolées du reste de la chaîne par {{ et }}) grace au parseur.
   * Une fonction est alors retournée :
   *   return interpolationFn (context)
   * celle-ci prend en paramètre un objet qui sera transmis au parseur
   * afin de retourner une chaîne de carctère parsée.
   *
   * Tout ce qui est entre {{!-- --}} est considéré comme commentaire et est
   * donc supprimé.
   *
   * Le contenu entre {{ et }} est parsé à l'aide du service $parse, les blocs
   * {{#qqc }} du contenu {{/#qqc}} sont traités à l'aide d'helpers.
   *
   * {{#if test}}Retourné si test vrai{{/if}}
   *
   * {{#each items}}Expression bouclées avec chaques éléments d'items{{/each}}
   *
   * Si mustHaveExpressions vaut true, alors la méthode ne renvera une
   * fonction que si elle trouve une expression dans text, sinon rien ne
   * sera retourné.
   *
   * Si une expression est une Promise, la fonction renverra une Promise.
   * @param {String} text Expression à parser
   * @param {Boolean} mustHaveExpressions Cf. présentation
   * @returns {Function}
   */
  return function interpolate (text, mustHaveExpressions) {

    // On y récupèrera tous les morceaux de text (strings et functions)
    let parts = [] ;
    // et ici la liste des expressions et des fonctions correspondantes
    let expressions         = [] ;
    let expressionFns       = [] ;
    let expressionPositions = [] ;

    let index = 0 ;
    let startIndex, endIndex, exp, expFn ;


    /**
     * @param {Array} values Valeurs à assembler
     * @param {Boolean} [promises=false] Y'a-t-il des promises
     * @returns {String|Promise}
     * @private
     */
    function _compute (values, promises = false) {

      // Si on est face à une promise, on a du taff
      if (promises) {

        return Promise.all(values).then((values) => {
          return _compute(values, false) ;
        }) ;
      }
      else {
        values.forEach((value, i) => {
          parts[expressionPositions[i]] = _stringify(value) ;
        }) ;

        return parts.join('') ;
      }
    }

    // On supprime tous les commentaires
    text = text.replace(/\{\{!--.*?--\}\}/g, '') ;

    // On parcours toute la chaîne de caractère afin de traiter toutes les
    // expressions.
    while (index < text.length) {

      // On regarde si text contient les marqueurs {{ et }} (dans l'ordre)
      startIndex = text.indexOf(startSymbol, index) ;
      if (startIndex !== -1)
        endIndex = text.indexOf(endSymbol, startIndex + startSymbol.length) ;

      // On isole l'expression et on récupère le parseur
      if (startIndex !== -1 && endIndex !== -1) {

        // texte avant l'expression
        if (startIndex !== index)
          parts.push(_unescapeText(text.substring(index, startIndex))) ;

        // ouverture d'un bloc
        if (text.charAt(startIndex + startSymbol.length) == '#') {

          // On fait pointer startIndex sur l'identifiant du bloc
          let startBloc = startIndex ;
          startIndex = startIndex + startSymbol.length + 1 ;
          // On récupère les infos du bloc
          let separator = text.substring(startIndex, endIndex).indexOf(' ') ;
          let blocName = text.substring(startIndex, startIndex + separator) ;
          let blocExp = text.substring(startIndex + separator + 1, endIndex) ;

          // On fait pointer endIndex sur la fin de la balise d'ouverture
          endIndex = endIndex + endSymbol.length ;

          // On regarde s'il y a un bloc inclu
          let nbBlocImbriqued = 0 ;
          let lastBloc = endIndex ;
          let blocImbriqued = lastBloc
            + text.substring(endIndex).indexOf(startSymbol + '#' + blocName) ;
          let endBloc = endIndex
            + text.substring(endIndex)
              .indexOf(startSymbol + '/' + blocName + endSymbol) ;
          while (blocImbriqued !== lastBloc && blocImbriqued < endBloc) {

            nbBlocImbriqued++ ;
            lastBloc = blocImbriqued ;
            blocImbriqued = lastBloc + 1
              + text.substring(lastBloc).indexOf(startSymbol + '#' + blocName) ;
          }
          // On cherche la balise de fermeture correspondant à notre bloc
          // (la première a déjà été isolée)
          if (nbBlocImbriqued > 0)
            nbBlocImbriqued-- ;
          while (nbBlocImbriqued--) {

            endBloc = endBloc + 1
              + text.substring(endBloc + 1)
                .indexOf(startSymbol + '/' + blocName + endSymbol) ;
          }

          // On isole le bloc
          let blocContent = text.substring(endIndex, endBloc) ;
          endBloc = endBloc
            + text.substring(endBloc).indexOf(endSymbol) + endSymbol.length ;

          // On ajoute le bloc
          exp = text.substring(startBloc, endBloc) ;
          let item = parse(blocExp) ;
          expFn = function (vars) {

            // Fonction déclarée par l'utilisateur
            if (Utils.isFunction(vars[blocName])) {
              return vars[blocName](
                item(vars), interpolate(blocContent), vars
              ) ;
            }
            // Helper disponible ici
            else if (Utils.isFunction(helpers[blocName])) {
              return helpers[blocName](
                item(vars), interpolate(blocContent), vars
              ) ;
            }
            else
              return '' ;
          } ;
          expressions.push(exp) ;
          expressionFns.push(expFn) ;
          expressionPositions.push(parts.length) ;
          parts.push(expFn) ;

          // On passe à la suite
          index = endBloc ;
        }
        // ce n'est pas un bloc
        else {

          // On traite l'expression
          exp = text.substring(startIndex + startSymbol.length, endIndex) ;
          expFn = parse(exp) ;
          expressions.push(exp) ;
          expressionFns.push(expFn) ;
          expressionPositions.push(parts.length) ;
          parts.push(expFn) ;

          // On passe à la suite
          index = endIndex + endSymbol.length ;
        }
      }
      // S'il n'y a plus d'expression, on arrête la boucle
      else {

        parts.push(_unescapeText(text.substring(index))) ;
        break ;
      }
    }

    // Fonction retournée par $interpolate()
    if (expressions.length || !mustHaveExpressions) {
      return Utils.extend(function interpolationFn (context) {
        let promises = false ;
        let values = expressionFns.map(function (expressionFn) {

          let exp = expressionFn(context) ;
          if (exp instanceof Promise) promises = true ;
          return exp ;
        }) ;
        return _compute(values, promises) ;
      }, {
        expressions: expressions,
        _watchDelegate: function (scope, listener) {

          let lastValue ;

          return scope.$watchGroup(expressionFns,
            function (newValues, oldValues) {
              let newValue = _compute(newValues) ;
              listener(
                newValue,
                (newValues === oldValues ? newValue : lastValue),
                scope
              ) ;
              lastValue = newValue ;
            }
          ) ;
        }
      }) ;
    }
  } ;
}
module.exports = createInterpolate ;


// Échapement des caractères
const escapedStartMatcher =
  new RegExp(startSymbol.replace(/./g, _escapeChar), 'g') ;
const escapedEndMatcher =
  new RegExp(endSymbol.replace(/./g, _escapeChar), 'g') ;

/**
 * escapeChar
 * @param {String} aChar caractère à échaper
 * @returns {String}
 * @private
 */
function _escapeChar (aChar) {

  return '\\\\\\' + aChar ;
}

/**
  * Déséchappe les caractères de text (\{ -> { et \} -> })
  * @param {String} text Texte à traiter
  * @returns {String}
 * @private
  */
function _unescapeText (text) {

  return text.replace(escapedStartMatcher, startSymbol)
             .replace(escapedEndMatcher,   endSymbol) ;
}


/**
 * Transforme `value` en une chaîne de caractères cohérente :
 *   - null et undefined deviennent ''
 *   - les nombres et les booléens deviennent un string
 *   - les arrays et les objects deviennent du JSON
 * @param {*} value Valeur à transformer
 * @returns {String}
 * @private
 */
function _stringify (value) {

  if (Utils.isNull(value) || Utils.isUndefined(value))
    return '' ;
  else if (Utils.isObject(value))
    return JSON.stringify(value) ;
  else
    return '' + value ;
}

