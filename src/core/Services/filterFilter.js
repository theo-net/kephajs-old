'use strict' ;

/**
 * src/core/Services/filter_filter.js
 */

const Utils = require('../Utils') ;

/**
 * Est la fonction exportée. Elle retourne le filtre 'filter' qui filtre un
 * tableau.
 *    filter:filterExpr:comparator
 * @param {Array} array Tableau à filtrer
 * @param {String} filterExpr expression qui sert de filtre
 * @param {Function} comparator fonction de comparaison
 * @returns {Array}
 */
function filterFilter (array, filterExpr, comparator) {


  /**
   * createPredicateFn (expression, comparator)
   *
   * Créé la fonction de comparaison qui est appliqué au tableau.
   * Si comparator vaut true, alors la fonction utilisée sera Utils.isEqual,
   * si comparator est une fonction, alors on utilisera celle-ci.
   * @param {String} expression Expression
   * @param {Boolean|Function} comparator Si vaut `true`, fonction de
   *    comparaison on utilise Utils.isEqal
   * @returns {Function}
   */
  function createPredicateFn (expression, comparator) {

    let shouldMatchPrimitives =
      Utils.isObject(expression) && ('$' in expression) ;

    // Si comparator vaut true
    if (comparator === true)
      comparator = Utils.isEqual.bind(Utils) ; // les this internes fonctionnent
    // On a fourni une fonction de comparaison
    else if (!Utils.isFunction(comparator)) {
      comparator = function (actual, expected) {

        // Cas d'undefined
        if (Utils.isUndefined(actual))
          return false ;
        // Cas du null
        if (Utils.isNull(actual) || Utils.isNull(expected))
          return actual === expected ;
        // Sinon
        actual = ('' + actual).toLowerCase() ;
        expected = ('' + expected).toLowerCase() ;
        return actual.indexOf(expected) !== -1 ;
      } ;
    }

    return function predicateFn (item) {

      if (shouldMatchPrimitives && !Utils.isObject(item))
        return deepCompare(item, expression.$, comparator) ;
      return deepCompare(item, expression, comparator, true) ;
    } ;
  }


  // deepCompare
  function deepCompare (actual, expected, comparator, matchAnyProperty,
    inWildcard) {

    // "!aString"
    if (Utils.isString(expected) && expected.startsWith('!')) {
      return !deepCompare(actual, expected.substring(1),
                          comparator, matchAnyProperty) ;
    }
    // Actual est un tableau
    if (Array.isArray(actual)) {
      return actual.some(function (actualItem) {
        return deepCompare(actualItem, expected,
                           comparator, matchAnyProperty) ;
      }) ;
    }
    // Actual est un objet
    if (Utils.isObject(actual)) {
      // Si expected est un objet
      if (Utils.isObject(expected) && !inWildcard) {
        return Object.getOwnPropertyNames(expected).every(
          function (property) {
            if (Utils.isUndefined(expected[property]))
              return true ;
            let isWildcard = (property === '$') ;
            let actualVal = isWildcard ? actual : actual[property] ;
            return deepCompare(actualVal, expected[property],
                               comparator, isWildcard, isWildcard) ;
          }
        ) ;
      }
      // Sinon
      else if (matchAnyProperty) {
        return Object.getOwnPropertyNames(actual).some(function (key) {
          return deepCompare(actual[key], expected,
                             comparator, matchAnyProperty) ;
        }) ;
      }
      else
        return comparator(actual, expected) ;
    }
    // Sinon (string, number, boolean, ...)
    else
      return comparator(actual, expected) ;
  }

  // Filter

  let predicateFn ;

  if (Utils.isFunction(filterExpr))
    predicateFn = filterExpr ;
  else if (Utils.isString(filterExpr)  ||
           Utils.isNumber(filterExpr)  ||
           Utils.isBoolean(filterExpr) ||
           Utils.isNull(filterExpr)    ||
           Utils.isObject(filterExpr))
    predicateFn = createPredicateFn(filterExpr, comparator) ;
  else
    return array ;

  return array.filter(predicateFn) ;
}

module.exports = filterFilter ;

