'use strict' ;

/**
 * src/core/Services/Tmp.js
 */

const Config = require('./Config') ;

/**
 * Service permettant de stocker des données temporaires (elles seront effacées
 * si l'application redémarre.
 * Il comporte les mêmes méthodes que le service `$config`
 */
class Tmp extends Config {
}

module.exports = Tmp ;

