'use strict' ;

/**
 * src/core/Services/Events.test.js
 */


const expect = require('chai').expect,
      sinon  = require('sinon') ;

const Events = require('./Events'),
      Event  = require('./Event') ;

describe('core/Services/Events', function () {

  let events ;

  beforeEach(function () {

    events = new Events() ;
  }) ;


  it('register enregistre un nouveau canal d\'évènement', function () {

    events.register('foobar') ;

    expect(events._events.foobar).to.be.instanceof(Event) ;
    expect(events._events.foobar._id).to.be.equal('foobar') ;
  }) ;

  it('attach attache un observer à un canal', function () {

    events.register('foobar') ;
    let observer = function () {} ;

    events.attach('foobar', 'lol', observer) ;
    expect(events._events.foobar._observers.lol).to.be.equal(observer) ;
  }) ;

  it('notify notifie tous les observers d\'un canal et transmet les données',
  function () {

    events.register('foobar') ;
    events.register('lol') ;
    let spy1 = sinon.spy() ;
    let spy2 = sinon.spy() ;
    let spy3 = sinon.spy() ;
    let data = {a: 1, b: 2} ;

    events.attach('foobar', 'spy1', spy1) ;
    events.attach('foobar', 'spy2', spy2) ;
    events.attach('lol', 'spy3', spy3) ;

    events.notify('foobar', data) ;

    expect(spy1.calledOnce).to.be.true ;
    expect(spy1.calledWith(data)).to.be.true ;
    expect(spy2.calledOnce).to.be.true ;
    expect(spy2.calledWith(data)).to.be.true ;
    expect(spy3.calledOnce).to.be.false ;
  }) ;

  it('has indique si un canal est enregistré', function () {

    events.register('foobar') ;

    expect(events.has('foobar')).to.be.equal(true) ;
    expect(events.has('test')).to.be.equal(false) ;
  }) ;
}) ;

