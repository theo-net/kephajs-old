'use strict' ;

/**
 * src/core/Services/Logs.js
 */

const KernelAccess = require('../KernelAccess') ;

/**
 * Gestion des logs de l'application
 *
 * Association type <--> logLevel :
 *
 *  - `normal` : `info`, `success`, `error`
 *  - `warn` : `normal` + `warning`
 *  - `debug` : `warn` + `debug` + `data`
 */
class Logs extends KernelAccess {

  /**
   * Initialisation du service
   */
  constructor () {

    super() ;
  }


  /**
   * Traite un message de log
   * @param {String} message Message
   * @param {String} [type='info'] Type
   *    (`info`, `success`, `warning`, `error`, `debug`)
   */
  log (message, type = 'info') {

    let logLevel = this._getLogLevel() ;

    if (type == 'info')
      message = 'Info: ' + message ;
    else if (type == 'warning')
      message = 'Warning: ' + message ;

    if (this.kernel().container().has('$color')) {

      let $color = this.kernel().container().get('$color') ;

      if (type == 'info')
        message = $color.blue(message) ;
      else if (type == 'success')
        message = $color.green(message) ;
      else if (type == 'warning')
        message = $color.yellow(message) ;
      else if (type == 'debug')
        message = $color.cyan(message) ;
      else if (type == 'data')
        message = $color.magenta(message) ;
    }

    if (type == 'info')
      console.log(message) ;
    else if (type == 'success')
      console.log(message) ;
    else if (type == 'error')
      console.error(message) ;
    else if (type == 'warning' && (logLevel == 'warn' || logLevel == 'debug'))
      console.log(message) ;
    else if (type == 'debug' && logLevel == 'debug')
      console.log(message) ;
    else if (type == 'data' && logLevel == 'debug')
      console.log(message) ;
  }


  /**
   * Retourne le logLevel
   * @returns {String}
   */
  _getLogLevel () {

    return this.kernel().get('$config').get('logLevel') ;
  }
}

module.exports = Logs ;

