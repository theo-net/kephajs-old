'use strict' ;

/**
 * src/core/Services/Cache.test.js
 */

let expect = require('chai').expect ;

const Cache  = require('./Cache'),
      Config = require('./Config') ;

describe('Cache', function () {

  let cache = null ;

  beforeEach(function () {

    cache = new Cache() ;
  }) ;

  it('étend Config', function () {

    expect(cache).to.be.instanceOf(Config) ;
  }) ;

  it('par défaut, cache activé', function () {

    expect(cache.isEnabled()).to.be.true ;
  }) ;

  it('active ou désactive le cache', function () {

    cache.enable(false) ;
    expect(cache.isEnabled()).to.be.false ;
    cache.enable(true) ;
    expect(cache.isEnabled()).to.be.true ;
  }) ;

  describe('set', function () {

    it('une date d\'expiration peut être définie', function () {

      cache.set('foo', 'bar', 100) ;
      expect(cache._vars.foo).to.be.equal('bar') ;
      expect(cache._exp.foo).to.be.equal(Date.now() + 100 * 1000) ;
    }) ;

    it('par défaut, pas d\'expiration', function () {

      cache.set('foo', 'bar') ;
      expect(cache._vars.foo).to.be.equal('bar') ;
      expect(cache._exp.foo).to.be.equal(null) ;
    }) ;

    it('écrasera toujours une donnée déjà en cache', function () {

      cache.set('foo', 'bar', 10) ;
      cache.set('foo', 'bar2', 20) ;
      expect(cache._vars.foo).to.be.equal('bar2') ;
      expect(cache._exp.foo).to.be.equal(Date.now() + 20 * 1000) ;
    }) ;
  }) ;


  describe('has', function () {

    it('renverra false et supprimera l\'élément si expiré', function () {

      cache.set('foo', 'bar', 10) ;
      cache.set('foo2', 'bar2', -10) ;
      cache.set('foo3', 'bar3') ;
      expect(cache.has('foo')).to.be.true ;
      expect(cache.has('foo2')).to.be.false ;
      expect(cache.has('foo3')).to.be.true ;
      expect(cache._vars.foo2).to.be.undefined ;
      expect(cache._exp.foo2).to.be.undefined ;
    }) ;

    it('toujours `false` si cache désactivé', function () {

      cache.set('foo', 'bar') ;
      expect(cache.has('foo')).to.be.true ;
      cache.enable(false) ;
      expect(cache.has('foo')).to.be.false ;
    }) ;
  }) ;


  describe('get', function () {

    it('met en cache la valeur par défaut', function () {

      cache.set('foo', 'bar') ;
      expect(cache.has('bar')).to.be.false ;
      expect(cache.get('bar', 'foo')).to.be.equal('foo') ;
      expect(cache.has('bar')).to.be.true ;
      expect(cache.get('bar', 'truc')).to.be.equal('foo') ;
      expect(cache.get('truc')).to.be.equal(null) ;
      expect(cache.has('truc')).to.be.false ;
    }) ;

    it('date d\'expiration définie pour la valeur par défaut', function () {

      cache.set('foo', 'bar') ;
      cache.get('bar', 'foo') ;
      expect(cache._exp.bar).to.be.equal(null) ;
      cache.get('truc', 'muche', 10) ;
      expect(cache._exp.truc).to.be.equal(Date.now() + 10 * 1000) ;
    }) ;

    it('fonctionne avec les séries foo.bar.*', function () {

      cache.set('foo.truc', 1) ;
      cache.set('foo.bar.a', 1) ;
      cache.set('foo.bar.b', 1) ;
      cache.set('foo.bar.c.d', 1) ;
      cache.set('foo.bar.c.e', 1) ;
      cache.set('foo.bar.c.f.g', 1) ;

      expect(cache.get('foo.bar.*')).to.be.deep.equal({
        a: 1, b: 1, 'c.d': 1, 'c.e': 1, 'c.f.g': 1
      }) ;
    }) ;

    it('On veille à l\'expiration avec les séries', function () {

      cache.set('foo.truc', 1) ;
      cache.set('foo.bar.a', 1, 10) ;
      cache.set('foo.bar.b', 1, -10) ;
      cache.set('foo.bar.c.d', 1) ;
      cache.set('foo.bar.c.e', 1) ;
      cache.set('foo.bar.c.f.g', 1) ;

      expect(cache.get('foo.bar.*')).to.be.deep.equal({
        a: 1, 'c.d': 1, 'c.e': 1, 'c.f.g': 1
      }) ;
      expect(cache._vars['foo.bar.b']).to.be.undefined ;
      expect(cache._exp['foo.bar.b']).to.be.undefined ;
    }) ;

    it('pour série merge val par déf, mais pas de cache', function () {

      cache.set('foo.truc', 1) ;
      cache.set('foo.bar.a', 1) ;
      cache.set('foo.bar.c.d', 1) ;
      cache.set('foo.bar.c.e', 0) ;
      cache.set('foo.bar.c.f.g', 1) ;

      expect(cache.get('foo.bar.*', {b: 1, 'c.e': 1})).to.be.deep.equal({
        a: 1, b: 1, 'c.d': 1, 'c.e': 0, 'c.f.g': 1
      }) ;
      expect(cache.has('foo.bar.b')).to.be.false ;
    }) ;
  }) ;


  describe('delete', function () {

    it('netoie aussi les dates d\'expiration', function () {

      cache.set('foo', 'bar') ;
      cache.delete('foo') ;
      expect(cache._vars.foo).to.be.undefined ;
      expect(cache._exp.foo).to.be.undefined ;
    }) ;

    it('fonctionne bien avec les séries foo.bar.*', function () {

      cache.set('foo.truc', 1) ;
      cache.set('foo.bar.a', 1) ;
      cache.set('foo.bar.b', 1) ;
      cache.set('foo.bar.c.d', 1) ;
      cache.set('foo.bar.c.e', 1) ;
      cache.set('foo.bar.c.f.g', 1) ;
      cache.delete('foo.bar.*') ;

      expect(cache.has('foo.truc')).to.be.true ;
      expect(cache.has('foo.bar.a')).to.be.false ;
      expect(cache.has('foo.bar.b')).to.be.false ;
      expect(cache.has('foo.bar.c.d')).to.be.false ;
      expect(cache.has('foo.bar.c.e')).to.be.false ;
      expect(cache.has('foo.bar.c.f.g')).to.be.false ;

      expect(cache._exp['foo.truc']).to.be.not.undefined ;
      expect(cache._exp['foo.bar.a']).to.be.undefined ;
      expect(cache._exp['foo.bar.b']).to.be.undefined ;
      expect(cache._exp['foo.bar.c.d']).to.be.undefined ;
      expect(cache._exp['foo.bar.c.e']).to.be.undefined ;
      expect(cache._exp['foo.bar.c.f.g']).to.be.undefined ;
    }) ;
  }) ;
}) ;

