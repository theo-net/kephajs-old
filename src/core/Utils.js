'use strict' ;

/**
 * src/core/Utils.js
 */

const crypto = require('crypto') ;

/*
 * Différents outils
 * Certaines méthodes sont inspirées de Lodash (Licence MIT) et leurs tests
 * également. Nous ne reprenons pas la bibliothèque en entier, car de
 * nombreuses méthodes ne nous sont pas utiles. La compatibilité n'est pas
 * assurée, car n'étant pas un pro, j'ai supprimé des tests si je n'arrivais pas
 * à reproduire certains comportements.
 */
class Utils {

  /**
   * Test si `value` est une chaine de caractères
   *
   * @category Lang
   * @param {*} value valeur à tester
   * @returns {boolean}
   * @author Lodash
   * @example
   *
   * Utils.isString('abc') ;
   *   // => true
   * Utils.isString(1)
   *   // => false
   */
  static isString (value) {

    return typeof value === 'string'  ||
         (this.isObjectLike(value) &&
           Object.prototype.toString.call(value) == TAG.string
         ) ;
  }


  /**
   * Test si `value` est considérable comme un objet (non `null` et le
   * `typeof` est `object`.
   *
   * @category Lang
   * @param {*} value valeur à tester
   * @return {boolean}
   * @author Lodash
   * @example
   *
   * Util.isObjectLike({}) ;
   *   // => true
   * Util.isObjectLike([1, 2, 3]) ;
   *   // => true
   * Util.isObjectLike(Util.noop) ;
   *   // => false
   * Util.isObjectLike(null) ;
   *   // => false
   */
  static isObjectLike (value) {

    return !!value && typeof value == 'object' ;
  }


  /**
   * Test si `value` est un objet
   *
   * @category Lang
   * @param {*} value valeur à tester
   * @return {boolean}
   * @author Lodash
   */
  static isObject (value) {

    let type = typeof value ;
    return !!value && (type == 'object' || type == 'function') ;
  }


  /**
   * Test si `value` est un objet `Date`
   *
   * @category Lang
   * @param {*} value valeur à tester
   * @return {boolean}
   * @author Lodash
   */
  static isDate (value) {

    return value instanceof Date ;
  }


  /**
   * Test si `value` est une `RegExp`
   *
   * @category Lang
   * @param {*} value valeur à tester
   * @return {boolean}
   * @author Lodash
   */
  static isRegExp (value) {

    return value instanceof RegExp ;
  }


  /**
   * `value` est-il un nombre ?
   *
   * @category Lang
   * @param {*} value valeur à tester
   * @param {Boolean} [convert=false] Si `true` `'123'` sera un nombre
   * @return {boolean}
   * @author Lodash
   */
  static isNumber (value, convert = false) {

    return typeof value === 'number'  ||
         (this.isObjectLike(value) &&
          Object.prototype.toString.call(value) == TAG.number
         ) || (convert &&
           (/^0x[0-9a-f]+$/i.test(value) ||
            /^[-+]?(?:\d+(?:\.\d*)?|\.\d+)(e[-+]?\d+)?$/.test(value))) ;
  }


  /**
   * `value` est-elle un `boolean` ?
   *
   * @category Lang
   * @param {*} value valeur à tester
   * @return {boolean}
   * @author Lodash
   */
  static isBoolean (value) {

    return value === true  || value === false ||
         (this.isObjectLike(value) &&
          Object.prototype.toString.call(value) == TAG.bool
         ) ;
  }


  /**
   * `value` est-elle une fonction ?
   *
   * @category Lang
   * @param {*} value valeur à tester
   * @return {boolean}
   * @author Lodash
   */
  static isFunction (value) {

    return this.isObject(value) &&
      Object.prototype.toString.call(value) == TAG.func ;
  }


  /**
   * `value` est-elle `null` (égalité stricte)
   *
   * @category Lang
   * @param {*} value valeur à tester
   * @return {boolean}
   * @author Lodash
   */
  static isNull (value) {

    return value === null ;
  }


  /**
   * `value` est-elle considérable à un Json à parser. Retournera  `true`
   * si `value` est un objet simple (ex: `{a: 1}`)
   *
   * @category Lang
   * @param {*} value valeur à tester
   * @return {boolean}
   */
  static isJsonLike (value) {

    if (typeof value == 'symbol')
      return false ;
    if (/^\{(?!\{)/.test(value))
      return /\}$/.test(value) ;
    else if (/^\[/.test(value))
      return /\]$/.test(value) ;
    else
      return false ;
  }


  /**
   * `value` vaut-elle `undefined`
   *
   * @category Lang
   * @param {*} value valeur à tester
   * @return {boolean}
   * @author Lodash
   */
  static isUndefined (value) {

    return value === undefined ;
  }


  /**
   * `value` est-elle vide ?
   *
   * @category Lang
   * @param {*} value valeur à tester
   * @return {boolean}
   * @author Lodash
   */
  static isEmpty (value) {

    if (value === null)
      return true ;
    else if (Array.isArray(value) || Utils.isString(value))
      return value.length === 0 ;
    else if (Utils.isRegExp(value))
      return true ;
    else if (Utils.isObject(value))
      return !Object.getOwnPropertyNames(value).length ;
    else
      return true ;
  }


  /**
   * Ne fait rien
   *
   * @category Util
   * @return {undefined}
   * @author Lodash
   */
  static noop () { }


  /**
   * Invoque `n` fois `callback`
   *
   * @category Util
   * @param {number} n nombre de fois que la fonction callback doit être exécutée
   * @param {Function} [callback] (i) : fonction appelée avec i qui varie de 0
   *        à n-1, si n'est pas définie ou n'est pas une fonction, remplace par
   *        une fonction renvoyant le paramètre (identity)
   * @param {*} [thisArg] this binding
   * @returns {Array} Tableau des résultats
   * @author Lodash
   */
  static times (n, callback, thisArg) {

    let result = [] ;

    if (!n || n < 0 || n === Infinity)
      return [] ;

    if (!Utils.isFunction(callback)) {
      for (let i = 0 ; i < Math.floor(n) ; i++)
        result[i] = i ;
      return result ;
    }

    for (let i = 0 ; i < Math.floor(n) ; i++) {
      if (thisArg)
        result[i] = callback.call(thisArg, i) ;
      else
        result[i] = callback(i) ;
    }

    return result ;
  }


  /**
   * Compare value et other pour déterminer s'ils sont équivalents. Fonctionne
   * avec des arrays, booleans, Date objects, nombres, Object objects, regexp
   * et string. Pour cette fonction, NaN est égale à lui-même.
   *
   * @category Lang
   * @param {*} value valeur à comparer
   * @param {*} other l'autre valeur à comparer
   * @param {Function} [customizer] fonction de comparaison
   * @param {*} [thisArg] binding pour customizer
   * @param {*} [stackA=[]] Pour détecter la récursivité
   * @param {*} [stackB=[]] Pour détecter la récursivité
   * @returns {boolean}
   * @author Lodash
   */
  static isEqual (value, other, customizer, thisArg, stackA = [], stackB = []) {

    // Si customizer est définie
    if (customizer !== undefined) {

      if (thisArg)
        return customizer.call(thisArg, value, other) ;
      else
        return customizer(value, other) ;
    }

    // Pour booleans, nombres, string
    if (value === other)
      return true ;

    // null
    if (value == null || other == null ||
        (!Utils.isObject(value) && !Utils.isObjectLike(other)))
      return value !== value && other !== other ;


    let valTag = Object.prototype.toString.call(value),
        othTag = Object.prototype.toString.call(other) ;
    if (valTag === othTag) {
      switch (Object.prototype.toString.call(value)) {

        case TAG.bool:
        case '[object Date]':
          return +value == +other ;
        case '[object Error]':
          return (value.name == other.name && value.message == other.message) ;
        case TAG.number: // Treat `NaN`
          return (value != +value) ? other != +other : value == +other ;
        case TAG.regExp:
        case TAG.string:
          return value == (other + '') ;
      }
    }


    // Tests pour la récursivité
    let length = stackA.length ;
    while (length--) {
      if (stackA[length] == value)
        return stackB[length] == other ;
    }
    stackA.push(value) ;
    stackB.push(other) ;


    let result = false ;


    // Tableaux
    if (Array.isArray(value) && Array.isArray(other)) {

      // Si les tailles sont différentes, alors il y a un problème
      if (value.length !== other.length)
        return false ;

      // On igore les propriétés qui ne sont pas des index
      for (let i = 0 ; i < value.length ; i++) {

        if (!(value[i] === other[i] ||
              Utils.isEqual(value[i], other[i], undefined,
                            Utils, stackA, stackB)
        ))
          return false ;
      }

      result = true ;
    }


    // Objets divers
    else if (typeof value === 'object' && typeof other === 'object' &&
             other !== null) {

      if ((Object.getOwnPropertyNames(value).length ==
           Object.getOwnPropertyNames(other).length) &&
          Object.getPrototypeOf(value) === Object.getPrototypeOf(other)) {

        result = Object.getOwnPropertyNames(value).every(property => {

          if (!other.hasOwnProperty(property))
            return false ;
          return Utils.isEqual(value[property], other[property],
                              undefined, Utils, stackA, stackB) ;
        }) ;
      } else
        return false ;
    }

    stackA.pop() ;
    stackB.pop() ;

    return result ;
  }


  /**
   * Appelle fn sur chacun des éléments de la collection.
   * Si fn renvoit false, stop le forEach
   *
   * @category Collection
   * @param {Array|Object} collection Collection
   * @param {Function} fn Fonction appelée fn(value, index, collection)
   * @param {*} [thisArg] Contexte
   * @returns {Array|Object} Retourne `collection`
   * @author Lodash
   */
  static forEach (collection, fn, thisArg) {

    if (typeof fn !== 'function')
      throw new TypeError(fn + ' is not a function') ;

    let k = -1 ;

    if (Array.isArray(collection)) {

      // Défini en amont pour ne pas recalculer à chaque fois la longueur
      let length = collection.length ;

      while (++k < length) {
        if (fn.call(thisArg, collection[k], k, collection) === false)
          break ;
      }
    }
    else if (collection !== undefined) {

      let iterable = Utils.isObject(collection) ?
            collection : Object(collection),
          props = Object.keys(collection),
          length = props.length ;

      while (++k < length) {

        let key = props[k] ;
        if (fn(iterable[key], key, iterable) === false)
          break ;
      }
    }

    return collection ;
  }


  /**
   * Appelle fn sur chacun des éléments de la collection, mais en la parcourant
   * à partir de la fin. Si fn renvoit false, stop le forEach
   *
   * @category Collection
   * @param {Array|Object} collection Collection
   * @param {Function} fn Fontion appellée
   * @param {*} [thisArg] Context
   * @returns {Array|Object} Retourne `collection`
   * @author Lodash
   */
  static forEachRight (collection, fn, thisArg) {

    if (typeof fn !== 'function')
      throw new TypeError(fn + ' is not a function') ;

    let k = collection.length ;

    if (Array.isArray(collection)) {

      while (k) {

        k-- ;
        if (fn.call(thisArg, collection[k], k, collection) === false)
          return false ;
      }
    }
    else if (collection !== undefined) {

      let iterable = Utils.isObject(collection) ?
            collection : Object(collection),
          props = Object.keys(collection),
          length = props.length,
          index = length ;

      while (index--) {

        let key = props[index] ;
        if (fn(iterable[key], key, iterable) === false)
          break ;
      }
    }

    return collection ;
  }


  /**
   * Assigne les propriétés énumérables des objets sources à `object`. Si la
   * propriété existe déjà, elle sera écrasée. De même, une source écrase les
   * propriétés d'une source précédente.
   *
   * @param {Object} object Objet qui sera étendus
   * @param {source} sources Les sources
   * @return {Object}
   */
  static extend (object, ...sources) {

    sources.forEach(source => {
      if (Utils.isObject(source)) {
        Object.getOwnPropertyNames(source).forEach(property => {
          object[property] = source[property] ;
        }) ;
      }
    }) ;

    return object ;
  }


  /**
   * Converti `string` en camelCase
   *
   * @category Util
   * @param {String} string Chaîne de caractère à convertir
   * @returns {String}
   * @author Lodash
   */
  static camelCase (string) {

    // On force string à être un String
    string = string === null ? '' : string + '' ;

    // On convertit les caractères latin-1 supplémentaires en latin classique
    // et on retire les caractères diacriques.
    let reComboMark = /[\u0300-\u036f\ufe20-\ufe23]/g ;
    let reLatin1 = /[\xc0-\xd6\xd8-\xde\xdf-\xf6\xf8-\xff]/g ;

    function deburrLetter (letter) {

      // Utilisé pour mapper les caractères latin-1 supplementary en
      // caractères basic latin.
      let deburredLetters = {
        '\xc0': 'A',  '\xc1': 'A', '\xc2': 'A', '\xc3': 'A', '\xc4': 'A',
        '\xc5': 'A',
        '\xe0': 'a',  '\xe1': 'a', '\xe2': 'a', '\xe3': 'a', '\xe4': 'a',
        '\xe5': 'a',
        '\xc7': 'C',  '\xe7': 'c',
        '\xd0': 'D',  '\xf0': 'd',
        '\xc8': 'E',  '\xc9': 'E', '\xca': 'E', '\xcb': 'E',
        '\xe8': 'e',  '\xe9': 'e', '\xea': 'e', '\xeb': 'e',
        '\xcC': 'I',  '\xcd': 'I', '\xce': 'I', '\xcf': 'I',
        '\xeC': 'i',  '\xed': 'i', '\xee': 'i', '\xef': 'i',
        '\xd1': 'N',  '\xf1': 'n',
        '\xd2': 'O',  '\xd3': 'O', '\xd4': 'O', '\xd5': 'O', '\xd6': 'O',
        '\xd8': 'O',
        '\xf2': 'o',  '\xf3': 'o', '\xf4': 'o', '\xf5': 'o', '\xf6': 'o',
        '\xf8': 'o',
        '\xd9': 'U',  '\xda': 'U', '\xdb': 'U', '\xdc': 'U',
        '\xf9': 'u',  '\xfa': 'u', '\xfb': 'u', '\xfc': 'u',
        '\xdd': 'Y',  '\xfd': 'y', '\xff': 'y',
        '\xc6': 'Ae', '\xe6': 'ae',
        '\xde': 'Th', '\xfe': 'th',
        '\xdf': 'ss'
      } ;

      return deburredLetters[letter] ;
    }
    string = string.replace(reLatin1, deburrLetter).replace(reComboMark, '') ;

    // On génère un tableau de mots
    let reWords = (function () {
      let upper = '[A-Z\\xc0-\\xd6\\xd8-\\xde]',
          lower = '[a-z\\xdf-\\xf6\\xf8-\\xff]+' ;
      return new RegExp(upper + '+(?=' + upper + lower + ')|' + upper + '?'
                    + lower + '|' + upper     + '+|[0-9]+', 'g') ;
    }()) ;
    let array = string.match(reWords) || [] ;

    // On parcours la liste des mots
    let result = '' ;
    array.forEach(function (word, index) {

      // On met tout le mot en minuscule
      word = word.toLowerCase() ;
      // On met la première lettre en majuscule si ce n'est pas le premier mot
      result += index ? (word.charAt(0).toUpperCase() + word.slice(1)) : word ;
    }) ;

    return result ;
  }


  /**
   * Converti `string` en kebab-case
   *    Foo Bar     foo-bar
   *    fooBar      foo-bar
   *    __FOO_BAR__ foo-bar
   * @param {String} string Chaîne à convertir
   * @returns {String}
   * @static
   * @author Lodash
   */
  static kebabCase (string) {

    const upper = '[A-Z\\xc0-\\xd6\\xd8-\\xde]',
          lower = '[a-z\\xdf-\\xf6\\xf8-\\xff]+' ;
    let index = -1,
        array = string.match(RegExp(upper + '+(?=' + upper + lower + ')|'
          + upper + '?' + lower + '|' + upper + '+|[0-9]+', 'g')) || [],
        length = array.length,
        result = '' ;

    while (++index < length)
      result += (index ? '-' : '') + array[index].toLowerCase() ;

    return result ;
  }


  /**
   * Calcul la distance de Levenshtein donnant une mesure de similarité entre
   * deux chaînes de caractères.
   *
   * @param {String} str1 Première chaîne à tester
   * @param {String} str2 Deuxième chaîne à tester
   * @param {Boolean} [collator=false] Utilise `IntlCollator` pour une
   *                                  comparaison locale-sensitive
   * @returns {Integer}
   * @static
   */
  static levenshtein (str1, str2, collator = false) {

    let str1Len = str1.length,
        str2Len = str2.length ;

    // Cas basiques
    if (str1Len === 0) return str2Len ;
    if (str2Len === 0) return str1Len ;

    // Initialisation du collator
    if (collator) {
      try {
        collator = (typeof Intl !== 'undefined' &&
                    typeof Intl.Collator !== 'undefined') ?
          Intl.Collator('generic', {sensitivity: 'base'}) : null ;
      } catch (err) {
        console.log('Collator could not be initialized and wouldn\'t be used') ;
      }
    }


    // Variables à déclarer
    let prevRow = [],
        str2Char = [],
        curCol, nextCol, i, j, tmp, strCmp ;

    // Initialisation de prevRow
    for (i = 0 ; i < str2Len ; i++) {

      prevRow[i] = i ;
      str2Char[i] = str2.charCodeAt(i) ;
    }
    prevRow[str2Len] = str2Len ;

    // Calcule de la distance entre la ligne courante avec la précédente
    for (i = 0 ; i < str1Len ; i++) {

      nextCol = i + 1 ;

      for (j = 0 ; j < str2Len ; j++) {

        curCol = nextCol ;

        // substitution
        if (!collator)
          strCmp = str1.charCodeAt(i) === str2Char[j] ;
        else {
          strCmp = 0 === collator.compare(
            str1.charAt(i), String.fromCharCode(str2Char[j])) ;
        }
        nextCol = prevRow[j] + (strCmp ? 0 : 1) ;

        // insertion
        tmp = curCol + 1 ;
        if (nextCol > tmp)
          nextCol = tmp ;

        // deletion
        tmp = prevRow[j + 1] + 1 ;
        if (nextCol > tmp)
          nextCol = tmp ;

        // Préparation pour la prochaine itération
        prevRow[j] = curCol ;
      }

      // Préparation pour la prochaine itération
      prevRow[j] = nextCol ;
    }

    return nextCol ;
  }


  /**
   * Créé un clone de `value` et le retourne. Si `isDeep` vaut `true, les
   * propriétés ayant pour valeur un objet sont également clonée et non passées
   * par référence. Les propriétés d'`arguments` ou les objets créés par un
   * constructeur autre que `Object` sont clonés comme des objets `Objects`. Un
   * objet vide est retourné pour les valeurs non clonables (comme les
   * fonctions, nœud DOM, ...)
   *
   * @category Lang
   * @param {*} value Valeur à cloner
   * @param {Boolean} [isDeep = false] Deep clone
   * @param {String} [key] The key of `value`
   * @param {Object} [object] The object `value ` belongs to.
   * @param {Array} [stackA=[]] Tracks traversed source objects.
   * @param {Array} [stackB=[]] Associates clones with source counterparts
   * @returns {*}
   * @author Lodash
   */
  static clone (value, isDeep = false, key, object, stackA, stackB) {

    let result ;

    // Value n'est pas un objet, on la retourne directement
    if (!Utils.isObject(value))
      return value ;

    let isArr = Array.isArray(value) ;

    // Value est un tableau
    if (isArr) {

      result = new value.constructor(value.length) ;

      // Add array properties assigned by `RegExp#exec`
      if (value.length && typeof value[0] == 'string' &&
          value.hasOwnProperty('index')) {
        result.index = value.index ;
        result.input = value.input ;
      }

      if (!isDeep) {
        value.forEach((val, ind) => {
          result[ind] = val ;
        }) ;
        return result ;
      }
    }
    // Value est un objet
    else {

      let tag = Object.prototype.toString.call(value),
          isFunc = tag == TAG.func ;

      if (tag == TAG.object || tag == TAG.args || (isFunc && !object)) {

        let Ctor = (isFunc ? {} : value).constructor ;
        if (!(typeof Ctor == 'function' && Ctor instanceof Ctor))
          Ctor = Object ;
        result = new Ctor ;

        if (!isDeep) {
          if (value == null)
            return result ;

          Object.keys(value).forEach(key => {
            result[key] = value[key] ;
          }) ;
          return result ;
        }
      }
      else {
        if (CLONEABLE_TAG[tag]) {

          let Ctor = value.constructor ;

          switch (tag) {

            case TAG.arrayBuffer: {

              result = new ArrayBuffer(value.byteLength) ;
              let view = new Uint8Array(result) ;
              view.set(new Uint8Array(value)) ;
              return result ;
            }

            case TAG.bool:
            case TAG.date:
              return new Ctor(+value) ;

            case TAG.float32: case TAG.float64: case TAG.int8: case TAG.int16:
            case TAG.int32:   case TAG.uint8:   case TAG.uint8Clamped:
            case TAG.uint16:  case TAG.uint32: {

              let buffer = value.buffer ;
              result = new ArrayBuffer(buffer.byteLength) ;
              let v = new Uint8Array(result) ;
              v.set(new Uint8Array(buffer)) ;
              return new Ctor(isDeep ? result : buffer,
                              value.byteOffset, value.length) ;
            }

            case TAG.number:
            case TAG.string:
              return new Ctor(value) ;

            case TAG.regExp:
              result = new Ctor(value.source, /\w*$/.exec(value)) ;
              result.lastIndex = value.lastIndex ;
          }
          return result ;
        }
        else
          return (object ? value : {}) ;
      }
    }

    // Check for circular references and return its corresponding clone.
    stackA || (stackA = []) ;
    stackB || (stackB = []) ;

    let length = stackA.length ;
    while (length--) {
      if (stackA[length] == value)
        return stackB[length] ;
    }
    stackA.push(value) ;
    stackB.push(result) ;

    (isArr ? Utils.forEach : Utils.forOwn)(value, (subValue, key) => {
      result[key] = Utils.clone(subValue, isDeep, key, value, stackA, stackB) ;
    }) ;

    return result ;
  }


  /**
   * Retourne un nouvel objet, copie de objet. Les propriétés ayant pour valeur
   * un objet sont également clonée et non passées par référence. Les propriétés
   * d'`arguments` ou les objets créés par un constructeur autre que `Object`
   * sont clonés comme des objets `Objects`. Un objet vide est retourné pour
   * les valeurs non clonables (comme les fonctions, nœud DOM, ...)
   *
   * @category Lang
   * @param {*} value L'objet à copier
   * @returns {*}
   * @author Lodash
   */
  static cloneDeep (value) {

    return Utils.clone(value, true) ;
  }


  /**
   * Parcours les propriétés de l'objet.
   *
   * @category Object
   * @param {Object} obj Objet à parcourir
   * @param {Function} fn Function callback
   * @param {*} [thisArg] Context
   * @returns {Object} Retourne `obj`
   * @author Lodash
   */
  static forOwn (obj, fn, thisArg) {

    if (typeof fn !== 'function')
      throw new TypeError(fn + ' is not a function') ;

    let iterable = Utils.isObject(obj) ? obj : Object(obj),
        props = Object.keys(obj),
        length = props.length,
        k = -1 ;

    while (++k < length) {

      let key = props[k] ;
      if (Array.isArray(obj)) key = Number(key) ;
      if (fn.call(thisArg, iterable[key], key, iterable) === false)
        break ;
    }

    return obj ;
  }


  /**
   * Exécute `callback` sur chacun des éléments. Retourne true, si chaque appel
   * de callback a retourné true, false sinon.
   * predicate (currentValue, index, collection)
   *
   * @category Collection
   * @param {Array|Object} collection Collection
   * @param {Function} predicate Function appelée sur chaque élément. Si non
   *                             définie : value => { return value ; }
   * @param {*} [thisArg] this binding
   * @returns {boolean}
   * @author Lodash
   * @static
   */
  static some (collection, predicate, thisArg) {

    if (!Utils.isFunction(predicate))
      predicate = value => { return value ; } ;

    // Fonction interne pour les Array
    if (Array.isArray(collection)) {

      let index  = -1,
          length = collection.length ;

      while (++index < length) {
        if (predicate.call(thisArg, collection[index], index, collection))
          return true ;
      }

      return false ;
    }
    // Pour les objets
    else if (Utils.isObject(collection)) {

      let result ;

      // On teste chaque propriété
      Utils.forEach(collection, (value, key, collection) => {

        result = predicate.call(thisArg, value, key, collection) ;
        // Si result == false, on a pas besoin de tester les autres valeurs
        return !result ;
      }) ;

      // On s'assure d'avoir un boolean
      return !!result ;
    }

    return false ;
  }


  /**
   * Transforme `object` en un nouveau `accumulator`, chaque propriété a été
   * transformée par `callback`. S'arrête si une `callback` retourne
   * explicitement `false`
   *
   * @category Object
   * @param {Object} object Objet
   * @param {Function} callback Fonction callback
   * @param {Object} [accumulator = {}] Accumulator
   * @param {*} [thisArg] this binding
   * @returns {Object}
   * @author Lodash
   */
  static transform (object, callback, accumulator, thisArg) {

    if (!Utils.isFunction(callback))
      callback = value => { return value ; } ;

    if (accumulator === null || accumulator === undefined) {
      if(Array.isArray(object) || Utils.isObject(object)) {
        let Ctor = object.constructor ;
        if (Array.isArray(object))
          accumulator = new Ctor ;
        else {
          let obj = function () {} ;
          if (Utils.isFunction(Ctor) && Utils.isObject(Ctor.prototype)) {
            obj.prototype = Ctor.prototype ;
            accumulator = new obj ;
          }
          else
            accumulator = {} ;
        }
      }
      else
        accumulator = {} ;
    }

    Utils.forEach(object, (value, property, object) => {
      return callback.call(thisArg, accumulator, value, property, object) ;
    }) ;

    return accumulator ;
  }

  /**
   * Suprimmes tous les éléments d'array pour lesquels predicate retourne true
   * et retourne un tableau des éléments supprimés. predicate est bindé avec
   * thisArg et appelé avec (value, index, array).
   *
   * @category Array
   * @param {Array} array Tableau
   * @param {Function|Object} predicate Fonction déterminant les entrées à
   *        retirer. S'il s'agit d'un objet :
   *
   * @param {*} [thisArg] Context
   * @returns {Array}
   * @author Lodash
   */
  static remove (array, predicate, thisArg) {

    let result  = [],
        indexes = [],
        index   = -1 ;

    // Si le tableau est vide
    if (!(array && array.length))
      return result ;

    // Si predicate est un Objet
    if (!Utils.isFunction(predicate) && Utils.isObject(predicate)) {

      let index = -1,
          props = Object.keys(predicate),
          length = props.length,
          matchData = Array(length) ;
      while (++index < length) {
        let key = props[index] ;
        matchData[index] = [key, predicate[key]] ;
      }

      length = matchData.length ;

      while (length--) {
        matchData[length][2] =
          matchData[length][1] === matchData[length][1] &&
          !Utils.isObject(matchData[length][1]) ;
      }

      if (matchData.length == 1 && matchData[0][2]) {

        let key = matchData[0][0],
            value = matchData[0][1] ;

        predicate = function (object) {
          if (object == null)
            return false ;
          return object[key] == value && (value !== undefined ||
            (key in Object(object))) ;
        } ;
      }
      else {
        predicate = function (object) {

          let index = matchData.length,
              length = index ;

          if (object == null)
            return !length ;

          object = Object(object) ;

          while (index--) {
            let data = matchData[index] ;
            if ((data[2] && data[1] !== object[data[0]]) ||
                !(data[0] in object))
              return false ;
          }

          while (++index < length) {

            let data = matchData[index] ;
            let key = data[0],
                objValue = object[key],
                srcValue = data[1] ;

            if (data[2]) {
              if (objValue === undefined && !(key in object))
                return false ;
            }
            else
              return Utils.isEqual(srcValue, objValue) ;
          }

          return true ;
        } ;
      }
    }

    // On parcours le tableau
    let length = array.length ;
    while (++index < length) {

      let value = array[index] ;
      if (predicate.call(thisArg, value, index, array)) {
        result.push(value) ;
        indexes.push(index) ;
      }
    }

    // On supprime les éléments
    Utils.forEachRight(indexes, index => {

      let previous ;
      if (index != previous && index > -1) {
        previous = index ;
        array.splice(index, 1) ;
      }
    }) ;

    // On retourne les éléments supprimés
    return result ;
  }


  /**
   * Met la première lettre en majuscule
   * @param {String} str String à transformer
   * @returns {String}
   */
  static ucfirst (str) {

    if (Utils.isString(str))
      return str.charAt(0).toUpperCase() + str.substr(1) ;
    else
      return str ;
  }


  /**
   * Génère un UUID v4
   * De la forme `xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx`, où chaque `x` est un
   * caractère hexadécimal aléatoire et `y` vaut `8`, `9`, `a` ou `b`.
   *
   * 4B - 2B - 2B - 2B - 6B = 16 Bytes
   *
   * @returns {String}
   */
  static uuid () {

    let buf = crypto.randomBytes(16) ;

    buf[6] = (buf[6] & 0x0f) | 0x40 ;
    buf[8] = (buf[8] & 0x3f) | 0x80 ;

    return BYTE2HEX[buf[0]] + BYTE2HEX[buf[1]]
         + BYTE2HEX[buf[2]] + BYTE2HEX[buf[3]] + '-'
         + BYTE2HEX[buf[4]] + BYTE2HEX[buf[5]] + '-'
         + BYTE2HEX[buf[6]] + BYTE2HEX[buf[7]] + '-'
         + BYTE2HEX[buf[8]] + BYTE2HEX[buf[9]] + '-'
         + BYTE2HEX[buf[10]] + BYTE2HEX[buf[11]]
         + BYTE2HEX[buf[12]] + BYTE2HEX[buf[13]]
         + BYTE2HEX[buf[14]] + BYTE2HEX[buf[15]] ;
  }


  /**
   * Retourne la RegExp permettant de valider un UUID v4
   * @returns {RegExp}
   */
  static uuidRegExp () {

    return RegExp(
      '^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$',
      'i'
    ) ;
  }
}

module.exports = Utils ;

// On enregistre des tags
const TAG = {
  'args':         '[object Arguments]',
  'array':        '[object Array]',
  'bool':         '[object Boolean]',
  'date':         '[object Date]',
  'error':        '[object Error]',
  'func':         '[object Function]',
  'map':          '[object Map]',
  'number':       '[object Number]',
  'object':       '[object Object]',
  'regExp':       '[object RegExp]',
  'set':          '[object Set]',
  'string':       '[object String]',
  'weakMap':      '[object WeakMap]',
  'arrayBuffer':  '[object ArrayBuffer]',
  'float32':      '[object Float32Array]',
  'float64':      '[object Float64Array]',
  'int8':         '[object Int8Array]',
  'int16':        '[object Int16Array]',
  'int32':        '[object Int32Array]',
  'uint8':        '[object Uint8Array]',
  'uint8Clamped': '[object Uint8ClampedArray]',
  'uint16':       '[object Uint16Array]',
  'uint32':       '[object Uint32Array]'
} ;

const CLONEABLE_TAG = {} ;
CLONEABLE_TAG[TAG.args]        = CLONEABLE_TAG[TAG.array]  =
CLONEABLE_TAG[TAG.arrayBuffer] = CLONEABLE_TAG[TAG.bool]   =
CLONEABLE_TAG[TAG.date]        = CLONEABLE_TAG[TAG.float]  =
CLONEABLE_TAG[TAG.float64]     = CLONEABLE_TAG[TAG.int8]   =
CLONEABLE_TAG[TAG.int16]       = CLONEABLE_TAG[TAG.int32]  =
CLONEABLE_TAG[TAG.number]      = CLONEABLE_TAG[TAG.object] =
CLONEABLE_TAG[TAG.regExp]      = CLONEABLE_TAG[TAG.string] =
CLONEABLE_TAG[TAG.uint8]       = CLONEABLE_TAG[TAG.uint8]  =
CLONEABLE_TAG[TAG.uint16]      = CLONEABLE_TAG[TAG.uint32] = true ;
CLONEABLE_TAG[TAG.error]   = CLONEABLE_TAG[TAG.func] =
CLONEABLE_TAG[TAG.map]     = CLONEABLE_TAG[TAG.set]  =
CLONEABLE_TAG[TAG.weakMap] = false ;

// Bytes to hex
const BYTE2HEX = [] ;
for (let i = 0 ; i < 256 ; ++i)
  BYTE2HEX[i] = (i + 0x100).toString(16).substr(1) ;

