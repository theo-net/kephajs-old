'use strict' ;

/**
 * src/core/Framework.test.js
 */

const expect = require('chai').expect ;

const Framework = require('./Framework'),
      Utils     = require('./Utils'),
      Kernel    = require('./Kernel'),
      Entity    = require('./Entity') ;

describe('Framework', function () {

  it('fournit toutes les méthodes d\'Utils', function () {

    const utilsMethods = Reflect.ownKeys(Utils) ;
    let expected = true ;

    utilsMethods.forEach(method => {

      if (!Reflect.has(Framework, method))
        expected = false ;
    }) ;

    expect(expected).to.be.true ;
  }) ;

  it('donne accès au Kernel', function () {

    expect(Framework.kernel()).to.be.instanceOf(Kernel) ;
  }) ;

  it('donne accès à la class Entity', function () {

    expect(Framework.Entity).to.be.equal(Entity) ;
  }) ;
}) ;

