'use strict' ;

/**
 * src/server/Slot.js
 */

const path = require('path'),
      fs   = require('fs') ;

const SlotError    = require('./SlotError'),
      View         = require('./View'),
      KernelAccess = require('../core/KernelAccess'),
      User         = require('./User/User'),
      Utils        = require('../cli/Utils') ;

/**
 * Un slot est une action routée par le routeur que l'on peut exécuter
 *
 * Les controllers sont des objets. Ils peuvent avoir une propriété `setViews`
 * qui asocie à chaque action une vue. Si aucune correspondance n'est trouvée,
 * le framework essayera de la trouver toute seule. Si au contraire la valeur
 * est `null`, aucune vue ne sera associée.
 *
 * Les controllers peuvent posséder une méthode `init()` qui sera appellée
 * avant l'exécution de chaque action. Si celle-ci possède un traitement
 * asynchrone, elle doit retourner une `Promise`, ainsi le serveur attendra que
 * ce traitement soit terminé avant d'exécuter l'action. Elle peut recevoir en
 * paramètres les variables issues du routeur (dont données GET) et POST.
 *
 * Dans config, $userObj si définie sera l'objet à partir duquel on instance
 * l'objet User.
 */
class Slot extends KernelAccess {

  /**
   * Initialise le Slot. Lui associera sa vue et définira le layout
   * @param {String|Function} controller Controller à exécuter. Peut être un
   *        objet à initialiser
   * @param {String} action Action à exécuter
   * @param {Object} vars Variables transmises
   * @param {http.IncomingMessage} request Requête reçue
   * @param {http.ServerResponse} response Réponse du serveur
   * @param {Object} [user] Objet user à transmettre (si non définie, en
   *    créra un nouveau)
   */
  constructor (controller, action, vars, request, response, user) {

    super() ;

    this._controllerName = Utils.ucfirst(controller) ;
    this._action = action ;
    this._vars = vars ;
    this._request = request ;
    this._response = response ;

    // On initialise le controller
    //   Un objet
    if (Utils.isFunction(controller))
      this._controllerName = '[object]' ;
    //   Un String
    else {
      if (this._controllerName.substr(0, 1) == '@') {
        controller = __dirname + path.sep
          + Utils.ucfirst(this._controllerName.substr(1))
          + 'Controller' ;
        if (!action)
          this._action = action = 'slot' ;
      }
      else {
        controller = this.kernel().get('$config').get('basepath')
          + this.kernel().get('$config').get('controllerDir')
          + this._controllerName + path.sep
          + this._controllerName + 'Controller' ;
      }

      if(fs.existsSync(controller + '.js'))
        controller = require(controller) ;
      else {
        throw new SlotError(
          'Controller `' + this._controllerName + '` not found'
        ) ;
      }
    }

    // On transmet le kernel au controller
    controller.prototype.kernel = this.kernel ;
    controller.prototype.get = name => {
      return this.kernel().get(name) ;
    } ;

    // On créé une instance
    this._controller = new controller() ;

    // On vérifie que l'action existe
    if (!Utils.isFunction(this._controller[action + 'Action'])) {
      throw new SlotError(
        'Action `' + action + '` of controller `'
        + this._controllerName + '` not found'
      ) ;
    }

    // On initialise la vue
    let view = '' ;

    if (controller.prototype.setViews &&
      controller.prototype.setViews[this._action] === null)
      view = null ;
    else if (controller.prototype.setViews &&
      controller.prototype.setViews[this._action])
      view = controller.prototype.setViews[this._action] ;
    else
      view = this._action + '.html' ;

    this._view = new View(view == null ? null
      : this.kernel().get('$config').get('basepath')
       + this.kernel().get('$config').get('controllerDir')
       + this._controllerName + path.sep + 'views' + path.sep + view
    ) ;


    let layout = this.kernel().get('$config').get('layout.html', false) ;
    if (layout)
      layout = this.kernel().get('$config').get('basepath') + layout ;
    this._view.$setLayout(layout) ;

    // On transmet certaines choses au controller et à la vue
    this._controller.$request = request ;
    this._controller.$response = response ;
    this._controller.$view = this._view ;
    this._view.$request = request ;
    this._view.$response = response ;

    // On créé l'objet User ou on transmet celui déjà reçu
    if (!user) {

      let UserObj = this.kernel().get('$config').get('$userObj', () => User) ;
      user = new UserObj(
        request, response,
        this.kernel().get('$sessions'),
        this.kernel().get('$config').get('user')
      ) ;
      this._userInit = user.init() ;
    }
    if (!(this._userInit instanceof Promise))
      this._userInit = new Promise(resolve => { resolve() ; }) ;
    this._controller.$user = user ;
    this._view.$user = this._controller.$user ;
    if (this._response)
      this._response.$user = this._controller.$user ;

    // On notifie le canal Slot
    this.kernel().events().notify('Slot',
      'Slot ' + this._controllerName + ':' + this._action + ' initialisé'
    ) ;
  }


  /**
   * Exécute le slot. Gère les actions asynchrones, et même une initialisation
   * du controleur asynchrone. Pour cela, l'`init` ou l'action doivent
   * retourner une `Promise`. Le reject de cette Promise peut contenir un code
   * erreur (403, 404, 500, ...) qui renverra l'erreur page correspondante. Si
   * un message est retourné, ce sera une erreur 500, si une exception est
   * attrapée, son message sera affiché.
   * Une action dont la promise est résolue avec la valeur `null`, ne vera pas
   * son exécution résolue : utile si la requête est terminée par elle-même
   * (ex : envoit d'un fichier lourd via un buffer).
   * @param {Object} [data] Données pouvant être issues d'une requête
   * @returns {Promise}
   */
  execute (data = {}) {

    return new Promise((resolve, reject) => {

      Utils.extend(this._vars, data) ;

      this._userInit.then(() => {

        if (Utils.isFunction(this._controller.init))
          return this._controller.init(this._vars) ;

      }).then(() => {

        return this._controller[this._action + 'Action']
          .call(this._controller, this._vars) ;
      }).then((code) => {

        if (code !== null)
          resolve() ;
      }).catch((code) => {

        if (code && code.stack) {

          console.error(this.kernel().get('$color').red(code.stack)) ;
          code = 500 ;
        }
        if (!Utils.isFunction(this._response['sendError' + code]))
          code = 500 ;

        this._response['sendError' + code]()
          .then((code) => { reject(code) ; }) ;
      }) ;
    }) ;
  }


  /**
   * Retourne la vue associée au slot
   * @returns {View|null}
   */
  getView () {

    return this._view ;
  }
}

module.exports = Slot ;

