'use strict' ;

/**
 * src/server/User/User.js
 */

const Sessions      = require('./Sessions'),
      MemoryStorage = require('./MemoryStorage'),
      Utils         = require('../../cli/Utils'),
      KernelAccess  = require('../../core/KernelAccess') ;

const CONFIG_DEFAULT = {
  sessionCookieName:     'kjs_session_id',
  sessionCookieDomain:   null,
  sessionCookiePath:     null,
  sessionCookieSecure:   false,
  sessionCookieHttpOnly: true
} ;

/**
 * Cette classe gère les données d'un client, un user. C'est à dire notamment
 * ses cookies et sa session.
 */
class User extends KernelAccess {

  /**
   * Initialise l'objet avec la requête et la réponse.
   *    sessionCookieName:     'kjs_session_id',
   *    sessionCookieDomain:   null,
   *    sessionCookiePath:     null,
   *    sessionCookieSecure:   false,
   *    sessionCookieHttpOnly: false
   * Si `sessionCookieDomain` est un `Array`, on parcourera la liste des
   * domaines pour déterminer celui qui correspond à la requête. Il faut donc
   * les fournir du plus précis au moins précis (s'arrête dès qu'une
   * occurence est trouvée :
   *    - `a.domain.com`
   *    - `b.domain.com`
   *    - `domain.com`
   *    - `other-domain.org`
   *    - ...
   * Si aucun domaine n'est trouvé, on prend le premier de la liste.
   * @param {http.ServerRequest} request La requête
   * @param {Resp} response La réponse
   * @param {Sessions} sessions Service gérant les sessions
   * @param {Object} [config] Configuration
   */
  constructor (request, response, sessions, config) {

    super() ;

    this._request = request ;
    this._response = response ;

    // Configuration
    this._config = Utils.extend({}, CONFIG_DEFAULT, config) ;
    if (Array.isArray(this._config.sessionCookieDomain)) {

      this._sessionCookieDomain = this._config.sessionCookieDomain[0] ;
      let host = request.headers.host.split(':')[0] ;

      Utils.forEach(this._config.sessionCookieDomain, domain => {

        if (host.search(new RegExp(domain + '$')) > -1) {

          this._sessionCookieDomain = domain ;
          return false ;
        }
      }) ;
    }
    else
      this._sessionCookieDomain = this._config.sessionCookieDomain ;

    // Si aucun service n'est associé, on en créé un local
    if (!sessions)
      this._sessions = new Sessions(new MemoryStorage) ;
    else
      this._sessions = sessions ;

    // on récupère les cookies
    this._cookies = {} ;
    this._loadCookies() ;

    // On initialise ou on récupère la session
    if (!this._getSession())
      this._newSession() ;
  }


  /**
   * À étendre si besoin. On peut lui faire retourner une promise, ainsi, le
   * cotroller ne sera exécuté que lorsque cet objet sera complètement
   * initialisé.
   */
  init () {}


  /**
   * Enregistre un cookie. Si expires et maxAge sont `undefined` ou `null`
   * alors la durée de vie du cookie est celle de la session du navigateur.
   * Attention, les navigateurs ont tendance à restaurer ces cookies.
   * Si `domain` n'est pas défini, alors le cookie s'appliquera au domain
   * courant, mais pas aux sous-domaines.
   * @param {String} name Nom du cookie
   * @param {String} value Valeur du cookie
   * @param {Integer} expires Date d'expiration (timestamp)
   * @param {Integer} maxAge Durée de vie du cookie
   * @param {String} domain Inclue les sous-domaine
   * @param {String} path Chemin pour lesquels s'applique le cookie
   * @param {Boolean} secure Le cookie ne sera transmis qu'en HTTPS
   * @param {Boolean} httpOnly Le cookie ne sera disponible que dans les
   *        requête, pas dans la page à travers les méthodes `Document.cookie`,
   *        `XMLHttpRequest` ou `Request`
   * @throws {Error} Si les headers ont déjà été envoyés
   */
  setCookie (name, value, expires, maxAge, domain, path, secure, httpOnly) {

    if (this._response && this._response.headersSent &&
      this._response.headersSent())
      throw new Error('Le cookie ne peut être créé : headers déjà evoyés') ;

    let cookie = name + '=' + value ;

    if (expires != undefined && expires != null)
      cookie += ';Expires=' + expires ;
    if (maxAge != undefined && maxAge != null)
      cookie += ';Max-Age=' + maxAge ;
    if (domain != undefined && domain != null)
      cookie += ';Domain=' + domain ;
    if (path != undefined && path != null)
      cookie += ';Path=' + path ;
    if (secure)
      cookie += ';Secure' ;
    if (httpOnly)
      cookie += ';HttpOnly' ;

    if (this._response && this._response._cookies)
      this._response._cookies.push(cookie) ;

    this._cookies[name] = value ;
  }


  /**
   * Retourne la valeur d'un cookie
   * @param {String} name Nom du cookie
   * @returns {*}
   */
  getCookie (name) {

    return this._cookies[name] ;
  }


  /**
   * Indique si un cookie existe ou pas
   * @param {String} name Nom du cookie
   * @returns {Boolean}
   */
  hasCookie (name) {

    return Object.keys(this._cookies).indexOf(name) > -1 ;
  }


  /**
   * Supprime un cookie. Attention, on doit passer les mêmes options pour
   * supprimer le cookie que celles envoyées lors de sa création
   * @param {String} name Cookie à supprimer
   * @param {String} domain Inclue les sous-domaine
   * @param {String} path Chemin pour lesquels s'applique le cookie
   * @param {Boolean} secure Le cookie ne sera transmis qu'en HTTPS
   * @param {Boolean} httpOnly Le cookie ne sera disponible que dans les
   *        requête, pas dans la page à travers les méthodes `Document.cookie`,
   *        `XMLHttpRequest` ou `Request`
   */
  deleteCookie (name, domain, path, secure, httpOnly) {

    this.setCookie(name, '', 0, -1, domain, path, secure, httpOnly) ;
    delete this._cookies[name] ;
  }


  /**
   * Supprime tous les cookies, sauf celui de la session
   */
  resetCookies () {

    Utils.forEach(this._cookies, (value, cookie) => {
      if (cookie != this._config.sessionCookieName)
        this.deleteCookie(cookie) ;
    }) ;
  }


  /**
   * Définie un élément dans la session
   * @param {String} name Nom de la variable
   * @param {*} value Valeur
   */
  set (name, value) {

    this._sessions.set(this._sessionId, name, value) ;
  }


  /**
   * Indique si un élément est défini
   * @param {String} name Nom de la variable
   * @returns {Boolean}
   */
  has (name) {

    return this._sessions.hasElement(this._sessionId, name) ;
  }


  /**
   * Retourne la valeur d'un élément ou la valeur par défaut si celle-ci est
   * spécifiée et que l'élément n'existe pas.
   * @param {String} name Nom de la variable
   * @param {*} [dflt=null] Valeur par défaut
   * @returns {*}
   */
  get (name, dflt = null) {

    return this._sessions.hasElement(this._sessionId, name) ?
           this._sessions.get(this._sessionId, name)
           : (Utils.isFunction(dflt) ? dflt() : dflt) ;
  }


  /**
   * Supprime un élément
   * @param {String} name Nom de la variable
   */
  delete (name) {

    this._sessions.delete(this._sessionId, name) ;
  }


  /**
   * Réinitialise la session et détruit l'ancienne
   */
  reset () {

    let old = this._sessionId ;
    this._newSession() ;
    this._sessions.remove(old) ;
  }


  /**
   * Enregistre un message flash
   * @param {String} content Le message
   * @param {String} type Type (success, info, wrning, danger)
   */
  setFlash (content, type) {

    if (!this.has('flash'))
      this.set('flash', []) ;

    let flash = this.get('flash') ;
    flash.push({type, content}) ;

    this.set('flash', flash) ;
  }


  /**
   * Indique si des messages falsh sont enregistrés en session
   * @returns {Boolean}
   */
  hasFlash () {

    if (!this.has('flash'))
      return false ;
    else
      return this.get('flash').length > 0 ;
  }


  /**
   * Retourne la liste des messages flash
   * @returns {Array} Tableau contenant des objets de la forme {type, content}
   */
  getFlash () {

    if (this.has('flash')) {

      let flash = this.get('flash') ;
      this.delete('flash') ;
      return flash ;
    }
    else
      return [] ;
  }


  /**
   * Retourne quelques infos sur le navigateur dans un objet ayant les propriétés
   * suivantes :
   *   - `user-agent {String}` : User agent du navigateur
   *   - `accept {Array}` : liste des content-types acceptés
   *   - `accept-language {Array}` : liste des langages acceptés
   * @returns {Object}
   */
  browserInfo () {

    let headers = {} ;
    if (this._request && this._request.headers)
      headers = this._request.headers ;

    let info    = {},
        userAgent = headers['user-agent'] ? headers['user-agent'] : '',
        acceptLanguage = headers['accept-language'] ?
                           headers['accept-language'] : '' ;

    info['user-agent'] = userAgent ;
    info['accept-language'] = acceptLanguage != '' ?
      acceptLanguage.split(',').map(str => str.trim()) : [] ;
    info.signature = userAgent + acceptLanguage ;

    return info ;
  }


  /**
   * Change la session de l'utilisateur vers une session donnée. Cela permet
   * de forcer l'association d'un utilisateur à une session donnée sans
   * utiliser de cookies.
   * Attention, aucune vérification n'est effectuée ! Une erreur est lévée
   * juste si l'ide transmis ne correspond à aucune session.
   * @param {String} id Identifiant de la session
   * @throws {Error}
   */
  switchSession (id) {

    let old = this._sessionId ;

    if (!this._sessions.has(id))
      throw new Error('switchSession: unknown id [' + id + ']') ;

    this._sessionId = id ;
    this._sessions.hello(id) ;
    this._setCookieSession(this._sessionId) ;

    this._sessions.remove(old) ;
  }


  /**
   * Charge les cookies à partir de la requête
   */
  _loadCookies () {

    if (typeof this._request !== 'undefined' &&
      typeof this._request.headers !== 'undefined' &&
      typeof this._request.headers.cookie !== 'undefined') {

      let cookies = this._request.headers.cookie.split(';') ;

      cookies.forEach(cookie => {
        cookie = cookie.split('=') ;
        if (cookie.length > 1)
          this._cookies[cookie[0].trim()] = cookie[1].trim() ;
      }) ;
    }
  }


  /**
   * Initialise une nouvelle session
   */
  _newSession () {

    this._sessionId = this._sessions.init() ;
    this.set('_browser', this.browserInfo().signature) ;

    this._setCookieSession(this._sessionId) ;
  }


  /**
   * Récupère une session, met à jour l'id courant de l'user. On fera
   * également un coucou au gestionnaire pour mettre à jour le dernier accès
   * @returns {String|false} identifiant de la session ou `false` si aucune
   *  session trouvée (pas de cookie ou pas en mémoire)
   */
  _getSession () {

    let id = false,
        signature = this.browserInfo().signature ;

    if (this.hasCookie(this._config.sessionCookieName)) {

      id = this.getCookie(this._config.sessionCookieName) ;

      // session en mémoire ?
      if (!this._sessions.has(id))
        id = false ;
      // même navigateur ?
      else if (signature != this._sessions.get(id, '_browser'))
        id = false ;
      // Ok, on retourne la session
      else {
        this._sessionId = id ;
        this._sessions.hello(id) ;
      }
    }

    return id ;
  }


  /**
   * Créé le cookie permettant de transmettre l'identifiant de session.
   * @param {String} id Idenfifiant de la session
   */
  _setCookieSession (id) {

    this.setCookie(
      this._config.sessionCookieName,
      id,
      null, null,
      this._sessionCookieDomain,
      this._config.sessionCookiePath,
      this._config.sessionCookieSecure,
      this._config.sessionCookieHttpOnly
    ) ;
  }
}

module.exports = User ;

