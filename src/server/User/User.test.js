'use strict' ;

/**
 * src/server/User/User.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon'),
      http   = require('http') ;

const User          = require('./User'),
      Sessions      = require('./Sessions'),
      MemoryStorage = require('./MemoryStorage'),
      Resp          = require('../Resp') ;


// buddy ignore:start
describe('src/server/User/User', function () {

  let user, req, resp, sessions ;

  beforeEach(function () {

    req = new http.IncomingMessage() ;
    resp = new Resp(new http.ServerResponse('get')) ;
    sessions = new Sessions(new MemoryStorage()) ;
    user = new User(req, resp, sessions) ;
  }) ;


  it('on peut spécifier une configuration', function () {

    user = new User(req, resp, sessions, {sessionCookieName: 'foobar'}) ;

    expect(user._config.sessionCookieName).to.be.equal('foobar') ;
  }) ;


  it('si `sessionCookieDomain` est un `Array`, prend le meilleur domaine',
  function () {

    req.headers.host = 'www.domain.com' ;
    user = new User(req, resp, sessions, {sessionCookieDomain: 'foobar.com'}) ;
    expect(user._sessionCookieDomain).to.be.equal('foobar.com') ;

    user = new User(req, resp, sessions, {sessionCookieDomain: [
      'aaa.domain.com',
      'www.domain.com',
      'domain.com',
      'foobar.com'
    ]}) ;
    expect(user._sessionCookieDomain).to.be.equal('www.domain.com') ;

    user = new User(req, resp, sessions, {sessionCookieDomain: [
      'aaa.domain.com',
      'domain.com',
      'foobar.com'
    ]}) ;
    expect(user._sessionCookieDomain).to.be.equal('domain.com') ;
  }) ;


  it('fonctionne également si un port est transmis', function () {

    req.headers.host = 'www.domain.com:8089' ;
    user = new User(req, resp, sessions, {sessionCookieDomain: 'foobar.com'}) ;
    expect(user._sessionCookieDomain).to.be.equal('foobar.com') ;

    user = new User(req, resp, sessions, {sessionCookieDomain: [
      'aaa.domain.com',
      'domain.com',
      'foobar.com'
    ]}) ;
    expect(user._sessionCookieDomain).to.be.equal('domain.com') ;
  }) ;


  it('si aucun domaine trouvé, prend le premier de la liste', function () {

    req.headers.host = 'www.other.com' ;
    user = new User(req, resp, sessions, {sessionCookieDomain: [
      'aaa.domain.com',
      'domain.com',
      'foobar.com'
    ]}) ;
    expect(user._sessionCookieDomain).to.be.equal('aaa.domain.com') ;
  }) ;


  describe('cookies', function () {

    it('enregistre un cookie', function () {

      user.setCookie('foo', 'bar') ;
      user._response.end() ;

      expect(resp.getHeader('Set-Cookie')).to.be.deep.equal(
        ['kjs_session_id=' + user._sessionId + ';HttpOnly', 'foo=bar']
      ) ;
    }) ;

    it('on peut spécifier un expires et/ou un max-age', function () {

      user.setCookie('foo', 'bar', 100, 200) ;
      user.setCookie('fo1', 'bar', null, 200) ;
      user.setCookie('fo2', 'bar', 100) ;
      user._response.end() ;

      expect(resp.getHeader('Set-Cookie'))
        .to.be.deep.equal([
          'kjs_session_id=' + user._sessionId + ';HttpOnly',
          'foo=bar;Expires=100;Max-Age=200',
          'fo1=bar;Max-Age=200',
          'fo2=bar;Expires=100'
        ]) ;
    }) ;

    it('on peut sprécifier un domain et/ou un path', function () {


      user.setCookie('foo', 'bar', null, null, 'theo-net.org') ;
      user.setCookie('fo1', 'bar', null, null, null, '/hello') ;
      user.setCookie('fo2', 'bar', null, null, 'theo-net.org', '/hello') ;
      user._response.end() ;
      expect(resp.getHeader('Set-Cookie'))
        .to.be.deep.equal([
          'kjs_session_id=' + user._sessionId + ';HttpOnly',
          'foo=bar;Domain=theo-net.org',
          'fo1=bar;Path=/hello',
          'fo2=bar;Domain=theo-net.org;Path=/hello'
        ]) ;
    }) ;

    it('on peut spécfier si le cookie est Secure ou HttpOnly', function () {

      user.setCookie('foo', 'bar', null, null, null, null, true) ;
      user.setCookie('fo1', 'bar', null, null, null, null, false, true) ;
      user.setCookie('fo2', 'bar', null, null, null, null, true, true) ;
      user._response.end() ;

      expect(resp.getHeader('Set-Cookie'))
        .to.be.deep.equal([
          'kjs_session_id=' + user._sessionId + ';HttpOnly',
          'foo=bar;Secure',
          'fo1=bar;HttpOnly',
          'fo2=bar;Secure;HttpOnly'
        ]) ;
    }) ;

    it('envoit une erreur si les headers ont déjà été envoyés', function () {

      user._response.end() ;
      expect(user.setCookie.bind(user, 'foo', 'bar'))
        .to.throw(Error) ;
    }) ;

    it('récupère la valeur d\'un cookie', function () {

      req.headers.cookie = 'theo=net;kepha=js' ;
      user._loadCookies() ;
      user.setCookie('foo', 'bar') ;

      expect(user.getCookie('theo')).to.be.equal('net') ;
      expect(user.getCookie('kepha')).to.be.equal('js') ;
      expect(user.getCookie('foo')).to.be.equal('bar') ;
    }) ;

    it('récupère la valeur d\'un cookie, même si whitespace', function () {

      req.headers.cookie = 'theo=net; kepha=js' ;
      user._loadCookies() ;
      user.setCookie('foo', 'bar') ;

      expect(user.getCookie('theo')).to.be.equal('net') ;
      expect(user.getCookie('kepha')).to.be.equal('js') ;
      expect(user.getCookie('foo')).to.be.equal('bar') ;
    }) ;

    it('indique si un cookie est enregistré', function () {

      req.headers.cookie = 'theo=net' ;
      user._loadCookies() ;
      user.setCookie('foo', 'bar') ;

      expect(user.hasCookie('theo')).to.be.equal(true) ;
      expect(user.hasCookie('kepha')).to.be.equal(false) ;
      expect(user.hasCookie('foo')).to.be.equal(true) ;
    }) ;

    it('supprime un cookie', function () {

      user.setCookie('foo', 'bar') ;
      user.deleteCookie('foo') ;
      user._response.end() ;

      expect(user.hasCookie('foo')).to.be.false ;
      expect(resp.getHeader('Set-Cookie'))
        .to.be.deep.equal([
          'kjs_session_id=' + user._sessionId + ';HttpOnly',
          'foo=bar',
          'foo=;Expires=0;Max-Age=-1'
        ]) ;
    }) ;

    it('transmet les autres params', function () {

      user.setCookie('foo', 'bar', null, null, 'truc.org', '/', true, true) ;
      user.deleteCookie('foo', 'truc.org', '/', true, true) ;
      user._response.end() ;

      expect(user.hasCookie('foo')).to.be.false ;
      expect(resp.getHeader('Set-Cookie'))
        .to.be.deep.equal([
          'kjs_session_id=' + user._sessionId + ';HttpOnly',
          'foo=bar;Domain=truc.org;Path=/;Secure;HttpOnly',
          'foo=;Expires=0;Max-Age=-1;Domain=truc.org;Path=/;Secure;HttpOnly'
        ]) ;
    }) ;

    it('supprime tous les cookies', function () {

      req.headers.cookie = 'theo=net;kepha=js' ;
      user._loadCookies() ;
      user.setCookie('foo', 'bar') ;
      user.resetCookies() ;
      user._response.end() ;

      expect(user.hasCookie('theo')).to.be.false ;
      expect(user.hasCookie('kepha')).to.be.false ;
      expect(user.hasCookie('foo')).to.be.false ;
      expect(resp.getHeader('Set-Cookie'))
        .to.be.deep.equal([
          'kjs_session_id=' + user._sessionId + ';HttpOnly',
          'foo=bar',
          'theo=;Expires=0;Max-Age=-1',
          'kepha=;Expires=0;Max-Age=-1',
          'foo=;Expires=0;Max-Age=-1'
        ]) ;
    }) ;
  }) ;


  describe('session', function () {

    it('créé une nouvelle session', function () {

      let user2 = new User(req,
        new Resp(new http.ServerResponse('get')), sessions) ;

      expect(user._sessionId).to.be.not.equal(user2._sessionId) ;
    }) ;

    it('créé une nouvelle session configurée', function () {

      let config = {
        sessionCookieName: 'foobar',
        sessionCookieDomain: 'localhost',
        sessionCookiePath: '/hello',
        sessionCookieSecure: true,
        sessionCookieHttpOnly: true
      } ;
      let user2 = new User(req,
        new Resp(new http.ServerResponse('get')), sessions, config) ;
      let spy = sinon.spy(user2, 'setCookie') ;
      user2.reset() ;

      expect(spy.calledWith(
        'foobar',
        user2._sessionId,
        null, null,
        user2._sessionCookieDomain,
        '/hello',
        true, true))
        .to.be.true ;
    }) ;

    it('envoit l\'identifiant de la session au client', function () {

      user._response.end() ;
      expect(resp.getHeader('Set-Cookie')).to.be.deep.equal(
        ['kjs_session_id=' + user._sessionId + ';HttpOnly']
      ) ;
    }) ;

    it('récupère la session du client et fait coucou', function () {

      let spy = sinon.spy(sessions, 'hello') ;

      let id = sessions.init() ;
      sessions.set(id, '_browser', user.browserInfo().signature) ;
      req.headers.cookie = 'kjs_session_id=' + id ;
      user = new User(req, resp, sessions) ;

      expect(user._sessionId).to.be.equal(id) ;
      expect(spy.calledWith(id)).to.be.true ;
    }) ;

    it('réinitialise la session et détruit l\'ancienne', function () {

      let id = sessions.init() ;
      sessions.set(id, '_browser', user.browserInfo().signature) ;
      req.headers.cookie = 'kjs_session_id=' + id ;
      user = new User(req, resp, sessions) ;
      expect(user._sessionId).to.be.equal(id) ;
      user.reset() ;

      expect(user._sessionId).to.not.be.equal(id) ;
      expect(sessions.has(id)).to.be.false ;
    }) ;

    it('défini un élément dans la session', function () {

      user.set('foo', 'bar') ;
      expect(sessions.get(user._sessionId, 'foo')).to.be.equal('bar') ;
    }) ;

    it('indique si la session possède un élément', function () {

      sessions.set(user._sessionId, 'foo', 'bar') ;

      expect(user.has('foo')).to.be.equal(true) ;
      expect(user.has('theo')).to.be.equal(false) ;
    }) ;

    it('récupère la valeur d\'un élément', function () {

      sessions.set(user._sessionId, 'foo', 'bar') ;

      expect(user.get('foo')).to.be.equal('bar') ;
    }) ;

    it('si élement existe pas, retourne la valeur par défaut', function () {

      expect(user.get('foo', 'bar')).to.be.equal('bar') ;
    }) ;

    it('si default est une fonction, retourne son résultat', function () {

      expect(user.get('foo', () => { return 'bar' ; })).to.be.equal('bar') ;
    }) ;

    it('supprime un élément de la session', function () {

      sessions.set(user._sessionId, 'foo', 'bar') ;
      user.delete('foo') ;

      expect(user.has('foo')).to.be.equal(false) ;
    }) ;

    it('si le navigateur n\'est plus le même, reset', function () {

      req.headers['user-agent'] = 'foobar' ;
      req.headers.accept = 'text/html, application/xhtml+xml' ;
      req.headers['accept-language'] = 'fr-FR,fr, en-Us' ;
      req.headers['accept-encoding'] = 'gzip, deflate' ;
      user = new User(req, resp, sessions) ;
      let idOld = user._sessionId ;

      req.headers.cookie = 'kjs_session_id=' + idOld ;
      req.headers['user-agent'] = 'foobar2' ;
      req.headers.accept = 'text/html' ;
      req.headers['accept-language'] = 'en-Us' ;
      req.headers['accept-encoding'] = 'deflate' ;
      user = new User(req, resp, sessions) ;

      expect(user._sessionId).to.be.not.equal(idOld) ;
    }) ;

    it('On peut switcher vers une autres session', function () {

      let user2 = new User(req, resp, sessions) ;

      user2.switchSession(user._sessionId) ;

      expect(user2._sessionId).to.be.equal(user._sessionId) ;
    }) ;

    it('Lors d\'un switch, fait coucou avec le bon id', function () {

      let user2 = new User(req, resp, sessions),
          spy   = sinon.spy(sessions, 'hello') ;

      user2.switchSession(user._sessionId) ;

      expect(spy.calledWith(user._sessionId)).to.be.true ;
    }) ;

    it('Lors d\'un switch, cookie envoyé avec l\'id', function () {

      let user2 = new User(req, resp, sessions) ;
      let oldId = user2._sessionId ;
      user2.switchSession(user._sessionId) ;
      user2._response.end() ;

      expect(resp.getHeader('Set-Cookie')).to.be.deep.equal([
        'kjs_session_id=' + user._sessionId + ';HttpOnly',
        'kjs_session_id=' + oldId + ';HttpOnly',
        'kjs_session_id=' + user._sessionId + ';HttpOnly'
      ]) ;
    }) ;

    it('Lors d\'un switch, l\'ancienne session delete', function () {

      let user2 = new User(req, resp, sessions) ;
      let oldId = user2._sessionId ;
      user2.switchSession(user._sessionId) ;

      expect(sessions.has(oldId)).to.be.false ;
    }) ;

    it('erreur quand id ne correspond à aucune session', function () {

      let user2 = new User(req, resp, sessions) ;

      expect(user2.switchSession.bind(user2, 'foobar'))
        .to.throw(Error, 'unknown id') ;
    }) ;
  }) ;


  describe('flash message', function () {

    it('On peut enregistrer un message flash', function () {

      user.setFlash('msg', 'info') ;

      expect(user.get('flash'))
        .to.be.deep.equal([{type: 'info', content: 'msg'}]) ;
    }) ;

    it('On peut en enregister plusieurs', function () {

      user.setFlash('msg1', 'info') ;
      user.setFlash('msg2', 'success') ;

      expect(user.get('flash')).to.be.deep.equal([
        {type: 'info', content: 'msg1'},
        {type: 'success', content: 'msg2'}
      ]) ;
    }) ;

    it('indique si un/des message(s) sont enregistré(s)', function () {

      expect(user.hasFlash()).to.be.equal(false) ;
      user.setFlash('msg', 'info') ;
      expect(user.hasFlash()).to.be.equal(true) ;
    }) ;

    it('retourne la liste des messages', function () {

      user.setFlash('msg1', 'info') ;
      user.setFlash('msg2', 'success') ;

      expect(user.getFlash()).to.be.deep.equal([
        {type: 'info', content: 'msg1'},
        {type: 'success', content: 'msg2'}
      ]) ;
    }) ;

    it('quand on récupère la liste, vide celle-ci', function () {

      user.setFlash('msg1', 'info') ;
      user.setFlash('msg2', 'success') ;
      user.getFlash() ;

      expect(user.getFlash()).to.be.deep.equal([]) ;
      expect(user.hasFlash()).to.be.false ;
    }) ;
  }) ;


  describe('browserInfo', function () {

    it('retourne un object avec certains champs obligatoires', function () {

      expect(user.browserInfo()).to.be.deep.equal({
        'user-agent': '',
        'accept-language': [],
        signature: ''
      }) ;
    }) ;

    it('retourne l\'user agent', function () {

      req.headers['user-agent'] = 'foobar' ;
      expect(user.browserInfo()['user-agent']).to.be.equal('foobar') ;
      expect(user.browserInfo().signature).to.be.equal('foobar') ;
    }) ;

    it('retourne les langages acceptés', function () {

      req.headers['accept-language'] = 'fr-FR,fr, en-Us' ;
      expect(user.browserInfo()['accept-language']).to.be.deep.equal([
        'fr-FR',
        'fr',
        'en-Us'
      ]) ;
      expect(user.browserInfo().signature).to.be.equal('fr-FR,fr, en-Us') ;
    }) ;

    it('retourne une signature du navigateur', function () {

      req.headers['user-agent'] = 'foobar' ;
      req.headers.accept = 'text/html, application/xhtml+xml' ;
      req.headers['accept-language'] = 'fr-FR,fr, en-Us' ;

      expect(user.browserInfo().signature).to.be.equal(
        'foobarfr-FR,fr, en-Us'
      ) ;
    }) ;
  }) ;
}) ;
// buddy ignore:end

