'use strict' ;

/**
 * src/server/User/MemoryStorage.test.js
 */

const expect = require('chai').expect ;

const MemoryStorage = require('./MemoryStorage') ;

let storage ;

// buddy ignore:start
describe('server/User/MemoryStorage', function () {

  beforeEach(function () {

    storage = new MemoryStorage() ;
  }) ;


  it('init, initialise une session', function () {

    storage.init('hello') ;

    expect(storage._sessions.hello).to.not.be.undefined ;
    expect(storage._sessions.hello.content).to.not.be.undefined ;
    expect(storage._sessions.hello.lastAccess).to.not.be.undefined ;
    expect(typeof storage._sessions.hello.lastAccess)
      .to.be.equal('number') ;
  }) ;

  it('has, retourne `true` si la session existe', function () {

    storage.init('hello') ;

    expect(storage.has('hello')).to.be.equal(true) ;
  }) ;

  it('has, retourne `false` sinon', function () {

    storage.init('hello') ;

    expect(storage.has('foobar')).to.be.equal(false) ;
  }) ;

  it('remove détruit un élément', function () {

    storage.init('hello') ;
    storage.remove('hello') ;

    expect(storage.has('hello')).to.be.false ;
  }) ;

  it('set enregistre un élément dans une session', function () {

    storage.init('hello') ;
    storage.set('hello', 'foo', 'bar') ;

    expect(storage._sessions.hello.content.foo).to.be.equal('bar') ;
  }) ;

  it('hasElement indique si une session possède un élément', function () {

    storage.init('hello') ;
    storage.set('hello', 'foo', 'bar') ;

    expect(storage.hasElement('hello', 'foo')).to.be.equal(true) ;
    expect(storage.hasElement('hello', 'bar')).to.be.equal(false) ;
  }) ;

  it('get récupère un élément de la session', function () {

    storage.init('hello') ;
    storage.set('hello', 'foo', 'bar') ;

    expect(storage.get('hello', 'foo')).to.be.equal('bar') ;
  }) ;

  it('delete supprime un élément de la session', function () {

    storage.init('hello') ;
    storage.set('hello', 'foo', 'bar') ;
    storage.delete('hello', 'foo') ;

    expect(storage._sessions.hello.content.foo).to.be.undefined ;
    expect(storage.get('hello', 'foo')).to.be.undefined ;
  }) ;

  it('access met à jour le timestamp', function () {

    storage.init('hello') ;
    let timestamp = storage._sessions.hello.lastAccess ;
    storage._sessions.hello.lastAccess = 100 ;
    storage.access('hello') ;

    expect(storage._sessions.hello.lastAccess).to.be.equal(timestamp) ;
  }) ;

  it('count retourne le nombre de sessions enregistrées', function () {

    storage.init('foo') ;
    storage.init('bar') ;

    expect(storage.count()).to.be.equal(2) ;
  }) ;

  it('isTooOld indique si une sessions est trop vieille', function () {

    storage.init('foo') ;
    expect(storage.isTooOld('foo', Date.now() + 10)).to.be.equal(false) ;

    storage._sessions.foo.lastAccess = 0 ;
    expect(storage.isTooOld('foo', 200)).to.be.equal(true) ;
  }) ;

  it('garbage supprime toutes les sessions trop anciennes', function () {

    storage.init('hello') ;
    storage.init('foo') ;
    storage.init('bar') ;

    expect(storage.count()).to.be.equal(3) ;

    storage._sessions.foo.lastAccess = 100 ;
    storage._sessions.bar.lastAccess = 100 ;

    storage.garbage(200) ;

    expect(storage.count()).to.be.equal(1) ;
  }) ;
}) ;
// buddy ignore:end

