'use strict' ;

/**
 * src/server/Server.test.js
 */

const expect  = require('chai').expect,
      path = require('path') ;
const Server   = require('./Server'),
      Router   = require('./Services/Router'),
      Models   = require('./Services/Models'),
      Sessions = require('./User/Sessions'),
      Kfmd     = require('./Services/Kfmd'),
      Forms    = require('./Services/Forms'),
      Config   = require('../core/Services/Config') ;


const basepath = path.normalize(__dirname + '/../../doc/demo-server') ;
const config = {
  'basepath': basepath + '/',
  'staticDir': 'web/',
  'routes': 'config/routes.json',
  'foo': 'bar',
} ;


describe('server/Server', function () {

  let server,
      close ;

  beforeEach(function () {

    server = new Server(config) ;
    server.run(['start']) ;
    close = server.kernel().get('$config').get('$server') ;
  }) ;

  afterEach(function () {

    close.close() ;
    server.kernel().get('$config').delete('$server')  ;
  }) ;


  it('charge la configuration', function () {

    server.K.forEach(config, (value, id) => {
      expect(server.kernel().get('$config').get(id)).to.be.equal(value) ;
    }) ;
    server.kernel().get('$config').get('$server').close() ;
  }) ;

  it('initialise les canaux d\'events de bases', function () {

    expect(server.kernel().events().has('Router')).to.be.true ;
    expect(server.kernel().events().has('Slot')).to.be.true ;
  }) ;

  it('initialise les services', function () {

    expect(server.kernel().container().has('$router')).to.be.true ;
    expect(server.get('$router')).to.be.instanceOf(Router) ;
    expect(server.kernel().container().has('$models')).to.be.true ;
    expect(server.get('$models')).to.be.instanceOf(Models) ;
    expect(server.kernel().container().has('$sessions')).to.be.true ;
    expect(server.get('$sessions')).to.be.instanceOf(Sessions) ;
    expect(server.kernel().container().has('$kfmd')).to.be.true ;
    expect(server.get('$kfmd')).to.be.instanceOf(Kfmd) ;
    expect(server.kernel().container().has('$forms')).to.be.true ;
    expect(server.get('$forms')).to.be.instanceOf(Forms) ;
    expect(server.get('$forms').$validate)
      .to.be.equal(server.kernel().get('$validate')) ;
    expect(server.get('$staticCache')).to.be.instanceOf(Config) ;
    expect(server.kernel().container().has('$staticCache')).to.be.true ;
  }) ;

  it('charge les modèles avec setModels()', function () {

    let model1 = {},
        model2 = {} ;

    server.setModels({foo: model1, bar: model2}) ;

    expect(server.get('$models').has('foo')).to.be.true ;
    expect(server.get('$models').get('foo')).to.be.equal(model1) ;
    expect(server.get('$models').has('bar')).to.be.true ;
    expect(server.get('$models').get('bar')).to.be.equal(model2) ;
  }) ;

  it('charge les formBuilders avec setFormBuilders()', function () {

    let builder1 = {},
        builder2 = {} ;

    server.setFormBuilders({foo: builder1, bar: builder2}) ;

    expect(server.get('$forms').has('foo')).to.be.true ;
    expect(server.get('$forms')._formBuilders.foo).to.be.equal(builder1) ;
    expect(server.get('$forms').has('bar')).to.be.true ;
    expect(server.get('$forms')._formBuilders.bar).to.be.equal(builder2) ;
  }) ;

  it('exécute l\'init après le chargement de la configuration',
  function (done) {

    close.close() ;
    server.kernel().get('$config').delete('$server')  ;
    server = new Server(config) ;

    server.init = (config, kernel) => {

      expect(config.get('foo')).to.be.equal('bar') ;
      expect(kernel).to.be.equal(server.kernel()) ;

      done() ;
    } ;

    server.run(['start']) ;
    close = server.kernel().get('$config').get('$server') ;
  }) ;
}) ;

