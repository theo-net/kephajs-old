'use strict' ;

/**
 * src/server/Resp.test.js
 */

const expect = require('chai').expect,
      http   = require('http') ;

const Resp = require('./Resp') ;

describe('server/Resp', function () {

  it('setContent définit l\'header correspondant', function () {

    let resp = new Resp(new http.ServerResponse('get')) ;
    resp.setContentType('application/json') ;

    expect(resp.getHeader('Content-Type')).to.be.equal('application/json') ;
  }) ;

  it('setStatusCode définit le code de retour', function () {

    let resp = new Resp(new http.ServerResponse('get')) ;
    resp.setStatusCode(2) ;

    expect(resp._response.statusCode).to.be.equal(2) ;
  }) ;
}) ;

