'use strict' ;

/*
 * src/server/Services/Route/RouteError.js
 */

const BaseError = require('../../core/Error/BaseError') ;

class RouteError extends BaseError {
}

module.exports = RouteError ;

