'use strict' ;

/**
 * src/server/Services/Kfmd.js
 */

const Renderer = require('./Renderer') ;

/**
 * Ce service permet d'implémenter une version inspirée de la syntaxe
 * Markdown : Kepha Flavored Markdown.
 *
 * Pour l'utiliser, il suffit d'initialiser le service et de faire appel à la
 * méthode `render(texte à parser)` qui transformera la syntaxe en code HTML
 * valide.
 *
 * Le code est très très largement inspiré de *marked* :
 *   https://github.com/chjj/marked Christopher Jeffrey (MIT Licensed)
 * Nous l'avons transformé en service compatible pour notre framework, supprimé
 * les options et complété un peu pour que tout corresponde à nos besoins.
 *
 * Le rendu permet de récupérer plusieurs choses :
 *  - le code HTML correspondant
 *  - la table des matière (ou toc) : un `Array` contenant une serie d'objet
 *    `{type: 'hx', content: '...'}` avec `hx` entre `h1` et `h6`
 *  - la liste des notes : un `Array` contenant le texte de chaque note (le
 *    numéro correspond à son ordre d'arrivée.
 */
class Kfmd {

  /**
   * Initialise le service
   */
  constructor () {

    this._renderer = new Renderer() ;

    // safeHtml
    this._safeHtml = {
      block: 'h1|h2|h3|h4|h5|h6|ol|ul|li|table|thead|tbody|tr|th|td'
        + '|figure|caption|figcaption|div|blockquote|footer|iframe'
        + '|dl|dt|dd|p',
      inline: 'a|em|strong|small|s(?= |>)|cite|q|dfn|abbr|data|time|code'
        + '|var|samp|kbd|sub|sup|i|b|u|mark|ruby|rt|rp|bdi|bdo'
        + '|span|br|wbr|ins|del|img',
      regExpSource: '<(?!/?(?:block|inline))(.*?)>'
    } ;
    this._initSafeHtml() ;
  }


  /**
   * Définit le moteur de rendu
   * @param {Object} renderer Le moteur (doit au moins implémenter les méthodes
   *                          de Renderer
   */
  setRenderer (renderer) {

    this._renderer = renderer ;
  }


  /**
   * Ajout d'un élément inline au safeHtml
   * @param {String} element Élément à ajouter
   */
  addSafeInline (element) {

    if (this._safeHtml.inline.indexOf('|' + element + '|') < 0) {

      this._safeHtml.inline += '|' + element ;
      this._initSafeHtml() ;
    }
  }


  /**
   * Retrait d'un élément inline au safeHtml
   * @param {String} element Élément à retirer
   */
  removeSafeInline (element) {

    this._safeHtml.inline = this._safeHtml.inline
      .replace('|' + element + '|', '') ;
    this._initSafeHtml() ;
  }


  /**
   * Ajout d'un élément block au safeHtml
   * @param {String} element Élément à ajouter
   */
  addSafeBlock (element) {

    if (this._safeHtml.block.indexOf('|' + element + '|') < 0) {

      this._safeHtml.block += '|' + element ;
      this._initSafeHtml() ;
    }
  }


  /**
   * Retrait d'un élément block au safeHtml
   * @param {String} element Élément à retirer
   */
  removeSafeBlock (element) {

    this._safeHtml.block = this._safeHtml.block
      .replace('|' + element + '|', '') ;
    this._initSafeHtml() ;
  }


  /**
   * Parse un texte et renvoit un objet contenant les éléments issus de la
   * transformation :
   *    results = {
   *      source: 'texte injecté au départ',
   *      contentHtml: texte parsé,
   *      toc: ['éléments de la table des matières'],
   *      footNotes: ['notes de bas de page extraites du document']
   *    } ;
   *
   * @param {String} text Texte à parser
   * @returns {Object}
   */
  render (text) {

    let parsed = text,
        tokens ;

    // Préprocesseur
    text = text
      .replace(/\r\n|\r/g, '\n')
      .replace(/\t/g, '    ')
      .replace(/\u00a0/g, ' ')
      .replace(/\u2424/g, '\n') ;

    // Parsage
    tokens = this._lexer(text, true) ;
    parsed = this._parse(tokens) ;

    return {
      source: text,
      contentHtml: parsed,
      toc: tokens.toc,
      footNotes: tokens.footNotes
    } ;
  }


  /**
   * Initialise le safeHtml, ainsi que les syntaxe en fonction des éléments
   * autorisés ou interdits
   * @private
   */
  _initSafeHtml () {

    this._safeHtml.regExp = this._safeHtml.regExpSource
      .replace('block', this._safeHtml.block)
      .replace('inline', this._safeHtml.inline) ;
    this._safeHtml.regExp = new RegExp(this._safeHtml.regExp, 'gm') ;

    // Initalisation des regExp
    this._initBlock() ;
    this._initInline() ;
  }


  /**
   * Initialise les syntaxes de blocks
   * @private
   */
  _initBlock () {

    // Grammaire
    let block = {
      newline: /^\n+/,
      code: /^( {4}[^\n]+\n*)+/,
      fences: /^ *(`{3,}|~{3,})[ .]*(\S+)? *\n([\s\S]*?)\n? *\1 *(?:\n+|$)/,
      heading: /^ *(#{1,6}) +([^\n]+?) *#* *(?:\n+|$)/,
      blockquote: /^( {0,3}> ?(paragraph|[^\n]*)(?:\n|$))+/,
      list: /^( *)(bull) [\s\S]+?(?:\n{2,}(?! )(?!\1bull )\n*|\s*$)/,
      // eslint-disable-next-line max-len
      taskList: /^( *)(bull) \[x?\] [\s\S]+?(?:\n{2,}(?! )(?!\1bull \[x?\] )\n*|\s*$)/,
      // eslint-disable-next-line max-len
      html: /^ *(?:comment *(?:\n|\s*$)|closed *(?:\n{2,}|\s*$)|closing *(?:\n{2,}|\s*$))/,
      lheading: /^([^\n]+)\n *(=|-){2,} *(?:\n+|$)/,
      paragraph: /^([^\n]+(?:\n?(?!heading|lheading| {0,3}>|tag)[^\n]+)+)/,
      text: /^[^\n]+/,
      bullet: /(?:[*+-]|\d+\.)/,
      item: /^( *)(bull) [^\n]*(?:\n(?!\1bull )[^\n]*)*/,
      taskItem: /^( *)(bull) \[x?\] [^\n]*(?:\n(?!\1bull \[x?\] )[^\n]*)*/,
      _label: /(?:\\[[\]]|[^[\]])+/,
      _title: /(?:"(?:\\"|[^"]|"[^"\n]*")*"|'\n?(?:[^'\n]+\n?)*'|\([^()]*\))/,
    } ;

    // safeHtml (éléments blocs autorisés : les autres seront placés dans un paragraphe)
    block._tag = '(?=(?:' + this._safeHtml.block
      + ')\\b)\\w+(?!:|[^\\w\\s@]*@)\\b' ;

    // On remplace des morceaux
    block.item = this._replace(block.item, 'gm')(/bull/g, block.bullet)() ;
    block.list = this._replace(block.list)(/bull/g, block.bullet)() ;
    block.taskItem = this._replace(block.taskItem, 'gm')(
      /bull/g, block.bullet)() ;
    block.taskList = this._replace(block.taskList)(/bull/g, block.bullet)() ;
    block.html = this._replace(block.html)('comment', /<!--[\s\S]*?-->/)(
      'closed', /<(tag)[\s\S]+?<\/\1>/)('closing',
      // eslint-disable-next-line no-useless-escape
      /<tag(?:"[^"]*"|'[^']*'|\s[^'"\/>]*)*?\/?>/)(/tag/g, block._tag)() ;
    block.paragraph = this._replace(block.paragraph)('heading', block.heading)(
      'lheading', block.lheading)('tag', '<' + block._tag)() ;
    block.blockquote = this._replace(block.blockquote)(
      'paragraph', block.paragraph)() ;

    // eslint-disable-next-line max-len
    block.nptable = /^ *(\S.*\|.*)\n *([-:]+ *\|[-| :]*)\n((?:.*\|.*(?:\n|$))*)\n*/ ;
    block.table = /^ *\|(.+)\n *\|( *[-:]+[-| :]*)\n((?: *\|.*(?:\n|$))*)\n*/ ;

    this._block = block ;
  }


  /**
   * Initialise les syntaxes inline
   * @private
   */
  _initInline () {

    let inline = {
      escape: /^\\([\\`*{}[\]()#+\-.!_>])/,
      // eslint-disable-next-line no-useless-escape
      tag: /^<!--[\s\S]*?-->|^<\/?_tag(?:"[^"]*"|'[^']*'|\s[^<\'">\/]*)*?\/?>/,
      link: /^!?\[(inside)\]\(href\)/,
      nolink: /^!?\[((?:\[[^\]]*\]|\\[[\]]|[^[\]])*)\]/,
      strong: /^__([\s\S]+?)__(?!_)|^\*\*([\s\S]+?)\*\*(?!\*)/,
      em: /^_([^\s_](?:[^_]|__)+?[^\s_])_\b|^\*((?:\*\*|[^*])+?)\*(?!\*)/,
      code: /^(`+)(\s*)([\s\S]*?[^`]?)\2\1(?!`)/,
      br: /^ {2,}\n(?!\s*$)/,
      text: /^[\s\S]+?(?=[\\<![`*]|\b_| {2,}\n|$)/,
      footNote: /\(\(((?:.|\n)+?)\)\)/gm
    } ;

    // safeHtml (éléments inline autorisés, les autres seront échapés)
    inline._tag = '(?:' + this._safeHtml.inline + ')' ;
    inline.tag = this._replace(inline.tag)('_tag', inline._tag)() ;

    inline._scheme = /[a-zA-Z][a-zA-Z0-9+.-]{1,31}/ ;
    inline._email = new RegExp('[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+(@)'
      + '[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?'
      + '(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+(?![-_])') ;

    inline._inside = /(?:\[[^\]]*\]|\\[[\]]|[^[\]]|\](?=[^[]*\]))*/ ;
    inline._href = /\s*<?([\s\S]*?)>?(?:\s+['"]([\s\S]*?)['"])?\s*/ ;

    inline.link = this._replace(inline.link)(
      'inside', inline._inside)('href', inline._href)() ;

    inline.escape = this._replace(inline.escape)('])', '~|])')() ;
    inline.url = this._replace(
      /^((?:ftp|https?):\/\/|www\.)(?:[a-zA-Z0-9-]+\.?)+[^\s<]*|^email/
    )('email', inline._email)() ;
    inline._backpedal =
      /(?:[^?!.,:;*_~()&]+|\([^)]*\)|&(?![a-zA-Z0-9]+;$)|[?!.,:;*_~)]+(?!$))+/ ;
    inline.del = /^~~(?=\S)([\s\S]*?\S)~~/ ;
    inline.text = this._replace(inline.text)(']|', '~]|')('|',
      '|https?://|ftp://|www\\.|[a-zA-Z0-9.!#$%&\'*+/=?^_`{\\|}~-]+@|')() ;

    this._inline = inline ;
  }


  /**
   * Tekennise le texe et retourne les tokens
   * @param {String} text Texte à tokenniser
   * @param {Boolean} top Indique si on est au premier niveau
   * @returns {Array}
   * @private
   */
  _lexer (text, top) {

    let tokens = [],
        cap, bull, next, l, i, item, space, b, loose ;

    let toc = [] ;
    tokens.links = {} ;

    // Préprocesseur
    text = text.replace(/^ +$/gm, '') ;

    // Lexing
    while (text) {

      // newline
      cap = this._block.newline.exec(text) ;
      if (cap) {

        text = text.substring(cap[0].length) ;
        if (cap[0].length > 1)
          tokens.push({type: 'space'}) ;
      }

      // code
      cap = this._block.code.exec(text) ;
      if (cap) {

        text = text.substring(cap[0].length) ;
        cap = cap[0].replace(/^ {4}/gm, '') ;
        tokens.push({
          type: 'code',
          text: cap.replace(/n+$/, '')
        }) ;
        continue ;
      }

      // fences (code ave ```)
      cap = this._block.fences.exec(text) ;
      if (cap) {

        text = text.substring(cap[0].length) ;
        tokens.push({
          type: 'code',
          lang: cap[2],
          text: cap[3] || '' // buddy ignore:line
        }) ;
        continue ;
      }

      // heading
      cap = this._block.heading.exec(text) ;
      if (cap) {

        text = text.substring(cap[0].length) ;
        tokens.push({
          type: 'heading',
          depth: cap[1].length,
          text: cap[2]
        }) ;
        toc.push({type: cap[1].length, content: cap[2]}) ;
        continue  ;
      }

      // blockquote
      cap = this._block.blockquote.exec(text) ;
      if (cap) {

        text = text.substring(cap[0].length) ;

        tokens.push({
          type: 'blockquote_start'
        }) ;

        cap = cap[0].replace(/^ *> ?/gm, '') ;

        // Pass `top` to keep the current
        // "toplevel" state. This is exactly
        // how markdown.pl works.
        tokens = tokens.concat(this._lexer(cap, top)) ;

        tokens.push({
          type: 'blockquote_end'
        }) ;

        continue ;
      }

      // task list
      cap = this._block.taskList.exec(text) ;
      if (cap) {

        text = text.substring(cap[0].length) ;
        bull = cap[2] ;

        tokens.push({
          type: 'taskList_start',
          ordered: bull.length > 1
        }) ;

        // Get each top-level item.
        cap = cap[0].match(this._block.taskItem) ;

        next = false ;
        l = cap.length ;
        i = 0 ;

        for (; i < l ; i++) {

          item = cap[i] ;

          // Remove the list item's bullet
          // so it is seen as the next token.
          space = item.length ;
          let checked = !!/^ *([*+-]|\d+\.) \[x\] +/.exec(item) ;
          item = item.replace(/^ *([*+-]|\d+\.) \[x?\] +/, '') ;

          // Outdent whatever the
          // list item contains. Hacky.
          if (~item.indexOf('\n ')) {

            space -= item.length ;
            item = item.replace(new RegExp('^ {1,' + space + '}', 'gm'), '') ;
          }

          // Determine whether the next list item belongs here.
          // Backpedal if it does not belong in this list.
          if (i !== l - 1) {

            b = this._block.bullet.exec(cap[i + 1])[0] ;
            if (bull !== b && !(bull.length > 1 && b.length > 1)) {
              text = cap.slice(i + 1).join('\n') + text ;
              i = l - 1 ;
            }
          }

          // Determine whether item is loose or not.
          // Use: /(^|\n)(?! )[^\n]+\n\n(?!\s*$)/
          // for discount behavior.
          loose = next || /\n\n(?!\s*$)/.test(item) ;
          if (i !== l - 1) {

            next = item.charAt(item.length - 1) === '\n' ;

            if (!loose) loose = next ;
          }

          tokens.push({
            type: loose ? 'loose_taskItem_start' : 'taskList_item_start',
            checked: checked
          }) ;

          // Recurse.
          tokens = tokens.concat(this._lexer(item, false)) ;

          tokens.push({type: 'taskList_item_end'}) ;
        }

        tokens.push({type: 'taskList_end'}) ;

        continue ;
      }

      // list
      cap = this._block.list.exec(text) ;
      if (cap) {

        text = text.substring(cap[0].length) ;
        bull = cap[2] ;

        tokens.push({
          type: 'list_start',
          ordered: bull.length > 1
        }) ;

        // Get each top-level item.
        cap = cap[0].match(this._block.item) ;

        next = false ;
        l = cap.length ;
        i = 0 ;

        for (; i < l ; i++) {

          item = cap[i] ;

          // Remove the list item's bullet
          // so it is seen as the next token.
          space = item.length ;
          item = item.replace(/^ *([*+-]|\d+\.) +/, '') ;

          // Outdent whatever the
          // list item contains. Hacky.
          if (~item.indexOf('\n ')) {

            space -= item.length ;
            item = item.replace(new RegExp('^ {1,' + space + '}', 'gm'), '') ;
          }

          // Determine whether the next list item belongs here.
          // Backpedal if it does not belong in this list.
          if (i !== l - 1) {

            b = this._block.bullet.exec(cap[i + 1])[0] ;
            if (bull !== b && !(bull.length > 1 && b.length > 1)) {
              text = cap.slice(i + 1).join('\n') + text ;
              i = l - 1 ;
            }
          }

          // Determine whether item is loose or not.
          // Use: /(^|\n)(?! )[^\n]+\n\n(?!\s*$)/
          // for discount behavior.
          loose = next || /\n\n(?!\s*$)/.test(item) ;
          if (i !== l - 1) {

            next = item.charAt(item.length - 1) === '\n' ;

            if (!loose) loose = next ;
          }

          tokens.push({type: loose ? 'loose_item_start' : 'list_item_start'}) ;

          // Recurse.
          tokens = tokens.concat(this._lexer(item, false)) ;

          tokens.push({type: 'list_item_end'}) ;
        }

        tokens.push({type: 'list_end'}) ;

        continue ;
      }

      // html
      cap = this._block.html.exec(text) ;
      if (cap) {

        text = text.substring(cap[0].length) ;
        tokens.push({
          type: 'html',
          text: cap[0]
        }) ;
        continue ;
      }

      // table
      if (top && (cap = this._block.table.exec(text))) {

        text = text.substring(cap[0].length) ;

        item = {
          type: 'table',
          header: cap[1].replace(/^ *| *\| *$/g, '').split(/ *\| */),
          align: cap[2].replace(/^ *|\| *$/g, '').split(/ *\| */),
          cells: cap[3].replace(/(?: *\| *)?\n$/, '').split('\n') // buddy ignore:line
        } ;

        item.align.forEach((align, i) => {

          if (/^ *-+: *$/.test(align))
            item.align[i] = 'right' ;
          else if (/^ *:-+: *$/.test(align))
            item.align[i] = 'center' ;
          else if (/^ *:-+ *$/.test(align))
            item.align[i] = 'left' ;
          else
            item.align[i] = null ;
        }) ;

        item.cells.forEach((cell, i) => {
          item.cells[i] = cell.replace(/^ *\| *| *\| *$/g, '').split(/ *\| */) ;
        }) ;

        tokens.push(item) ;
        continue ;
      }

      // lheading
      cap = this._block.lheading.exec(text) ;
      if (cap) {

        text = text.substring(cap[0].length) ;
        tokens.push({
          type: 'heading',
          depth: cap[2] === '=' ? 1 : 2,
          text: cap[1]
        }) ;
        toc.push({type: cap[2] === '=' ? 1 : 2, content: cap[1]}) ;
        continue ;
      }

      // text
      cap = this._block.text.exec(text) ;
      if (cap) {

        // Top-level should never reach here.
        text = text.substring(cap[0].length) ;
        tokens.push({
          type: 'text',
          text: cap[0]
        }) ;
        continue ;
      }

      if (text)
        throw new Error('Infinite loop on byte: ' + text.charCodeAt(0)) ;
    }

    tokens.toc = toc ;
    return tokens ;
  }


  /**
   * Parse le texte à partir de sa tokenisation
   * @param {Array} tokens Tokens
   * @returns {String}
   * @private
   */
  _parse (tokens) {

    let token ;

    tokens = tokens.reverse() ;

    let out = '' ;
    while ((token = tokens.pop()) !== undefined)
      out += this._tok(token, tokens) ;

    return out ;
  }


  /**
   * Parse le `token` et génère son rendu.
   * @param {Object} token Token courant
   * @param {Array} tokens Liste des tokens pas encore traités
   * @return {String}
   * @private
   */
  _tok (token, tokens) {

    switch (token.type) {

      case 'space': {
        return '' ;
      }
      case 'heading': {
        return this._renderer.heading(
          this._output(token.text, tokens),
          token.depth,
          this._unescape(token.text)) ;
      }
      case 'code': {
        return this._renderer.code(this._escape(token.text, true), token.lang) ;
      }
      case 'table': {
        let header = '',
            body = '',
            cell = '' ;

        // header
        token.header.forEach((th, i) => {

          cell += this._renderer.tablecell(
            this._output(th, tokens),
            {header: true, align: token.align[i]}
          ) ;
        }) ;
        header += this._renderer.tablerow(cell) ;

        token.cells.forEach(row => {

          cell = '' ;

          row.forEach((td, i) => {

            cell += this._renderer.tablecell(
              this._output(td, tokens),
              {header: false, align: token.align[i]}
            ) ;
          }) ;

          body += this._renderer.tablerow(cell) ;
        }) ;
        return this._renderer.table(header, body) ;
      }

      case 'blockquote_start': {
        let body = '' ;

        while ((token = tokens.pop()).type !== 'blockquote_end')
          body += this._tok(token, tokens) ;

        return this._renderer.blockquote(body) ;
      }
      case 'taskList_start': {
        let body = '',
            ordered = token.ordered ;

        while ((token = tokens.pop()).type !== 'taskList_end')
          body += this._tok(token, tokens) ;

        return this._renderer.taskList(body, ordered) ;
      }
      case 'taskList_item_start': {
        let body = '',
            checked = token.checked ;

        while ((token = tokens.pop()).type !== 'taskList_item_end') {

          body += token.type === 'text' ?
            this._parseText(token, tokens)
            : this._tok(token, tokens) ;
        }

        return this._renderer.taskListItem(body, checked) ;
      }
      case 'loose_taskItem_start': {
        let body = '',
            checked = token.checked ;

        while ((token = tokens.pop()).type !== 'list_taskItem_end')
          body += this.tok(token, tokens) ;

        return this._renderer.taskListItem(body, checked) ;
      }
      case 'list_start': {
        let body = '',
            ordered = token.ordered ;

        while ((token = tokens.pop()).type !== 'list_end')
          body += this._tok(token, tokens) ;

        return this._renderer.list(body, ordered) ;
      }
      case 'list_item_start': {
        let body = '' ;

        while ((token = tokens.pop()).type !== 'list_item_end') {

          body += token.type === 'text' ?
            this._parseText(token, tokens)
            : this._tok(token, tokens) ;
        }

        return this._renderer.listItem(body) ;
      }
      case 'loose_item_start': {
        let body = '' ;

        while ((token = tokens.pop()).type !== 'list_item_end')
          body += this.tok(token, tokens) ;

        return this._renderer.listItem(body) ;
      }
      case 'html': {
        token.text = token.text.replace(this._safeHtml.regExp, '&lt;$1&gt;')
          .replace(/&(?!#?\w+;)/g, '&amp;') ;
        return this._renderer.html(token.text) ;
      }
      case 'text': {
        return this._renderer.paragraph(this._parseText(token, tokens)) ;
      }
    }
  }


  /**
   * Parse un texte
   * @param {Object} token Token du texte
   * @param {Array} tokens Tokens non encore traités (pour avoir accès
   *                        au suivant)
   * @returns {String}
   * @private
   */
  _parseText (token, tokens) {

    let body = token.text ;

    while ((tokens[tokens.length - 1] || 0).type === 'text')
      body += '\n' + tokens.pop().text ;

    return this._output(body, tokens) ;
  }


  /**
   * Rendu d'un élément
   * @param {String} src Élément à traiter
   * @param {Array} [tokens] Liste des tokens
   * @returns {String}
   * @private
   */
  _output (src, tokens) {

    let out = '',
        text,
        href,
        cap ;

    // footNotes
    if (!tokens) tokens = [] ;
    if (!tokens.footNotes) tokens.footNotes = [] ;
    src = src.replace(this._inline.footNote, (m, $1) => {

      tokens.footNotes.push(this._output($1)) ;
      return this._renderer.footNote(tokens.footNotes.length) ;
    }) ;

    while (src) {

      // escape
      cap = this._inline.escape.exec(src) ;
      if (cap) {
        src = src.substring(cap[0].length) ;
        out += cap[1] ;
        continue ;
      }

      // url
      cap = this._inline.url.exec(src) ;
      if (cap) {
        cap[0] = this._inline._backpedal.exec(cap[0])[0] ;
        src = src.substring(cap[0].length) ;
        if (cap[2] === '@') {
          text = this._escape(cap[0]) ;
          href = 'mailto:' + text ;
        } else {
          text = this._escape(cap[0]) ;
          if (cap[1] === 'www.')
            href = 'http://' + text ;
          else
            href = text ;
        }
        out += this._renderer.link(href, null, text) ;
        continue ;
      }

      // tag
      cap = this._inline.tag.exec(src) ;
      if (cap) {
        src = src.substring(cap[0].length) ;
        out += cap[0] ;
        continue ;
      }

      // link
      cap = this._inline.link.exec(src) ;
      if (cap) {
        src = src.substring(cap[0].length) ;
        out += this._outputLink(cap, {
          href: cap[2],
          title: cap[3] // buddy ignore:line
        }) ;
        continue ;
      }

      // strong
      cap = this._inline.strong.exec(src) ;
      if (cap) {
        src = src.substring(cap[0].length) ;
        out += this._renderer.strong(this._output(cap[2] || cap[1], tokens)) ;
        continue ;
      }

      // em
      cap = this._inline.em.exec(src) ;
      if (cap) {
        src = src.substring(cap[0].length) ;
        out += this._renderer.em(this._output(cap[2] || cap[1], tokens)) ;
        continue ;
      }

      // code
      cap = this._inline.code.exec(src) ;
      if (cap) {
        src = src.substring(cap[0].length) ;
        out += this._renderer.codespan(this._escape(cap[3].trim(), true)) ; // buddy ignore:line
        continue ;
      }

      // br
      cap = this._inline.br.exec(src) ;
      if (cap) {

        src = src.substring(cap[0].length) ;
        out += this._renderer.br() ;
        continue ;
      }

      // del
      cap = this._inline.del.exec(src) ;
      if (cap) {
        src = src.substring(cap[0].length) ;
        out += this._renderer.del(this._output(cap[1]), tokens) ;
        continue ;
      }

      // text
      cap = this._inline.text.exec(src) ;
      if (cap) {

        src = src.substring(cap[0].length) ;
        out += this._renderer.text(this._escape(this._smartypants(cap[0]))) ;
        continue ;
      }

      if (src)
        throw new Error('Infinite loop on byte: ' + src.charCodeAt(0)) ;
    }
    return out ;
  }


  /**
   * Rendu d'un lien
   * @param {Array} cap Expression capturée par la RegExp
   * @param {Object} link Élément du lien (href et title)
   * @returns {String}
   * @private
   */
  _outputLink (cap, link) {

    let href  = this._escape(link.href),
        title = link.title ? this._escape(link.title) : null ;

    return this._renderer.link(href, title, this._output(cap[1])) ;
  }


  /**
   * Remplace une partie d'une RegExp par une autre
   * @param {String|RegExp} regex RegExp à traiter
   * @param {String|RegExp} opt Chaîne que l'on remplace
   * @returns {Function}
   * @private
   */
  _replace (regex, opt) {

    regex = regex.source ;
    opt = opt || '' ;

    return function self (name, val) {

      if (!name) return new RegExp(regex, opt) ;

      val = val.source || val ;
      val = val.replace(/(^|[^[])\^/g, '$1') ;
      regex = regex.replace(name, val) ;
      return self ;
    } ;
  }


  /**
   * Quelques transformations cosmétiques
   * @param {String} text Texte à traiter
   * @returns {String}
   * @private
   */
  _smartypants (text) {

    return text
      // em-dashes
      .replace(/---/g, '\u2014')
      // en-dashes
      .replace(/--/g, '\u2013')
      // opening singles
      .replace(/(^|[-\u2014/([{"\s])'/g, '$1\u2018')
      // closing singles & apostrophes
      .replace(/'/g, '\u2019')
      // opening doubles
      .replace(/(^|[-\u2014/([{\u2018\s])"/g, '$1\u201c')
      // closing doubles
      .replace(/"/g, '\u201d')
      // ellipses
      .replace(/\.{3}/g, '\u2026') ;
  }


  /**
   * Protection de caractères
   * @param {String} html HTML à échaper
   * @param {Boolean} encode Encoder ?
   * @returns {String}
   * @private
   */
  _escape (html, encode) {

    return html
      .replace(!encode ? /&(?!#?\w+;)/g : /&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/"/g, '&quot;')
      .replace(/'/g, '&#39;') ;
  }


  /**
   * unescape
   * @param {String} html HTML
   * @returns {String}
   * @private
   */
  _unescape (html) {

    // explicitly match decimal, hex, and named HTML entities
    return html.replace(/&(#(?:\d+)|(?:#x[0-9A-Fa-f]+)|(?:\w+));?/ig,
      (_, n) => {
        n = n.toLowerCase() ;
        if (n === 'colon') return ':' ;
        if (n.charAt(0) === '#') {
          return n.charAt(1) === 'x' ?
            String.fromCharCode(parseInt(n.substring(2), 16)) // buddy ignore:line
            : String.fromCharCode(+n.substring(1)) ;
        }
        return '' ;
      }
    ) ;
  }
}

module.exports = Kfmd ;

