'use strict' ;

/**
 * src/core/Services/Models.js
 */

/**
 * Service permettant d'accéder aux modèles de l'application
 *
 *    let models = new Models() ;
 *    models.add({
 *      'model1': model1,
 *      'model2': model2
 *    }) ;
 *    let model = models.get('model2') ;
 */
class Models {

  constructor () {

    this._models = {} ;
  }


  /**
   * Enregistre un modèle
   *
   * @param {String} id Identifiant du modèle
   * @param {Object} model Modèle à enregistrer
   *                  identifiant (vaut `false` par défaut).
   */
  register (id, model) {

    this._models[id] = model ;
  }


  /**
   * Retourne un modèle ou null si le modèle n'existe pas
   *
   * @param {String} id Identifiant
   * @returns {Object}
   */
  get (id) {

    return this.has(id) ? this._models[id] : null ;
  }


  /**
   * Indique si un modèle est enregistré ou non
   * @param {String} id Identifiant de l'élément à tester
   * @returns {Boolean}
   */
  has (id) {

    return Object.keys(this._models).indexOf(id) > -1 ;
  }


  /**
   * Supprime un élément
   * @param {String} id Identifiant de l'élément à supprimer
   */
  delete (id) {

    delete this._models[id] ;
  }
}

module.exports = Models ;

