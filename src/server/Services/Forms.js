'use strict' ;

/**
 * src/server/Services/Forms.js
 */

const Utils = require('../../core/Utils') ;

/**
 * Gère les formBuilder : ce service permet d'initialiser et de traiter
 * facilement un formulaire
 */
class Forms {

  /**
   * Initialise le service
   * @param {Object} validate Service $validate
   */
  constructor (validate) {

    this._formBuilders = {} ;
    this._fields = {} ;
    this._class = {
      field: [],
      label: [],
      group: [],
      groupSuccess: [],
      groupError: [],
      msg: [],
      msgHelp: [],
      msgError: [],
      fieldset: []
    } ;

    this.$validate = validate ;

    this.registerField('string', require('../Form/StringField')) ;
    this.registerField('email', require('../Form/EmailField')) ;
    this.registerField('password', require('../Form/PasswordField')) ;
    this.registerField('search', require('../Form/SearchField')) ;
    this.registerField('tel', require('../Form/TelField')) ;
    this.registerField('url', require('../Form/UrlField')) ;
    this.registerField('number', require('../Form/NumberField')) ;
    this.registerField('range', require('../Form/RangeField')) ;
    this.registerField('color', require('../Form/ColorField')) ;
    this.registerField('date', require('../Form/DateField')) ;
    this.registerField('time', require('../Form/TimeField')) ;
    this.registerField('hidden', require('../Form/HiddenField')) ;
    this.registerField('submit', require('../Form/SubmitField')) ;
    this.registerField('button', require('../Form/ButtonField')) ;
    this.registerField('reset', require('../Form/ResetField')) ;
    this.registerField('radio', require('../Form/RadioField')) ;
    this.registerField('checkbox', require('../Form/CheckboxField')) ;
    this.registerField('text', require('../Form/TextField')) ;
    this.registerField('select', require('../Form/SelectField')) ;
    this.registerField('html', require('../Form/HtmlField')) ;
  }


  /**
   * Enregistre un formBuilder (écrasera un précédemment définit)
   * @param {String} id Identifiant du FormBuilder
   * @param {FormBuilder} formBuilder FormBuilder à enregistrer
   * @returns {this} pour le chaînage
   */
  register (id, formBuilder) {

    this._formBuilders[id] = formBuilder ;
    return this ;
  }


  /**
   * Construit et retourne un formulaire
   * @param {String} id Identifiant du formBuilder à utiliser
   * @param {Entity} [entity=null] Entité à transmettre
   * @param {*} [config=null] Configuration que l'on peut transmettre au form
   * @returns {Form}
   */
  build (id, entity = null, config = null) {

    let form =  new this._formBuilders[id](entity, this) ;

    form.getForm().addClass(this._class) ;

    form.build(config) ;

    return form.getForm() ;
  }


  /**
   * Indique si un FormBuilder est enregistré ou nom
   * @param {String} id Identifiant du FormBuilder
   * @returns {Boolean}
   */
  has (id) {

    return Object.keys(this._formBuilders).indexOf(id) > -1 ;
  }


  /**
   * Supprime un FormBuilder enegistré
   * @param {String} id Identifiant du FormBuilder à retirer
   */
  delete (id) {

    delete this._formBuilders[id] ;
  }


  /**
   * Enregistre un fiedl (écrasera un précédemment définit)
   * @param {String} id Identifiant du field
   * @param {Field} field Field à enregistrer
   */
  registerField (id, field) {

    this._fields[id] = field ;
  }


  /**
   * Retourne un field
   * @param {String} id Identifiant du field à retourner
   * @returns {Field}
   */
  getField (id) {

    if (!this.hasField(id))
      throw new Error('Le type de Field `' + id + '` n\'est pas enregistré') ;
    return  this._fields[id] ;
  }


  /**
   * Indique si un field est enregistré ou nom
   * @param {String} id Identifiant du field
   * @returns {Boolean}
   */
  hasField (id) {

    return Object.keys(this._fields).indexOf(id) > -1 ;
  }


  /**
   * Supprime un field enegistré
   * @param {String} id Identifiant du field à retirer
   */
  deleteField (id) {

    delete this._fields[id] ;
  }


  /**
   * Ajoute automatiquement des classes à tous les champs ajoutés au formulaire.
   * Cf. Field.addClass()
   * @param {String|Array} cssObj Les classes à ajouter
   * @returns {this}
   */
  addClass (cssObj) {

    if (Utils.isObject(cssObj)) {

      Utils.forEach(cssObj, (css, type) => {

        if (Array.isArray(this._class[type])) {

          if (Utils.isString(css))
            this._class[type].push(css) ;
          else if (Array.isArray(css))
            this._class[type] = this._class[type].concat(css) ;
        }
      }) ;
    }

    return this ;
  }
}

module.exports = Forms ;

