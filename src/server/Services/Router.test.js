'use strict' ;

/**
 * src/server/Services/Router.test.js
 */

const expect  = require('chai').expect,
      sinon   = require('sinon') ;
const Router     = require('./Router'),
      Route      = require('./Route'),
      RouteError = require('./RouteError') ;

describe('server/Services/Router', function () {

  let router ;

  beforeEach(function () {

    router = new Router('../../../doc/demo-server/') ;
  }) ;

  describe('load', function () {

    it('charge des routes à partir d\'un objet', function () {

      router.load({
        test: {
          method: 'get',
          pattern: 'test',
          controller: 'index',
          action: 'index',
          defaults: {
            a: 1, b: 'c'
          },
          requirements: {
            a: '\\d', b: '\\w'
          },
          domain: 'test.com'
        }
      }) ;

      expect(router._routes.test._method).to.be.equal('get') ;
      expect(router._routes.test._pattern).to.be.equal('test') ;
      expect(router._routes.test._defaults).to.be.deep.equal({a: 1, b: 'c'}) ;
      expect(router._routes.test._requirements)
        .to.be.deep.equal({a: '\\d', b: '\\w'}) ;
      expect(router._routes.test._callable)
        .to.be.deep.equal(['index', 'index']) ;
      expect(router._routes.test._domain).to.be.equal('test.com') ;
    }) ;

    it('préfixe toutes les routes si demandé', function () {

      router.load({
        test: {
          method: 'get',
          pattern: 'test',
          controller: 'index',
          action: 'index',
        }
      }, 'un_', 'le') ;

      expect(router._routes.leTest._pattern).to.be.equal('un_test') ;
    }) ;

    it('détermine un domaine par défaut si demandé', function () {

      router.load({
        test1: {
          method: 'get',
          pattern: 'test',
          controller: 'index',
          action: 'index',
          domain: 'test.com'
        },
        test2: {
          method: 'get',
          pattern: 'test',
          controller: 'index',
          action: 'index',
        }
      }, null, null, 'domain.com') ;

      expect(router._routes.test1._domain).to.be.equal('test.com') ;
      expect(router._routes.test2._domain).to.be.equal('domain.com') ;
    }) ;

    it('charge des routes à partir d\'un fichier', function () {

      router.load('config/routes.json') ;

      expect(router._routes.index).to.be.instanceof(Route) ;
      expect(router._routes.styles).to.be.instanceof(Route) ;
      expect(router._routes.index._pattern).to.be.equal('/') ;
      expect(router._routes.styles._pattern).to.be.equal('/{file}') ;
    }) ;

    it('gère l\'inclusion de fichiers', function () {

      router.load('config/routes.json') ;

      expect(router._routes.adminIndex).to.be.instanceof(Route) ;
      expect(router._routes.adminIndex._pattern).to.be.equal('/admin/') ;
    }) ;

    it('lors de l\'inclusion on peut spécifier le prefix du pattern',
    function () {

      router.load({
        admin: {
          ressource: 'config/routesAdmin.json',
          prefix: 'admin',
          pattern: ''
        },
        admin2: {
          ressource: 'config/routesAdmin.json',
          prefix: 'admin',
          pattern: 'ok'
        }
      }) ;

      expect(router._routes.adminIndex).to.be.instanceof(Route) ;
      expect(router._routes.adminIndex._pattern).to.be.equal('/') ;
      expect(router._routes.admin2Index).to.be.instanceof(Route) ;
      expect(router._routes.admin2Index._pattern).to.be.equal('ok/') ;
    }) ;

    it('par l\'inclusion, on peut définir des vars par défaut', function () {

      router.load({
        admin: {
          ressource: 'config/routesAdmin.json',
          prefix: 'admin',
          defaults: {
            foo: 'bar',
            bar: 'foo'
          }
        },
        admin2: {
          ressource: 'config/routesAdmin.json',
          prefix: 'admin',
        }
      }) ;

      expect(router._routes.admin2Index._defaults).to.be.deep.equal({}) ;
      expect(router._routes.admin2Test._defaults).to.be.deep.equal({
        foo: 'bar2'
      }) ;
      expect(router._routes.adminIndex._defaults).to.be.deep.equal({
        foo: 'bar',
        bar: 'foo'
      }) ;
      expect(router._routes.adminTest._defaults).to.be.deep.equal({
        foo: 'bar2',
        bar: 'foo'
      }) ;
    }) ;

    it('construit une ressource controller/action', function () {

      router.load({
        test: {
          method: 'get',
          pattern: 'test',
          controller: 'ctrl',
          action: 'act'
        }
      }) ;

      expect(router._routes.test.getCallable())
        .to.be.deep.equal(['ctrl', 'act']) ;
    }) ;

    it('construit une ressource @*', function () {

      router.load({
        test: {
          method: 'get',
          pattern: 'test',
          controller: '@ctrl'
        }
      }) ;

      expect(router._routes.test.getCallable())
        .to.be.deep.equal(['@ctrl']) ;
    }) ;
  }) ;


  describe('getRoute', function () {

    it('parse une URL pour retourner la bonne route', function () {

      router.load({
        route1: {
          method: 'get',
          pattern: '/index.html'
        },
        route2: {
          method: 'get',
          pattern: '/foobar',
          domain: 'forum.domain..*'
        },
        route3: {
          method: ['get', 'post'],
          pattern: '/index.html'
        }
      }) ;

      expect(router.getRoute('get', '/index.html', 'lol.fr').route)
        .to.be.equal(router._routes.route1) ;
      expect(router.getRoute('gEt', '/index.html', 'lol.fr').route)
        .to.be.equal(router._routes.route1) ;
      expect(router.getRoute('get', '/foobar', 'forum.domain.fr').route)
        .to.be.equal(router._routes.route2) ;
      expect(router.getRoute('gEt', '/index.html', 'lol.fr').route)
        .to.be.equal(router._routes.route1) ;
      expect(router.getRoute('post', '/index.html', 'lol.fr').route)
        .to.be.equal(router._routes.route3) ;
    }) ;

    it('lève une exception si aucune route trouvée', function () {

      router.load({
        route1: {
          method: 'get',
          pattern: '/index.html'
        },
        route2: {
          method: 'get',
          pattern: '/foobar',
          domain: /forum\.domain\..*/
        },
        route3: {
          method: ['get', 'post'],
          pattern: '/index.html'
        }
      }) ;

      expect(router.getRoute.bind(router, 'get', '/inde.html', 'lol.fr'))
        .to.throw(RouteError) ;
      expect(router.getRoute.bind(router, 'get', '/foobar', 'lol.fr'))
        .to.throw(RouteError) ;
    }) ;

    it('retourne un objet avec la route et les vars trouvées', function () {

      router.load({
        route: {
          method: 'get',
          pattern: '/index-{var1}{?var2}.html',
          requirements: {
            var1: '\\w+',
            var2: '-(\\w+)'
          },
          defaults: {
            var1: 'foo',
            var2: 'bar',
            var3: 'lol'
          }
        }
      }) ;

      expect(router.getRoute.bind(router, 'get', '/index.html', 'lol.fr'))
        .to.throw(RouteError) ;
      let match1 = router.getRoute('get', '/index-test-shalom.html', 'lol.fr') ;
      let match2 = router.getRoute('get', '/index-test.html', 'lol.fr') ;

      expect(match1.route).to.be.equal(router._routes.route) ;
      expect(match2.route).to.be.equal(router._routes.route) ;
      expect(match1.vars).to.be.deep.equal({
        var1: 'test',
        var2: 'shalom',
        var3: 'lol'
      }) ;
      expect(match2.vars).to.be.deep.equal({
        var1: 'test',
        var2: 'bar',
        var3: 'lol'
      }) ;
    }) ;

    it('notifie le canal `Router` lorsqu\'une route est trouvée', function () {

      router.load({
        route1: {
          method: 'get',
          pattern: '/index.html'
        }
      }) ;

      router.kernel().events().register('Router') ;
      let spy = sinon.spy() ;
      router.kernel().events().attach('Router', 'spy', spy) ;
      router.getRoute('get', '/index.html', 'lol.fr') ;

      expect(spy.getCall(0).args[0])
        .to.be.equal('Route pour `GET:lol.fr/index.html` trouvée') ;
    }) ;
  }) ;


  describe('unroute', function () {

    it('reconstruit une URL à partir d\'une route', function () {

      router.load({
        route: {
          method: 'get',
          pattern: '/index-{var1}{?var2}.html',
          requirements: {
            var1: '\\w+',
            var2: '-(\\w+)'
          },
          defaults: {
            var1: 'foo',
            var2: 'bar',
            var3: 'lol'
          }
        }
      }) ;

      expect(router.unroute('route', {var1: 'theo', var2: 'net'}))
        .to.be.equal('/index-theo-net.html') ;
    }) ;

    it('Lance une exeption si la route demandée n\'existe pas', function () {

      router.load({
        route: {
          method: 'get',
          pattern: '/index-{var1}{?var2}.html',
          requirements: {
            var1: '\\w+',
            var2: '-(\\w+)'
          }
        }
      }) ;

      expect(router.unroute.bind(router, 'inconnu', {}))
        .to.throw(RouteError) ;
    }) ;

    it('Renvoit une URL absolue si besoin (domaine différent)', function () {

      router.load({
        route1: {
          method: 'get',
          pattern: '/foobar.html',
        },
        route2: {
          method: 'get',
          pattern: '/truc.html',
          domain: 'admin.theo-net.*'
        }
      }) ;

      expect(router.unroute('route2', {})).to.be.equal('/truc.html') ;
      expect(router.unroute('route2', {}, 'admin.theo-net.org'))
        .to.be.equal('/truc.html') ;
      expect(router.unroute('route2', {}, 'www.theo-net.org'))
        .to.be.equal('http://admin.theo-net.org/truc.html') ;
    }) ;

    it('Renvoit une URL (domaine localhost)', function () {

      router.load({
        route1: {
          method: 'get',
          pattern: '/foobar.html',
          domain: 'admin.theo-net.org'
        },
        route2: {
          method: 'get',
          pattern: '/truc.html',
          domain: 'admin.theo-net.*'
        }
      }) ;

      expect(router.unroute('route1', {}, 'localhost'))
        .to.be.equal('http://admin.theo-net.org/foobar.html') ;
      expect(router.unroute('route2', {}, 'localhost'))
        .to.be.equal('http://admin.theo-net/truc.html') ;
    }) ;

    it('http ou https selon la requête', function () {

      router.load({
        route: {
          method: 'get',
          pattern: '/truc.html',
          domain: 'admin.theo-net.*'
        }
      }) ;

      expect(router.unroute('route', {}, 'admin.theo-net.org', true))
        .to.be.equal('/truc.html') ;
      expect(router.unroute('route', {}, 'www.theo-net.org', false))
        .to.be.equal('http://admin.theo-net.org/truc.html') ;
      expect(router.unroute('route', {}, 'www.theo-net.org', true))
        .to.be.equal('https://admin.theo-net.org/truc.html') ;
    }) ;

    it('fonctionne aussi si le port est spécifique', function () {

      router.load({
        route: {
          method: 'get',
          pattern: '/truc.html',
          domain: 'admin.theo-net.*'
        }
      }) ;

      expect(router.unroute('route', {}, 'admin.theo-net.local:8089', false))
        .to.be.equal('/truc.html') ;
      expect(router.unroute('route', {}, 'www.theo-net.local:8089', false))
        .to.be.equal('http://admin.theo-net.local:8089/truc.html') ;
    }) ;
  }) ;


  describe('getFiles', function () {

    it('retourne la liste des fichiers chargés par le router', function () {

      router.load('config/routes.json') ;
      expect(router.getFiles()).to.be.deep.equal([
        {
          file: '../../../doc/demo-server/config/routes.json',
          prefix: '',
          basename: ''
        },
        {
          file: '../../../doc/demo-server/config/routesAdmin.json',
          prefix: '/admin',
          basename: 'admin'
        },
      ]) ;
    }) ;
  }) ;
}) ;

