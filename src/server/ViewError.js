'use strict' ;

/*
 * src/server/Services/ViewError.js
 */

const BaseError = require('../core/Error/BaseError') ;

class ViewError extends BaseError {
}

module.exports = ViewError ;

