'use strict' ;

/**
 * src/server/View.test.js
 */

const expect = require('chai').expect,
      fs     = require('fs') ;

const View      = require('./View'),
      ViewError = require('./ViewError'),
      Router    = require('./Services/Router'),
      User      = require('./User/User') ;

const basepath =  __dirname + '/../../doc/demo-server/' ;

describe('server/View', function () {

  it('charge une vue', function () {

    let view = new View(basepath + 'controller/Index/views/index.html') ;
    expect(view._contentSource).to.be.equal(
      '<h1>Hello World!</h1>\n\n<p>{{message}}</p>\n\n<div>\n  <p>\n  '
      + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
      + 'Il peut également utiliser les données transmises à la vue : '
      + '{{foobar}}\n</p>\n\n\n</div>\n\n'
    ) ;
  }) ;

  it('peut charger une vue vide', function () {

    let view = new View(null) ;
    expect(view._contentSource).to.be.equal('') ;
    expect(view._content()).to.be.equal('') ;
  }) ;

  it('lève une exception si la vue ne peut être chargée', function () {

    let exec = function () {
      new View(basepath + 'controller/Index/views/idex.html') ;
    } ;

    expect(exec.bind({})).to.throw(ViewError, 'Impossible de charger la vue '
      + '/home/gregoire/Théo-Net/KephaJs/src/server/'
      + '../../doc/demo-server/controller/Index/views/idex.html: '
      + 'accès au fichier impossible'
    ) ;
  }) ;

  it('par défaut gère du HTML', function () {

    let view = new View(basepath + 'controller/Index/views/index.html') ;
    expect(view.$getMimeType()).to.be.equal('text/html') ;
  }) ;

  it('on peut préciser le type Mime', function () {

    let view = new View(
      basepath + 'controller/Index/views/index.html', 'json') ;
    expect(view.$getMimeType()).to.be.equal('application/json') ;
  }) ;

  it('on peut changer le type Mime', function () {

    let view = new View(basepath + 'controller/Index/views/index.html') ;
    view.$setMimeType('json') ;
    expect(view.$getMimeType()).to.be.equal('application/json') ;
  }) ;

  it('lève une exception si le type mime n\'est pas supporté', function () {

    let exec = function () {
      new View(basepath + 'controller/Index/views/index.html', 'foobar') ;
    } ;

    expect(exec.bind({})).to.throw(ViewError, 'Impossible de charger la vue '
      + '/home/gregoire/Théo-Net/KephaJs/src/server/'
      + '../../doc/demo-server/controller/Index/views/index.html: '
      + 'MIME type `foobar` non supporté'
    ) ;
  }) ;

  describe('$setContent', function () {

    it('modifie le contenu', function () {

      let view = new View(basepath + 'controller/Index/views/index.html') ;
      view.$setContent('test') ;

      expect(view._contentSource).to.be.equal('test') ;
    }) ;

    it('on peut inclure un fichier', function () {

      let view = new View(basepath + 'controller/Index/views/bloc.html') ;
      let source = view._contentSource ;
      view.$setContent('foo{{$include(\'/bloc.html\')}}bar') ;

      expect(view._contentSource).to.be.equal('foo' + source + 'bar') ;
    }) ;
  }) ;

  describe('$render', function () {

    it('pour une vue sans var, renverra juste le contenu', function (done) {

      let view = new View(basepath + 'controller/Index/views/index.html') ;
      view.$render().then(render => {
        expect(render).to.be.equal(
          '<h1>Hello World!</h1>\n\n<p></p>\n\n<div>\n  <p>\n  '
          + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
          + 'Il peut également utiliser les données transmises à la vue : '
          + '\n</p>\n\n\n</div>\n\n'
        ) ;
        done() ;
      }) ;
    }) ;

    it('sait utiliser les variables', function (done) {

      let view = new View(basepath + 'controller/Index/views/index.html') ;
      view.message = 'Un message' ;
      view.$render().then(render => {
        expect(render).to.be.equal(
          '<h1>Hello World!</h1>\n\n<p>Un message</p>\n\n<div>\n  <p>\n  '
          + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
          + 'Il peut également utiliser les données transmises à la vue : '
          + '\n</p>\n\n\n</div>\n\n'
        ) ;
        done() ;
      }) ;
    }) ;

    it('utilise aussi les var si le contenu a été modifié', function (done) {

      let view = new View(basepath + 'controller/Index/views/index.html') ;
      view.message = 'Un message' ;
      view.$setContent('Il dit « {{message}} »') ;

      view.$render().then(render => {
        expect(render).to.be.equal('Il dit « Un message »') ;
        done() ;
      }) ;
    }) ;

    it('si un layout est défini, renvoi son contenu', function (done) {

      let view = new View(basepath + 'controller/Index/views/index.html') ;
      view.$setLayout(basepath + 'controller/layout.html') ;
      view.title = 'Test' ;
      view.message = 'Message...' ;

      view.$render().then(render => {
        expect(render).to.be.equal(
          '<!DOCTYPE html>\n\n'
          + '<html>\n  <head>\n    <meta charset="utf-8" />\n'
          + '    <title>Test - Démo</title>\n'
          + '    <link rel="stylesheet" href="./styles.css" />\n'
          + '  </head>\n\n  <body>\n    '
          + '<h1>Hello World!</h1>\n\n<p>Message...</p>\n\n<div>\n  <p>\n  '
          + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
          + 'Il peut également utiliser les données transmises à la vue : '
          + '\n</p>\n\n\n</div>\n\n\n  </body>\n</html>\n') ;
        done() ;
      }) ;
    }) ;

    it('accès aux helpers déf dans $config.$view.helpers', function (done) {

      let view = new View(basepath + 'controller/Index/views/index.html') ;
      view.kernel().get('$config').set('$view', {
        helpers: {message: 'Salut', foobar: 'test'}
      }) ;
      view.$render().then(render => {
        expect(render).to.be.equal(
          '<h1>Hello World!</h1>\n\n<p>Salut</p>\n\n<div>\n  <p>\n  '
          + 'Ici, nous avons un bloc inclu dans une autre vue !\n</p>\n<p>\n  '
          + 'Il peut également utiliser les données transmises à la vue : test'
          + '\n</p>\n\n\n</div>\n\n'
        ) ;
        done() ;
      }) ;
    }) ;
  }) ;

  describe('helpers', function () {

    it('$getSlot existe', function (done) {

      let view = new View(basepath + 'controller/Index/views/test.html') ;
      view.$request = {} ;
      view.$response = {} ;
      view.$user = new User() ;
      view.$render().then(render => {
        expect(render).to.be.equal(
          'On inclue Slot\nSlot test\n\n'
        ) ;
        done() ;
      }).catch(e => { console.log(e) ; done() ; }) ;
    }) ;

    it('$getSlot accès à request, response, user', function (done) {

      let view = new View(basepath + 'controller/Index/views/test.html') ;
      view.$request = {} ;
      view.$response = {} ;
      view.$user = new User() ;
      view.$render().then(() => {

        expect(view.kernel().get('$config').get('testRequest'))
          .to.be.equal(view.$request) ;
        expect(view.kernel().get('$config').get('testResponse'))
          .to.be.equal(view.$response) ;
        expect(view.kernel().get('$config').get('testUser'))
          .to.be.equal(view.$user) ;
        done() ;
      }) ;
    }) ;


    describe('$getRoute', function () {

      let view ;

      beforeEach(function () {

        let router = new Router() ;
        router.load({
          test: {
            method: 'get',
            pattern: '/test.html',
            controller: 'ctr',
            action: 'act'
          },
          domain: {
            method: 'get',
            pattern: '/foobar.html',
            domain: 'admin.theo-net.*',
            controller: 'admin',
            action: 'foo'
          },
          complex: {
            method: 'get',
            pattern: '/add-{var1}-{var2}.html',
            requirements: {
              var1: '\\w+',
              var2: '\\w+',
            }
          }
        }) ;
        view = new View(basepath + 'controller/Index/views/test.html') ;
        view.kernel().container().set('$router', router) ;
        view.$request = {headers: {host: 'localhost'}} ;
      }) ;

      it('sans paramètres', function (done) {

        view.$setContent('{{$getRoute(\'test\')}}') ;

        view.$render().then(render => {
          expect(render).to.be.equal('/test.html') ;
          done() ;
        }) ;
      }) ;

      it('avec paramètres', function (done) {

        view.$setContent(
          '{{$getRoute(\'complex\', {var1: \'foo\', var2: \'bar\'})}}'
        ) ;

        view.$render().then(render => {
          expect(render).to.be.equal('/add-foo-bar.html') ;
          done() ;
        }) ;
      }) ;

      it('même domaine', function (done) {

        view.$request.headers.host = 'admin.theo-net.org' ;
        view.$setContent('{{$getRoute(\'domain\')}}') ;
        view.$render().then(render => {
          expect(render).to.be.equal('/foobar.html') ;
          done() ;
        }) ;
      }) ;

      it('url absolue', function (done) {

        view.$request.headers.host = 'www.theo-net.org' ;
        view.$setContent('{{$getRoute(\'domain\')}}') ;

        view.$render().then(render => {
          expect(render)
            .to.be.equal('http://admin.theo-net.org/foobar.html') ;
          done() ;
        }) ;
      }) ;

      it('HTTPS automatique', function (done) {

        view.$request.headers.host = 'www.theo-net.org' ;
        view.kernel().get('$config').set('forceHttps', true, true) ;
        view.$setContent('{{$getRoute(\'domain\')}}') ;

        view.$render().then(render => {
          expect(render)
            .to.be.equal('https://admin.theo-net.org/foobar.html') ;
          done() ;
        }) ;
      }) ;
    }) ;
  }) ;

  describe('$setLayout', function () {

    it('charge le fichier passé en paramère', function () {

      let view = new View(basepath + 'controller/Index/views/index.html') ;
      view.$setLayout(basepath + 'controller/layout.html') ;

      expect(view._layoutSource).to.be.equal(
        fs.readFileSync(basepath + 'controller/layout.html').toString()) ;
    }) ;

    it('lève une exception si le fichier ne peut être chargé', function () {

      let view = new View(basepath + 'controller/Index/views/index.html') ;

      expect(view.$setLayout.bind({}, 'controller/laout.html'))
        .to.throw('Impossible de charger le layout controller/laout.html') ;
    }) ;

    it('false désactive le layout', function () {

      let view = new View(basepath + 'controller/Index/views/index.html') ;
      view.$setLayout(basepath + 'controller/layout.html') ;
      view.$setLayout(false) ;

      expect(view._layout).to.be.false ;
    }) ;

    it('on peut inclure un fichier', function () {

      let view = new View(basepath + 'controller/Index/views/index.html') ;
      view.$setLayout(basepath + 'controller/layout.html') ;
      let source = view._layoutSource ;

      view.$setLayout(basepath + 'controller/layoutBis.html') ;

      expect(view._layoutSource).to.be.equal('foo' + source + 'bar\n') ;
    }) ;
  }) ;
}) ;

