'use strict' ;

/**
 * src/server/Form/HiddenField.js
 */

const InputField = require('./InputField') ;

class HiddenField extends InputField {

  init () {

    super.init() ;
    this._type = 'hidden' ;
    this._required = true ;
  }

  build () {

    return '<input id="' + this._id + '" name="' + this._name
         + '" type="hidden" value="' + this._valueAttr + '">\n' ;
  }
}

module.exports = HiddenField ;

