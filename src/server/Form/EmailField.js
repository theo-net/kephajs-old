'use strict' ;

/**
 * src/server/Form/EmailField.js
 */

const StringField = require('./StringField') ;

class EmailField extends StringField {

  init () {

    super.init() ;

    this._type = 'email' ;
    if (this._placeholder === null)
      this._placeholder = 'exemple@domain.com' ;
    this._validator.add('mail') ;
  }
}

module.exports = EmailField ;

