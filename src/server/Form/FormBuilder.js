'use strict' ;

/**
 * src/server/Form/FormBuilder.js
 */

const Form = require('./Form') ;

/**
 * Permet de gérer la construction d'un formulaire. Avec cette classe, un
 * formulaire peut facilement être réutilisé.
 */
class FormBuilder {

  /**
   * Initialise un nouveau formulaire. Si une entité est transmise, elle sera
   * donnée au formulaire qui pourra ainsi automatiser certaines choses.
   * @param {Entity} [entity=null] Entité à transmettre
   * @param {Forms} forms Service $forms, dispo dans l'objet avec `this.$forms`
   */
  constructor (entity = null, forms) {

    this.$forms = forms ;
    this._form = new Form(entity, forms) ;
  }


  /**
   * Fonction que doit implémenter chaque builder, c'est celle-ci qui construit
   * le formulaire. On peut spécifier un paramètre, `config` qui permettra de
   * paramètrer le formulaire. Celui-ci sera transmis par le service `$forms`.
   */
  build () {}


  /**
   * Retourne le formulaire associé
   * @returns {Form}
   */
  getForm () {

    return this._form ;
  }
}

module.exports = FormBuilder ;

