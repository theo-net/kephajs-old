'use strict' ;

/**
 * src/server/Form/SelectField.test.js
 */

const SelectField = require('./SelectField') ;

const expect = require('chai').expect ;

describe('server/Form/SelectField', function () {

  it('peut voir un attribut size', function () {

    let select = new SelectField({name: 'foo', size: 2}) ;

    expect(select._size).to.be.equal(2) ;
    expect(select._buildAttributes())
      .to.be.equal(' id="foo" name="foo" size="2"') ;
  }) ;

  it('peut voir un attribut multiple', function () {

    let select1 = new SelectField({name: 'foo'}),
        select2 = new SelectField({name: 'foo', multiple: false}),
        select3 = new SelectField({name: 'foo', multiple: true}) ;

    expect(select1._multiple).to.be.equal(false) ;
    expect(select1._buildAttributes())
      .to.be.equal(' id="foo" name="foo"') ;
    expect(select2._multiple).to.be.equal(false) ;
    expect(select2._buildAttributes())
      .to.be.equal(' id="foo" name="foo"') ;
    expect(select3._multiple).to.be.equal(true) ;
    expect(select3._buildAttributes())
      .to.be.equal(' id="foo" name="foo" multiple') ;
  }) ;

  it('liste de valeur', function () {

    let select = new SelectField({
      name: 'foo',
      list: [
        {value: 'foo', label: 'Foo'},
        {value: 'bar', label: 'Bar'}
      ]
    }) ;

    expect(select._buildList(select._list)).to.be.equal(
      '<option value="foo">Foo</option>\n    '
      + '<option value="bar">Bar</option>\n    '
    ) ;
  }) ;

  it('sous-groupe', function () {

    let select = new SelectField({
      name: 'foo',
      list: [
        {value: 'foo', label: 'Foo'},
        {
          group: [
            {value: 'bar', label: 'Bar'}
          ],
          label: 'Group'
        }
      ]
    }) ;

    expect(select._buildList(select._list)).to.be.equal(
      '<option value="foo">Foo</option>\n    '
      + '<optgroup label="Group">\n    '
      + '<option value="bar">Bar</option>\n    '
      + '</optgroup>\n    '
    ) ;
  }) ;

  it('les éléments peuvent être disabled ou selected', function () {

    let select = new SelectField({
      name: 'foo',
      list: [
        {value: 'foo', label: 'Foo', selected: true},
        {value: 'bar', label: 'Bar', disabled: true}
      ]
    }) ;

    expect(select._buildList(select._list)).to.be.equal(
      '<option value="foo" selected>Foo</option>\n    '
      + '<option value="bar" disabled>Bar</option>\n    '
    ) ;
  }) ;

  it('les sous-groupes peuvent être disabled', function () {

    let select = new SelectField({
      name: 'foo',
      list: [
        {value: 'foo', label: 'Foo'},
        {
          group: [
            {value: 'bar', label: 'Bar'}
          ],
          label: 'Group',
          disabled: true
        }
      ]
    }) ;

    expect(select._buildList(select._list)).to.be.equal(
      '<option value="foo">Foo</option>\n    '
      + '<optgroup label="Group" disabled>\n    '
      + '<option value="bar">Bar</option>\n    '
      + '</optgroup>\n    '
    ) ;
  }) ;

  it('construction par défaut', function () {

    let select = new SelectField({
      name: 'foo', id: 'bar',
      label: 'Un truc',
      help: 'Une aide',
      required: true,
      disabled: true,
      size: 20,
      multiple: true,
      list: [{value: 'foo', label: 'bar'}]
    }) ;
    select.addClass({field: 'a', label: 'b', group: 'c', msg: 'd'}) ;

    expect(select.build()).to.be.equal(
      '  <div class="c">\n    '
      + '<label class="b" for="bar">Un truc</label>\n    '
      + '<select class="a" id="bar" name="foo" required disabled'
      + ' multiple size="20">\n    '
      + '<option value="foo">bar</option>\n    '
      + '</select><i></i>\n    '
      + '<p class="d">Une aide</p>\n'
      + '  </div>'
    ) ;
  }) ;


  describe('Gestion de la valeur', function () {

    it('valeur non multiple', function () {

      let select = new SelectField({
        name: 'foo',
        list: [
          {value: 'a', label: 'A'},
          {value: 'b', label: 'B', selected: true},
          {
            group: [
              {value: 'c', label: 'C'},
              {value: 'd', label: 'D'}
            ],
            label: 'Group'
          },
          {value: 'e', label: 'E'}
        ]
      }) ;

      expect(select.getValue()).to.be.equal('b') ;
      select.setValue('c') ;
      expect(select.getValue()).to.be.equal('c') ;
      expect(select._list[1].selected).to.be.false ;
      expect(select._list[2].group[0].selected).to.be.true ;

      select.setValue('e') ;
      expect(select.getValue()).to.be.equal('e') ;
      expect(select._list[2].group[0].selected).to.be.false ;
      expect(select._list[3].selected).to.be.true ;
    }) ;

    it('valeur multiple', function () {

      let select = new SelectField({
        name: 'foo',
        multiple: true,
        list: [
          {value: 'a', label: 'A'},
          {value: 'b', label: 'B', selected: true},
          {
            group: [
              {value: 'c', label: 'C'},
              {value: 'd', label: 'D'}
            ],
            label: 'Group'
          },
          {value: 'e', label: 'E'}
        ]
      }) ;

      expect(select.getValue()).to.be.deep.equal(['b']) ;
      select.setValue(['c']) ;
      expect(select.getValue()).to.be.deep.equal(['c']) ;
      expect(select._list[1].selected).to.be.false ;
      expect(select._list[2].group[0].selected).to.be.true ;

      select.setValue(['c', 'd']) ;
      expect(select.getValue()).to.be.deep.equal(['c', 'd']) ;
      expect(select._list[2].group[0].selected).to.be.true ;
      expect(select._list[2].group[1].selected).to.be.true ;

      select.setValue(['d', 'e']) ;
      expect(select.getValue()).to.be.deep.equal(['d', 'e']) ;
      expect(select._list[2].group[0].selected).to.be.false ;
      expect(select._list[2].group[1].selected).to.be.true ;
      expect(select._list[3].selected).to.be.true ;
    }) ;
  }) ;
}) ;

