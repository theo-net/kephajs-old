'use strict' ;

/**
 * src/server/Form/FormBuilder.test.js
 */

const expect = require('chai').expect ;

const FormBuilder = require('./FormBuilder'),
      Forms       = require('../Services/Forms'),
      ValidateS   = require('../../core/Services/Validate'),
      MessageFormBuilder =
        require('../../../doc/demo-server/form/MessageFormBuilder') ;

describe('server/Form/FormBuilder', function () {

  it('Créé un nouveau formulaire', function () {

    let formBuilder1 = new FormBuilder() ;
    let formBuilder2 = new FormBuilder() ;
    formBuilder1.build() ;
    formBuilder2.build() ;

    expect(formBuilder1.getForm()).to.be.not.equal(formBuilder2.getForm()) ;
  }) ;

  it('Transmet une entité', function () {

    let entity = {} ;
    let formBuilder = new FormBuilder(entity) ;
    formBuilder.build() ;

    expect(formBuilder.getForm().getEntity()).to.be.equal(entity) ;
  }) ;

  it('On peut lui transmettre le service $forms', function () {

    let forms = {} ;
    let formBuilder = new FormBuilder(null, forms) ;
    formBuilder.build() ;

    expect(formBuilder.$forms).to.be.equal(forms) ;
  }) ;

  it('Exécute build lors de l\'initialisation', function () {

    let test = new MessageFormBuilder(null, new Forms(new ValidateS())) ;
    test.build() ;

    expect(test.a).to.be.equal('ok') ;
  }) ;
}) ;

