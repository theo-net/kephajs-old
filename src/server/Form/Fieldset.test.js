'use strict' ;

/**
 * src/server/Form/Fieldset.test.js
 */

const expect = require('chai').expect ;

const Block       = require('./Block'),
      Fieldset    = require('./Fieldset'),
      Forms       = require('../Services/Forms'),
      ValidateS   = require('../../core/Services/Validate') ;

describe('server/Form/Fieldset', function () {

  it('étend Block', function () {

    let fieldset = new Fieldset() ;
    expect(fieldset).to.be.instanceOf(Block) ;
  }) ;

  it('setLegend() définie la légende associée au fieldset', function () {

    let fieldset = new Fieldset() ;
    expect(fieldset.getLegend()).to.be.equal(null) ;
    expect(fieldset.setLegend('Une légende')).to.be.equal(fieldset) ;
    expect(fieldset.getLegend()).to.be.equal('Une légende') ;
  }) ;

  it('class CSS spécifiques au fieldset', function () {

    let fieldset = new Fieldset() ;
    expect(fieldset.getClass()).to.be.equal('') ;
    fieldset.addClass('foobar') ;
    expect(fieldset.getClass()).to.be.equal('foobar') ;
    fieldset.addClass(['foo', 'bar']) ;
    expect(fieldset.getClass()).to.be.equal('foobar foo bar') ;
    fieldset.addClass({fieldset: 'test'}) ;
    expect(fieldset.getClass()).to.be.equal('test foobar foo bar') ;
  }) ;


  describe('build', function () {

    let fieldset ;

    beforeEach(function () {

      fieldset = new Fieldset(null, new Forms(new ValidateS())) ;
    }) ;

    it('retourne le code HTML du fieldset', function () {

      expect(fieldset.build())
        .to.be.equal('<fieldset>\n</fieldset>') ;
      fieldset.setId('salut') ;
      expect(fieldset.build())
        .to.be.equal('<fieldset id="salut">\n</fieldset>') ;
      fieldset.setLegend('Foobar') ;
      expect(fieldset.build()).to.be.equal(
        '<fieldset id="salut">\n  <legend>Foobar</legend>\n</fieldset>'
      ) ;
      fieldset.addClass('foobar') ;
      expect(fieldset.build()).to.be.equal(
        '<fieldset id="salut" class="foobar">\n  '
        + '<legend>Foobar</legend>\n</fieldset>'
      ) ;
    }) ;
  }) ;
}) ;

