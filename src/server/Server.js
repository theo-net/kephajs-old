'use strict' ;

/**
 * src/server/Server.js
 */

const http        = require('http'),
      path        = require('path'),
      querystring = require('querystring') ;

const Cli           = require('../cli/Cli'),
      Router        = require('./Services/Router'),
      Models        = require('./Services/Models'),
      Slot          = require('./Slot'),
      Resp          = require('./Resp'),
      RouteError    = require('./Services/RouteError'),
      Sessions      = require('./User/Sessions'),
      MemoryStorage = require('./User/MemoryStorage'),
      Kfmd          = require('./Services/Kfmd'),
      Forms         = require('./Services/Forms'),
      Config        = require('../core/Services/Config') ;

const DEFAULT_PORT = 8089,
      POST_BODY_MAX_SIZE = 1e6,
      HTTP_PAYLOAD_TOO_LARGE = 413 ;

/**
 * Point d'entrée de notre application. KephaJs est disponible partout à travers
 * l'instance du serveur. Vous pourrez y accéder directement avec kephajs
 */
class Server extends Cli {

  /**
   * Initialise le serveur et le démarre.
   * @param {Object} config Configuration du serveur
   */
  constructor (config = {}) {

    super() ;

    this.init = () => {} ;

    const $config = this.get('$config') ;

    // On expose notre serveur qui sera disponible dans toute l'application
    global['kephajs'] = this ;

    // On intialise les canaux d'évènements
    this.kernel().events().register('Router') ;
    this.kernel().events().register('Slot') ;

    // On initialise les logs
    this.kernel().events().attach('Slot', 'log', data => {
      this.kernel().get('$logs').log(data, 'debug') ;
    }) ;

    // On initialise quelques services
    this.kernel().container().set('$models', new Models()) ;
    this.kernel().container()
      .set('$sessions', new Sessions(new MemoryStorage())) ;
    this.kernel().container().set('$kfmd', new Kfmd()) ;
    this.kernel().container()
      .set('$forms', new Forms(this.kernel().get('$validate'))) ;
    this.kernel().container().set('$staticCache', new Config()) ;


    // Commande principale
    this.command('start', 'Starts the server')
    .option('-c, --config', 'Fichier de configuration', 'string')
    .option('-p, --port', 'Port of server', {integer: {min: 0}}, DEFAULT_PORT)
    .action((args, options) => {

      // On charge la configuration
      $config.add(config) ;
      let basepath = config.basepath ? config.basepath : process.cwd() ;
      if (options.config) {
        basepath = process.cwd() + path.sep ;
        $config.add(require(basepath + options.config)) ;
      }
      basepath = path.normalize(basepath) + path.sep ;
      $config.set('basepath', basepath) ;

      // On initialise le routing
      this.kernel().container().set('$router', new Router(basepath)) ;
      if ($config.get('routes'))
        this.kernel().get('$router').load($config.get('routes')) ;

      // On lance l'init
      this.init($config, this.kernel()) ;

      // On démarre le server et on sauve son instance
      $config.set('port', options.port) ;
      $config.set('$server', this.startServer(options.port)) ;
    }) ;
  }


  /**
   * Démarre un serveur
   * @param {Integer} port Port sur lequel écoute le serveur
   * @returns {http.Server} Serveur créé
   */
  startServer (port = DEFAULT_PORT) {

    let logLevel = this.get('$config').get('logLevel') ;


    if (logLevel != 'warn') {

      console.log(this.color().red('Démarage du serveur sur le port ')
                + port + this.color().red('...')) ;
    }

    const server = http.createServer(this._requestListener.bind(this)) ;
    server.on('clientError', (err, socket) => {
      socket.end('HTTP/1.1 400 Bad Request\r\n\r\n') ;
    }) ;
    server.listen(port) ;

    return server ;
  }


  /**
   * Alias vers this.kernel().get()
   * @param {String} service Nom du service
   * @returns {Object}
   */
  get (service) {

    return this.kernel().get(service) ;
  }


  /**
   * Définis les modèles disponibles dans l'application.
   * @param {Object} models Modèles : nom => instance
   */
  setModels (models) {

    this.K.forEach(models, (model, name) => {
      this.get('$models').register(name, model) ;
    }) ;
  }


  /**
   * Définis les formsBuilders dispobibles dans l'application.
   * @param {Object} formBuilders formBuilders : nom => class
   */
  setFormBuilders (formBuilders) {

    this.K.forEach(formBuilders, (formBuilder, id) => {
      this.get('$forms').register(id, formBuilder) ;
    }) ;
  }


  /**
   * Exécute la requête reçue.
   * @param {http.IncomingMessage} request Requête reçue
   * @param {http.ServerResponse} res Réponse du serveur
   */
  _requestListener (request, res) {

    if (this.kernel().get('$config').get('logLevel') == 'debug') {
      console.log(this.color().green('Requête reçue ') + request.method + ' '
                   + 'http://' + request.headers.host + request.url) ;
    }

    // On passe par notre propre objet réponse
    let response = new Resp(res, request) ;

    // Dispatcher : traite la requête et envoit le bon slot
    try {
      let route = this.kernel().get('$router').getRoute(
        request.method, request.url, request.headers.host
      ) ;
      let slot = new Slot(
        route.route._callable[0], route.route._callable[1],
        route.vars, request, response
      ) ;

      // On traite les data POST
      if (request.method == 'POST') {

        let body = '' ;
        request.on('data', data => {

          body += data ;

          if (body.length > POST_BODY_MAX_SIZE) {
            body = '' ;
            res.writeHead(
              HTTP_PAYLOAD_TOO_LARGE, {'Content-Type': 'text/plain'}
            ).end() ;
            request.connection.destroy() ;
          }
        }) ;

        request.on('end', () => {

          slot.execute(querystring.parse(body))
            .then(() => { response.send(slot.getView()) ; })
            .catch(() => { response.send('') ; }) ;
        }) ;
      }
      else {
        slot.execute()
          .then(() => { response.send(slot.getView()) ; })
          .catch(() => { response.send('') ; }) ;
      }
    }
    catch (e) {

      if (this.kernel().get('$config').get('logLevel') == 'debug')
        console.error(e) ;

      if (e instanceof RouteError)
        response.sendError404() ;
      else
        response.sendError500() ;
    }
  }


  /**
   * Capture les signaux pour quitter correctement l'application
   * @param {Integer} signal Signal capturé
   */
  _handleExit (signal) {

    console.log('\nReceived ' + signal + '. Close the server...') ;

    let server =  this.get('$config').get('$server') ;
    if (server.close) {
      server.close(() => {
        console.log('...server closed') ;
        process.exit(0) ;
      }) ;
    }
    else {
      console.log('...nothing to close') ;
      process.exit(0) ;
    }
  }
}


module.exports = Server ;

