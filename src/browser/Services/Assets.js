'use strict' ;

/**
 * src/browser/Services/Assets.js
 */

class Assets {

  constructor () {

    this._loadedCss = [] ;
    this._loadedScript = [] ;
  }


  /**
   * Charge une feuille de style CSS (un couple url:media) ne sera chargé qu'une
   * seule fois.
   *
   * @param {String} url Url de la feuille de style
   * @param {String} [media] Attribut `media`
   */
  loadCss (url, media) {

    if (this._loadedCss.indexOf(url + ':' + media)) {

      let link = document.createElement('link') ;

      if (media !== undefined) link.media = media ;
      link.href = url ;
      link.rel = 'stylesheet' ;

      document.getElementsByTagName('head')[0].appendChild(link) ;

      this._loadedCss.push(url + ':' + media) ;
    }
  }


  /**
   * Charge un script s'il n'a pas déjà été chargé
   *
   * @param {String} url Url du script à chargé
   * @param {Function} [callback] Fonction appelée après le chargement du script
   */
  loadScript (url, callback) {

    if (this._loadedScript.indexOf(url)) {

      let script = document.createElement('script') ;
      script.src = url ;

      let done = false ;
      script.onload = script.onreadystatechange = function () {

        if (!done &&
          (!this.readyState || this.readyState === 'loaded' ||
           this.readyState === 'complete')) {

          done = true ;

          if (typeof callback == 'function')
            callback() ;
        }
      } ;

      document.getElementsByTagName('head')[0].appendChild(script) ;

      this._loadedScript.push(url) ;
    }
  }
}

module.exports = Assets ;

