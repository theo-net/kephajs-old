'use strict' ;

/**
 * src/browser/Services/Controller.test.js
 */

const expect = require('chai').expect ;

const Controller = require('./Controller') ;

let ctr ;

// buddy ignore:start
describe('Controller', function () {

  beforeEach(function () {

    ctr = new Controller() ;
  }) ;


  /**
   * Controller Instantiation
   */

  it('instantiates controller functions', function () {

    function MyController () {
      this.invoked = true ;
    }

    let controller = ctr.get(MyController) ;

    expect(controller).to.be.not.undefined ;
    expect(controller).to.be.instanceOf(MyController) ;
    expect(controller.invoked).to.be.equal(true) ;
  }) ;

  it('injects a service to controller functions', function () {

    let app = window.K.app() ;
    app.kernel().container().set('aDep', 2017) ;

    function MyController (aDep) {
      this.aDep = aDep ;
    }

    let controller = ctr.get(MyController) ;

    expect(controller.aDep).to.be.equal(2017) ;
  }) ;

  it('allows injecting locals to controller functions', function () {

    function MyController (aDep) {
      this.aDep = aDep ;
    }

    let controller = ctr.get(MyController, {aDep: 42}) ;

    expect(controller.aDep).to.be.equal(42) ;
  }) ;


  /**
   * Controller Registration
   */

  it('allows registering controllers at config time', function () {

    function MyController () {}
    ctr.register('MyController', MyController) ;

    let controller = ctr.get('MyController') ;
    expect(controller).to.be.not.undefined ;
    expect(controller).to.be.instanceOf(MyController) ;
  }) ;

  it('allows registering several controllers in an object', function () {

    function MyController () {}
    function MyOtherController () {}
    ctr.register({
      MyController: MyController,
      MyOtherController: MyOtherController
    }) ;

    let controller      = ctr.get('MyController') ;
    let otherController = ctr.get('MyOtherController') ;

    expect(controller).to.be.instanceOf(MyController) ;
    expect(otherController).to.be.instanceOf(MyOtherController) ;
  }) ;

  it('does not normally look controllers up from window', function () {

    window.MyController = function MyController () {} ;
    expect(function () {
      ctr('MyController') ;
    }).to.throw() ;
  }) ;

  it('looks up controllers up from window when so configured', function () {

    window.MyController = function MyController () {} ;
    ctr.allowGlobals() ;

    let controller = ctr.get('MyController') ;

    expect(controller).to.be.not.undefined ;
    expect(controller).to.be.instanceOf(window.MyController) ;
  }) ;

  it('can return a semi-constructed controller', function () {

    function MyController () {
      this.constructed = true ;
      this.myAttrWhenConstructed = this.myAttr ;
    }

    let controller = ctr.get(MyController, null, true) ;

    expect(controller.constructed).to.be.undefined ;
    expect(controller.instance).to.be.not.undefined ;

    controller.instance.myAttr = 2017 ;
    let actualController = controller() ;

    expect(actualController.constructed).to.be.not.undefined ;
    expect(actualController.myAttrWhenConstructed).to.be.equal(2017) ;
  }) ;

  it('can bind semi-constructed controller to scope', function () {

    function MyController () {}
    let scope = {} ;

    let controller = ctr.get(MyController, {$scope: scope}, true, 'myCtrl') ;

    expect(scope.myCtrl).to.be.equal(controller.instance) ;
  }) ;
}) ;
// buddy ignore:end

