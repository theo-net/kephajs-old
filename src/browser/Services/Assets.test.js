'use strict' ;

/**
 * src/browser/Services/Assets.test.js
 */

const expect = require('chai').expect ;

const Assets = require('./Assets') ;

// buddy ignore:start
describe.skip('Assets', function () {

  let assets, element ;

  beforeEach(function () {

    assets = new Assets() ;
    document.open() ;
    document.write('<html><head></head><body></body></html>') ;

    window.onbeforeunload = () => 'hello' ;
  }) ;

  afterEach(function () {

    document.close() ;
  }) ;


  describe('loadCss', function () {

    it('charge un document CSS à partir d\'une URL', function () {

      assets.loadCss('http://uneUrl', 'unMedia') ;

      element = document.getElementsByTagName('link')[0] ;

      expect(element.getAttribute('rel')).to.be.equal('stylesheet') ;
      expect(element.getAttribute('href')).to.be.equal('http://uneUrl') ;
      expect(element.getAttribute('media')).to.be.equal('unMedia') ;
    }) ;

    it('ne définit pas l\'attribut media si non donné', function () {

      assets.loadCss('http://uneAutreUrl') ;

      element = document.getElementsByTagName('link')[0] ;

      expect(element.getAttribute('rel')).to.be.equal('stylesheet') ;
      expect(element.getAttribute('href')).to.be.equal('http://uneAutreUrl') ;
      expect(element.getAttribute('media')).to.be.equal(null) ;
    }) ;

    it('ne charge qu\'une fois un document ayant la même URL et le même media',
    function () {

      assets.loadCss('http://uneUrl', 'unMedia') ;
      assets.loadCss('http://uneUrl', 'unMedia') ;
      assets.loadCss('http://uneAutreUrl', 'unMedia') ;
      assets.loadCss('http://uneUrl', 'unAutreMedia') ;

      expect(document.getElementsByTagName('link').length).to.be.equal(3) ;

      element = document.getElementsByTagName('link')[0] ;
      expect(element.getAttribute('href')).to.be.equal('http://uneUrl') ;
      expect(element.getAttribute('media')).to.be.equal('unMedia') ;
      element = document.getElementsByTagName('link')[1] ;
      expect(element.getAttribute('href')).to.be.equal('http://uneAutreUrl') ;
      expect(element.getAttribute('media')).to.be.equal('unMedia') ;
      element = document.getElementsByTagName('link')[2] ;
      expect(element.getAttribute('href')).to.be.equal('http://uneUrl') ;
      expect(element.getAttribute('media')).to.be.equal('unAutreMedia') ;
    }) ;
  }) ;


  describe('loadScript', function () {

    it('charge un script à partir d\'une URL', function () {

      assets.loadScript('http://uneUrl') ;

      element = document.getElementsByTagName('script')[0] ;

      expect(element.getAttribute('src')).to.be.equal('http://uneUrl') ;
    }) ;

    it('ne charge qu\'une fois un script et seul le premier callback sera '
      + 'exécuté', function () {

      assets.loadScript('http://uneUrl') ;
      assets.loadScript('http://uneUrl') ;
      assets.loadScript('http://uneAutreUrl') ;

      expect(document.getElementsByTagName('script').length).to.be.equal(2) ;

      element = document.getElementsByTagName('script')[0] ;
      expect(element.getAttribute('src')).to.be.equal('http://uneUrl') ;
      element = document.getElementsByTagName('script')[1] ;
      expect(element.getAttribute('src')).to.be.equal('http://uneAutreUrl') ;
    }) ;
  }) ;
}) ;
// buddy ignore:end

