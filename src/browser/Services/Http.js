'use strict' ;

/**
 * src/browser/Services/Http.js
 */

const Utils = require('../Utils') ;

const HTTP_CODE_OK    = 200,
      HTTP_CODE_ERROR = 300 ;

/**
 * Service permettant d'envoyer des requêtes HTTP (Ajax)
 */
class Http {

  /**
   * Initialise le service
   *
   * Certaines propriétés sont publiques et permettent de configurer le service
   *
   *   - `http.defaults` Pour les valeurs par défaut
   *      les valeurs des différents headers peuvent être une fonction qui sera
   *      appellée avec la requête comme argument.
   *   - `http.pendingRequests` requêtes en cours
   *
   *  Par défaut, les requêtes sont sérialisées et déssérialisées depuis et vers
   *  du JSON (si le content-type est `application/json` pour les réponses, ou
   *  si le contenu ressemble à du JSON).
   */
  constructor () {

    this.defaults = {
      headers: {
        common: {
          Accept: 'application/json, text/plain, */*'
        },
        post: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        put: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        patch: {
          'Content-Type': 'application/json;charset=utf-8'
        }
      },
      transformRequest: [function (data) {
        if (Utils.isObject(data) && !Utils.isBlob(data) &&
           !Utils.isFile(data) && !Utils.isFormData(data))
          return JSON.stringify(data) ;
        else
          return data ;
      }],
      transformResponse: [Http.defaultHttpResponseTransform],
      paramSerializer: this.serializeParams
    } ;

    this.pendingRequests = [] ;
  }


  /**
   * Envoie une requête
   *
   * Propriétés possibles :
   *   - `method`
   *   - `url`
   *   - `params` Objet clé => valeur permettant d'ajouter des params à l'URL
   *      http://...../..?clé=valeur
   *   - `data`
   *   - `headers`
   *     - `Content-type`
   *     - ...
   *   - `withCredential` active xhr withCredential
   *   - `transformRequest` Fonction transformant les `data`, peut-être
   *      également un tableau de fonctions.
   *      function (data, headers, status)
   *        - `data` données à transformer
   *        - `headers` getter disponible dans la réponse
   *   - `transformResponse` Transforme les réponses des requêtes
   *      function (data, headers, status)
   *   - `timeout` s'il s'agit d'une Promise, interrompt la requête si celle-ci
   *     est résolue entre temps. Sinon, c'est après la durée indiquée
   * @param {Object} requestConfig configuration de la requête
   * @returns {Promise}
   */
  request (requestConfig) {

    // On récupère les valeurs par défaut
    let config = Utils.extend({
      method: 'GET',
      transformRequest:  this.defaults.transformRequest,
      transformResponse: this.defaults.transformResponse,
      paramSerializer:   this.defaults.paramSerializer
    }, requestConfig) ;
    config.headers = this._mergeHeaders(requestConfig) ;

    // withCredentials ?
    if (Utils.isUndefined(config.withCredentials) &&
       !Utils.isUndefined(this.defaults.withCredentials))
      config.withCredentials = this.defaults.withCredentials ;

    // Transformation de la requête
    let reqData = Http._transformData(
      config.data,
      this._headersGetter(config.headers),
      undefined,
      config.transformRequest
    ) ;

    // Pas de Content-Type si aucune données envoyées
    if (Utils.isUndefined(reqData)) {
      Utils.forEach(config.headers, (v, k) => {
        if (k.toLowerCase() === 'content-type')
          delete config.headers[k] ;
      }) ;
    }

    // Construction de l'URL
    let url = this._buildUrl(config.url,
                             config.paramSerializer(config.params)) ;

    let promise = new Promise((resolve, reject) => {

      this.pendingRequests.push(config) ;

      this._httpBackend(
        config.method, url, reqData, config, resolve, reject,
        config.headers, config.timeout, config.withCredentials
      ) ;
    }) ;

    promise = promise.then(this._transformResponse, this._transformResponse) ;
    promise.then(() => {
      Utils.remove(this.pendingRequests, config) ;
    }, () => {
      Utils.remove(this.pendingRequests, config) ;
    }) ;

    promise.success = fn => {
      promise.then(response => {
        fn(response.data, response.status, response.headers, config) ;
      }) ;
      return promise ;
    } ;

    promise.error = fn => {
      promise.catch(response => {
        fn(response.data, response.status, response.headers, config) ;
      }) ;
      return promise ;
    } ;

    return promise ;
  }


  /**
   * Requête GET
   * @param {String} url URL
   * @param {Object} config configuration de la requête
   * @returns {Promise}
   */
  get (url, config) {

    return this.request(Utils.extend(config || {}, {
      method: 'GET',
      url: url
    })) ;
  }


  /**
   * Requête HEAD
   * @param {String} url URL
   * @param {Object} config configuration de la requête
   * @returns {Promise}
   */
  head (url, config) {

    return this.request(Utils.extend(config || {}, {
      method: 'HEAD',
      url: url
    })) ;
  }


  /**
   * Requête DELETE
   * @param {String} url URL
   * @param {Object} config configuration de la requête
   * @returns {Promise}
   */
  delete (url, config) {

    return this.request(Utils.extend(config || {}, {
      method: 'DELETE',
      url: url
    })) ;
  }


  /**
   * Requête POST
   * @param {String} url URL
   * @param {*} data Données de la requête
   * @param {Object} config configuration de la requête
   * @returns {Promise}
   */
  post (url, data, config) {

    return this.request(Utils.extend(config || {}, {
      method: 'POST',
      url: url,
      data: data
    })) ;
  }


  /**
   * Requête PUT
   * @param {String} url URL
   * @param {*} data Données de la requête
   * @param {Object} config configuration de la requête
   * @returns {Promise}
   */
  put (url, data, config) {

    return this.request(Utils.extend(config || {}, {
      method: 'PUT',
      url: url,
      data: data
    })) ;
  }


  /**
   * Requête PATCH
   * @param {String} url URL
   * @param {*} data Données de la requête
   * @param {Object} config configuration de la requête
   * @returns {Promise}
   */
  patch (url, data, config) {

    return this.request(Utils.extend(config || {}, {
      method: 'PATCH',
      url: url,
      data: data
    })) ;
  }


  /**
   * Sérialise une série de paramètres
   * @param {Object} params Paramètres à sérialiser
   * @returns {String}
   */
  serializeParams (params) {

    let parts = [] ;
    Utils.forEach(params, (value, key) => {

      if (Utils.isNull(value) || Utils.isUndefined(value))
        return ;

      if (!Array.isArray(value))
        value = [value] ;

      value.forEach(v => {
        if (Utils.isObject(v))
          v = JSON.stringify(v) ;
        parts.push(encodeURIComponent(key) + '=' + encodeURIComponent(v)) ;
      }) ;
    }) ;
    return parts.join('&') ;
  }


  /**
   * Sérialise une série de paramètres
   * @param {Object} params Paramètres à sérialiser
   * @returns {String}
   */
  paramSerializerJQLike (params) {

    let parts = [] ;

    function serialize (value, prefix, topLevel) {

      if (Utils.isNull(value) || Utils.isUndefined(value))
        return ;

      if(Array.isArray(value)) {
        value.forEach((v, i) => {
          serialize(v, prefix
                       + '['
                       + (Utils.isObject(v) ? i : '')
                       + ']') ;
        }) ;
      } else if (Utils.isObject(value) && !Utils.isDate(value)) {
        Utils.forEach(value, (v, k) => {
          serialize(v, prefix
                       + (topLevel ? '' : '[')
                       + k
                       + (topLevel ? '' : ']')
          ) ;
        }) ;
      }
      else {
        parts.push(
          encodeURIComponent(prefix) + '=' + encodeURIComponent(value)) ;
      }
    }

    serialize(params,'', true) ;

    return parts.join('&') ;
  }


  /**
   * Indique si la requête s'est exécutée ou pas avec succès
   * @param {Integer} status Code du status (200, 401, ...)
   * @returns {Boolean}
   * @static
   */
  static isSuccess (status) {

    return status >= HTTP_CODE_OK && status < HTTP_CODE_ERROR ;
  }


  /**
   * Méthode par défaut pour transformer les réponses : dessérialise le contenu
   * s'il s'agit de JSON
   * @param {String} data Données de la réponse
   * @param {Function} headers Accès au headers
   * @returns {*}
   * @static
   */
  static defaultHttpResponseTransform (data, headers) {

    if (Utils.isString(data)) {

      let contentType = headers('Content-Type') ;
      if (contentType && contentType.indexOf('application/json') === 0 ||
          Utils.isJsonLike(data))
        return JSON.parse(data) ;
    }

    return data ;
  }


  /**
   * Backend exécutant les requêtes
   * La requête retournera un objet, disponible dans le premier `then` :
   *   - `status` code status de la requête (200, 301, ...)
   *   - `data` donnée reçues
   *   - `statusText` message associé au status
   *   - `headers` fonction permettant d'accéder aux headers de la réponse,
   *               prend un paramètre `String` : le header demandé
   *               (`Content-Type`, ...)
   *   - `config` configuration intiial de la requête
   * @param {String} method Méthode (GET, POST, ...)
   * @param {String} url URL de la requête
   * @param {*} post Données de la requêtes
   * @param {Object} config Configuration de la requête
   * @param {Function} resolve Résolution de la Promise
   * @param {Function} reject Rejet de la Promise
   * @param {Object} headers Headers de la requête
   * @param {Promise} timeout Timeout
   * @param {Boolean} withCredentials Autorise CORS
   * @private
   */
  _httpBackend (method, url, post, config, resolve, reject,
    headers, timeout, withCredentials) {

    let xhr = new XMLHttpRequest(),
        timeoutId ;

    xhr.open(method, url, true) ;

    Utils.forEach(headers, (value, key) => {
      xhr.setRequestHeader(key, value) ;
    }) ;
    if (withCredentials)
      xhr.withCredentials = true ;

    xhr.send(post || null) ;

    xhr.onload = () => {

      if (!Utils.isUndefined(timeoutId))
        clearTimeout(timeoutId) ;

      let response = ('response' in xhr) ? xhr.response : xhr.responseText ;
      let statusText = xhr.statusText || '' ;

      let fn = Http.isSuccess(xhr.status) ? resolve : reject ;
      fn({
        status: xhr.status,
        data: response,
        statusText: statusText,
        headers: this._headersGetter(xhr.getAllResponseHeaders()),
        config: config
      }) ;
    } ;
    xhr.onerror = () => {

      if (!Utils.isUndefined(timeoutId))
        clearTimeout(timeoutId) ;

      reject({
        status: 0,
        data: null,
        statusText: '',
        headers: this._headersGetter(''),
        config: config
      }) ;
    } ;

    if (timeout && timeout.then) {
      timeout.then(() => {
        xhr.abort() ;
      }) ;
    } else if (timeout > 0) {
      timeoutId = setTimeout(() => {
        xhr.abort() ;
      }, timeout) ;
    }
  }


  /**
   * Fusionne les headers de la requête avec ceux par défaut
   * @param {Object} config Configuration de la requête
   * @returns {Object} Headers fusionnés
   * @private
   */
  _mergeHeaders (config) {

    let reqHeaders = Utils.extend(
      {},
      config.headers
    ) ;
    let defHeaders = Utils.extend(
      {},
      this.defaults.headers.common,
      this.defaults.headers[(config.method || 'get').toLowerCase()]
    ) ;

    Utils.forEach(defHeaders, (value, key) => {
      let headerExists = Utils.some(reqHeaders, (v, k) => {
        return k.toLowerCase() === key.toLowerCase() ;
      }) ;
      if (!headerExists)
        reqHeaders[key] = value ;
    }) ;

    return this._executeHeadersFns(reqHeaders, config) ;
  }


  /**
   * Si la valeur d'un header est une fonction, alors on exécute celle-ci
   * @param {Object} headers Les headers de la requête
   * @param {Object} config La configuration de la requête
   * @returns {Object} Headers transformés (la fonction est appliquée)
   * @private
   */
  _executeHeadersFns (headers, config) {

    return Utils.transform(headers, (result, v, k) => {
      if (Utils.isFunction(v)) {
        v = v(config) ;
        if (Utils.isNull(v) || Utils.isUndefined(v))
          delete result[k] ;
        else
          result[k] = v ;
      }
    }, headers) ;
  }


  /**
   * À partir d'une chaîne de carctères représentant les headers, créé une
   * fonction permettant d'accéder à chacun de ceux-ci
   * @param {String} headers Headers de la réponse
   * @returns {Function}
   * @private
   */
  _headersGetter (headers) {

    let headersObj ;

    return (name) => {

      headersObj = headersObj || this._parseHeaders(headers) ;

      if (name)
        return headersObj[name.toLowerCase()] ;
      else
        return headersObj ;
    } ;
  }


  /**
   * Parse une chaîne de caractères représentant des headers pour retourner
   * un objet représentant ceux-ci.
   * @param {String|Object} headers Headers de la réponse
   * @returns {Object}
   * @private
   */
  _parseHeaders (headers) {

    if (Utils.isObject(headers)) {

      return Utils.transform(headers, (result, v, k) => {
        result[k.toLowerCase().trim()] = v.trim() ;
      }, {}) ;
    }
    else {
      let lines = headers.split('\n') ;

      return Utils.transform(lines, (result, line) => {

        let separatorAt = line.indexOf(':') ;
        let name  = line.substr(0, separatorAt).trim().toLowerCase(),
            value = line.substr(separatorAt + 1).trim() ;

        if (name)
          result[name] = value ;
      }, {}) ;
    }
  }


  /**
   * Transforme des données avec une fonction, retourne les données si ce que
   * l'on transmet n'est pas une fonction
   * @param {*} data Données
   * @param {Function} headers Headers getter
   * @param {Integer} status Status de la réponse
   * @param {Function} transform Fonction
   * @returns {*}
   * @private
   * @static
   */
  static _transformData (data, headers, status, transform) {

    if (Utils.isFunction(transform))
      return transform(data, headers, status) ;
    else if (Array.isArray(transform)) {
      return transform.reduce((data, fn) => {
        return fn(data, headers, status) ;
      }, data) ;
    }
    else
      return data ;
  }


  /**
   * Transforme la réponse d'une requête
   * @param {Object} response Réponse de la requête
   * @returns {Object}
   * @private
   */
  _transformResponse (response) {

    if (response.data) {
      response.data = Http._transformData(response.data, response.headers,
        response.status, response.config.transformResponse) ;
    }

    if (Http.isSuccess(response.status))
      return response ;
    else
      return Promise.reject(response) ;
  }


  /**
   * Ajoute des paramètres sérialisé à une URL
   * @param {String} url URL
   * @param {String} serializedParams Paramètres sérialisés
   * @returns {String}
   * @private
   */
  _buildUrl (url, serializedParams) {

    if (serializedParams.length) {
      url += (url.indexOf('?') === -1) ? '?' : '&' ;
      url += serializedParams ;
    }

    return url ;
  }
}

module.exports = Http ;

