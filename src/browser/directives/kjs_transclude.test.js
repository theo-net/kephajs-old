'use strict' ;

/**
 * test/directives/kjs_transclude_spec.js
 */

const expect = require('chai').expect ;

const Application = require('../Application'),
      Utils       = require('../Utils') ;

const app = new Application() ;

describe('kjsTransclude', function () {

  let compile, rootScope ;

  beforeEach(function () {

    compile   = app.kernel().get('$compile') ;
    rootScope = app.kernel().get('$rootScope') ;
  }) ;


  it('transludes the parent directive transclusion', function () {

    compile.directive('myTranscluder', function () {
      return {
        transclude: true,
        template: '<div kjs-transclude></div>'
      } ;
    }) ;

    let el = Utils.str2DomElement('<div my-transcluder>Hello</div>') ;
    compile.run(el)(rootScope) ;
    expect(el[0].querySelector('[kjs-transclude]').innerHTML)
      .to.be.equal('Hello') ;
  }) ;

  it.skip('empties existing contents', function () {

    compile.directive('myTranscluder', function () {
      return {
        transclude: true,
        template: '<div kjs-transclude>Existing contents</div>'
      } ;
    }) ;

    let el = Utils.str2DomElement('<div my-transcluder>Hello</div>') ;
    compile.run(el)(rootScope) ;
    expect(el[0].querySelector('[kjs-transclude]').innerHTML)
      .to.be.equal('Hello') ;
  }) ;

  it.skip('may be used as element', function () {

    compile.directive('myTranscluder', function () {
      return {
        transclude: true,
        template: '<kjs-transclude>Existing content</kjs-transclude>'
      } ;
    }) ;

    let el = Utils.str2DomElement('<div my-transcluder>Hello</div>') ;
    compile.run(el)(rootScope) ;
    expect(el[0].querySelector('[kjs-transclude]').innerHTML)
      .toi.be.equal('Hello') ;
  }) ;

  it.skip('may be used as class', function () {

    compile.directive('myTranscluder', function () {
      return {
        transclude: true,
        template: '<div class="kjs-transclude">Existing contents</div>'
      } ;
    }) ;

    let el = Utils.str2DomElement('<div my-transcluder>Hello</div>') ;
    compile.run(el)(rootScope) ;
    expect(el[0].querySelector('[kjs-transclude]').innerHTML)
      .toi.be.equal('Hello') ;
  }) ;
}) ;

