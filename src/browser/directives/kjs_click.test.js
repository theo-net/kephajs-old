'use strict' ;

/**
 * src/browser/directives/kjs_click.test.js
 */

const expect = require('chai').expect,
      sinon  = require('sinon') ;

const Application = require('../Application'),
      Utils       = require('../Utils') ;

const app = new Application() ;

let click = document.createEvent('HTMLEvents') ;
click.initEvent('click', true, false) ;

describe('kjsClick', function () {

  let compile, rootScope ;

  beforeEach(function () {

    compile   = app.kernel().get('$compile') ;
    rootScope = app.kernel().get('$rootScope') ;
  }) ;

  it('starts a digest on click', function () {

    let watchSpy = sinon.spy() ;
    rootScope.$watch(watchSpy) ;

    let button = Utils.str2DomElement(
      '<button kjs-click="doSomething()"></button>') ;
    compile.run(button)(rootScope) ;

    button[0].dispatchEvent(click) ;
    expect(watchSpy.called).to.be.true ;
  }) ;

  it('evaluates given expression on click', function () {

    rootScope.doSomething = sinon.spy() ;
    let button = Utils.str2DomElement(
      '<button kjs-click="doSomething()"></button>') ;
    compile.run(button)(rootScope) ;

    button[0].dispatchEvent(click) ;
    expect(rootScope.doSomething.called).to.be.true ;
  }) ;

  it('passes $event to expression', function () {

    rootScope.doSomething = sinon.spy() ;
    let button = Utils.str2DomElement(
      '<button kjs-click="doSomething($event)"></button>') ;
    compile.run(button)(rootScope) ;

    button[0].dispatchEvent(click) ;
    let evt = rootScope.doSomething.lastCall.args[0] ;
    expect(evt).to.be.not.undefined ;
    expect(evt.type).to.be.equal('click') ;
    expect(evt.target).to.be.not.undefined ;
  }) ;
}) ;

