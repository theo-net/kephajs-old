'use strict' ;

/**
 * src/browser/directives/kjs_click.js
 */

function kjsClickDirective () {

  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      element.addEventListener('click', function (evt) {
        scope.$eval(attrs.kjsClick, {$event: evt}) ;
        scope.$apply() ;
      }) ;
    }
  } ;
}

module.exports = kjsClickDirective ;

