'use strict' ;

/**
 * src/browser/directives/kjs_click.js
 */

function kjsShowDirective () {

  return {
    restrict: 'A',
    link: function (scope, element, attrs) {

      const show = value => {

        if (!value)
          element.classList.add('visually-hidden') ;
        else
          element.classList.remove('visually-hidden') ;
      } ;

      show(attrs.kjsShow) ;
      scope.$watch(attrs.kjsShow, show) ;
    }
  } ;
}

module.exports = kjsShowDirective ;

