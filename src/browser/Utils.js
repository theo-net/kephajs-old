'use strict' ;

/**
 * src/browser/Utils.js
 */

const coreUtils = require('../core/Utils') ;


/**
 * Ajoute des outils spécifique aux scripts s'exécutant dans un navigateur
 */
class Utils extends coreUtils {

  /**
   * Test si `value` est un objet `Blob`
   * @category Lang
   * @param {*} value valeur à tester
   * @return {boolean}
   * @static
   */
  static isBlob (value) {

    return value.toString() === '[object Blob]' ;
  }


  /**
   * Test si `value` est un objet `File`
   * @category Lang
   * @param {*} value valeur à tester
   * @return {boolean}
   * @static
   */
  static isFile (value) {

    return value.toString() === '[object File]' ;
  }


  /**
   * Test si `value` un objet `FormData`
   * @category Lang
   * @param {*} value valeur à tester
   * @return {boolean}
   * @static
   */
  static isFormData (value) {

    return value.toString() === '[object FormData]' ;
  }


  /**
   * Créé du DOM à partir d'un String
   * @param {String} html HTML à transformer
   * @returns {DOM}
   * @static
   */
  static str2DomElement (html) {

    // buddy ignore:start
    let wrapMap = {
      option:  [1, '<select multiple="multiple">', '</select>'],
      legend:  [1, '<fieldset>', '</fieldset>'],
      area:    [1, '<map>', '</map>'],
      param:   [1, '<object>', '</object>'],
      thread:  [1, '<table>', '</table>'],
      tr:      [2, '<table><tbody>', '</tbody></table>'],
      col:     [2, '<table><tbody></tbody><colgroup>', '</colgroup></table>'],
      td:      [3, '<table><tbody><tr>', '</tr></tbody></table>'],
      body:    [0, '', ''],
      default: [1, '<div>', '</div>']
    } ;
    // buddy ignore:end

    wrapMap.optgroup = wrapMap.option ;
    wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption =
      wrapMap.thead ;
    wrapMap.th = wrapMap.td ;

    let match = /<\s*\w.*?>/g.exec(html) ;
    let element = document.createElement('div') ;

    if (match != null) {

      let tag = match[0].replace(/</g, '').replace(/>/g, '').split(' ')[0] ;

      if (tag.toLowerCase() === 'body') {

        //let dom = document.implementation
        //  .createDocument('http://www.w3.org/1999/xhtml', 'html', null) ;
        let body = document.createElement('body') ;

        // On garde les attribus
        element.innerHTML = html.replace(/<body>/g, '<div>')
                                .replace(/<\/body>/g, '</div>') ;
        let attrs = element.firstChild.attributes ;
        body.innerHTML = html ;

        attrs.forEach(attr => {
          body.setAttribute(attr.name, attr.value) ;
        }) ;

        return body ;
      }
      else {

        let map = wrapMap[tag] || wrapMap.default ;
        html = map[1] + html + map[2] ;
        element.innerHTML = html ;
        let j = map[0] ;
        while (j--)
          element = element.lastChild ;
        element = element.childNodes ;
      }
    }
    else {
      element.innerHTML = html ;
      element = element.childNodes ;
    }

    return element ;
  }


  /**
   * Permet de déclencher l'appel à une fonction après un certains délai et
   * réinitialise le timer si la fonction est appellée avant que le délai soit
   * dépassé.
   *
   * exemple d'appel:
  search.addEventListener('keyup', debounce(function(e) {
    // Le code ici sera exécuté au bout de 350 ms
    // mais si l'utilisateur tape une nouvelle fois durant cet intervalle
    // de temps, le timer sera réinitialisé.
  }, 350))
   *
   * @param {Function} callback La fonction
   * @param {Number} delay Le délai en ms
   * @returns {Function}
   * @static
   */
  static debounce (callback, delay) {

    let timer ;
    return function () {

      let args = arguments ;
      let context = this ;
      clearTimeout(timer) ;
      timer = setTimeout(function () {
        callback.apply(context, args) ;
      }, delay) ;
    } ;
  }


  /**
   * Permet d'éviter des appels consécutifs en introduisant un délai.
   * exemple :
  window.addEventListener('scroll', throttle(function (e) {
    // Le code ici ne pourra être exécuté que toutes les 50 ms (20 appels max
    // par secondes)
  }, 50))
   * @param {Function} callback La fonction
   * @param {Number} delay Le délai en ms
   * @returns {Function}
   * @static
   */
  static throttle (callback, delay) {

    let last ;
    let timer ;
    return function () {

      let context = this ;
      let now = +new Date() ;
      let args = arguments ;

      if (last && now < last + delay) {

        // le délai n'est pas écoulé, on reset le timer
        clearTimeout(timer) ;
        timer = setTimeout(function () {
          last = now ;
          callback.apply(context, args) ;
        }, delay) ;
      } else {
        last = now ;
        callback.apply(context, args) ;
      }
    } ;
  }


  /**
   * Retourne le chemin absolu d'un href
   *
   * @param {String} href `href` dont on veut le chemin absolu
   * @return {String}
   * @static
   */
  static absolutePath (href) {

    let link = document.createElement('a') ;
    link.href = href ;
    return (link.protocol + '//' + link.host + link.pathname + link.search
          + link.hash) ;
  }
}

module.exports = Utils ;

