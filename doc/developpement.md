Participation au projet
=======================

Nous utilisons toute une série d'outils pour vérifier la qualité du code
produit.


Vérification des dépendances
----------------------------

Nous utilsons deux outils pour vérifier les dépendances de notre projet :
`nsp` et `david`.

`nsp` vérifie qu'aucune dépendance actuelle ne comporte de vulnérabilités.

    nsp check --output [default|summary|json|codeclimate|none]

`david` vérifie que les dépendances sont à jour.

    david

Pour mettre à jour toutes les dépendances vers la dernière version stable :

    david update

Qualité du code
---------------

`buddy.js` détecte les nombres magiques dans nos scripts (tous les nombres
codés en dur et non à travers une constante.

    buddy .

Les nombres définis dans une constante seront ignorés ou en utilisant la
syntaxe suivante :

    var SECOND = 1000 ;
    var MINUTE = 60 * SECOND ; // buddy ignore:line
    var HOUR   = 60 * MINUTE ; // buddy ignore:line

ou alors :

    // buddy ignore:start
    var SECOND = 1000 ;
    var MINUTE = 60 * SECOND ;
    var HOUR   = 60 * MINUTE ;
    // buddy ignore:end


Liste des scripts NPM
---------------------

  - `build` construit le fichier contenant le framework à charger par le
    navigateur.
  - `build:minified` idem que `build`, mais produit une version minifiée.
  - `doc` construit la documentation du framework
  - `style` vérifie que tous les fichiers respectent les conventions de
    codage.
  - `test:uniq` exécute tous les tests, dont `style` (lancés une seule fois)
  - `test` vérifie les styles, et teste les parties core, cli et server
    relancés à chaque modification))
  - `test:browser` uniquement la partie navigateur (relancés à chaque
    modification de fichier et teste aussi core)
  - `check:dep` vérifie les dépendances (devrait être exécuté régulièrement)
  - `check:quality` vérifie la qualité du code
  - `validate` valide le code (`style` + `test` + `check:quality`)
  - `version` publie une nouvelle version (tag du repo, npm, et changelog auto)
     on l'utilise avec la commande `npm version major/minor/patch`


tests
-----

Ayant comme objectif de répéter le moins d'informations possible, nous
plaçons les fichiers de test à côté de leut fichier correspondant. Ainsi, on
ne duplique pas l'arborescence qui est une information.

Ces fichiers portent obligatoirement l'extension `.test.js`.

Pour que Karma puisse fonctionner avec Chromium, il faut définir la variable
d'environnement `CHROME_BIN` :

    export CHROME_BIN=/usr/bin/chromium-browser

Commit
------

Il faut exécuter `bin/beforeCi.sh` avant chaque commit !

