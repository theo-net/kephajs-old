#!/usr/bin/env node

'use strict' ;

const cli = require('../../src/index').cli() ;

cli
  .version('1.0.0')
  .command('hello', 'Affihce un Hello World!')
  .alias('salut')
  .argument('<nom>', 'Nom', 'string')
  .option('-e, --excla', 'Avec `!`')
  .action(function (args, options) {
       console.log('Hello ' + args.nom + (options.excla ? '!' : '.')) ;
     }) ;
cli.run() ;

