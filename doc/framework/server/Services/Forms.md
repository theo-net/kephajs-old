# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#addClass">`addClass`</a>
* <a href="#build">`build`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#delete">`delete`</a>
* <a href="#deleteField">`deleteField`</a>
* <a href="#getField">`getField`</a>
* <a href="#has">`has`</a>
* <a href="#hasField">`hasField`</a>
* <a href="#register">`register`</a>
* <a href="#registerField">`registerField`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="addClass"><a href="#addClass">#</a>&nbsp;<code>addClass(cssObj)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L163 "View in source") [&#x24C9;][1]

Ajoute automatiquement des classes à tous les champs ajoutés au formulaire.
Cf. Field.addClass()

#### Arguments
1. `cssObj` *(Array|String)*: Les classes à ajouter

#### Returns
*(this)*:

---

<!-- /div -->

<!-- div -->

<h3 id="build"><a href="#build">#</a>&nbsp;<code>build(id, [entity=null], [config=null])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L79 "View in source") [&#x24C9;][1]

Construit et retourne un formulaire

#### Arguments
1. `id` *(String)*: Identifiant du formBuilder à utiliser
2. `[entity=null]` *(Entity)*: Entité à transmettre
3. `[config=null]` *(&#42;)*: Configuration que l'on peut transmettre au form

#### Returns
*(Form)*:

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(validate)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L19 "View in source") [&#x24C9;][1]

Initialise le service

#### Arguments
1. `validate` *(Object)*: Service $validate

---

<!-- /div -->

<!-- div -->

<h3 id="delete"><a href="#delete">#</a>&nbsp;<code>delete(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L106 "View in source") [&#x24C9;][1]

Supprime un FormBuilder enegistré

#### Arguments
1. `id` *(String)*: Identifiant du FormBuilder à retirer

---

<!-- /div -->

<!-- div -->

<h3 id="deleteField"><a href="#deleteField">#</a>&nbsp;<code>deleteField(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L151 "View in source") [&#x24C9;][1]

Supprime un field enegistré

#### Arguments
1. `id` *(String)*: Identifiant du field à retirer

---

<!-- /div -->

<!-- div -->

<h3 id="getField"><a href="#getField">#</a>&nbsp;<code>getField(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L128 "View in source") [&#x24C9;][1]

Retourne un field

#### Arguments
1. `id` *(String)*: Identifiant du field à retourner

#### Returns
*(Field)*:

---

<!-- /div -->

<!-- div -->

<h3 id="has"><a href="#has">#</a>&nbsp;<code>has(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L96 "View in source") [&#x24C9;][1]

Indique si un FormBuilder est enregistré ou nom

#### Arguments
1. `id` *(String)*: Identifiant du FormBuilder

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="hasField"><a href="#hasField">#</a>&nbsp;<code>hasField(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L141 "View in source") [&#x24C9;][1]

Indique si un field est enregistré ou nom

#### Arguments
1. `id` *(String)*: Identifiant du field

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="register"><a href="#register">#</a>&nbsp;<code>register(id, formBuilder)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L65 "View in source") [&#x24C9;][1]

Enregistre un formBuilder *(écrasera un précédemment définit)*

#### Arguments
1. `id` *(String)*: Identifiant du FormBuilder
2. `formBuilder` *(FormBuilder)*: FormBuilder à enregistrer

#### Returns
*(this)*: pour le chaînage

---

<!-- /div -->

<!-- div -->

<h3 id="registerField"><a href="#registerField">#</a>&nbsp;<code>registerField(id, field)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L117 "View in source") [&#x24C9;][1]

Enregistre un fiedl *(écrasera un précédemment définit)*

#### Arguments
1. `id` *(String)*: Identifiant du field
2. `field` *(Field)*: Field à enregistrer

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
