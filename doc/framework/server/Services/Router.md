# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#getFiles">`getFiles`</a>
* <a href="#getRoute">`getRoute`</a>
* <a href="#load">`load`</a>
* <a href="#unroute">`unroute`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(basepath)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L24 "View in source") [&#x24C9;][1]

Initialise le service.

#### Arguments
1. `basepath` *(String)*: Chemin de base pour toutes les ressources chargées

---

<!-- /div -->

<!-- div -->

<h3 id="getFiles"><a href="#getFiles">#</a>&nbsp;<code>getFiles()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L204 "View in source") [&#x24C9;][1]

Retourne la liste des fichiers de routes chargés

#### Returns
*(Array)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getRoute"><a href="#getRoute">#</a>&nbsp;<code>getRoute(method, uri, domain)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L158 "View in source") [&#x24C9;][1]

Cherche la bonne route

#### Arguments
1. `method` *(String)*: Méthode de la requête
2. `uri` *(String)*: URI de la requête
3. `domain` *(String)*: Domaine de la requête

#### Returns
*(Object)*: Contenant deux éléments : `routes` et `vars` avec la route trouvée et les variables extraites de la requête.

---

<!-- /div -->

<!-- div -->

<h3 id="load"><a href="#load">#</a>&nbsp;<code>load(routes, [prefix], [basename], [domain])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L91 "View in source") [&#x24C9;][1]

Parse un objet de configuration ou un fichier JSON.
L'objet doit avoir la structure suivante :<br> { "leNom": { "ressource": "path", "prefix": "le préfixe", "domain": "un domain" } "unAutre": { "method": "get", "pattern": "un pattern", "domain": "le domaine à parser", "controller": "un controller", "action": "une action", "defaults": { "nom de la variable": "valeur" }, "requirements": { 'nom de la variable": "un pattern" } } }
<br>
<br>
`method` peut aussi contenir une série de méthode :<br> "method": ["get", "post"]
<br>
<br>
Chaque route doit avoir un identifiant unique.
<br>
<br>
Les routes de ressources permettent d'inclure les routes d'un autre
fichier. Dans ce cas le nom des routes sera préfixé par le nom de la
ressource dont la première lettre sera forcée en majuscule (ici `leNom`,
dont si une route de la ressource s'appelle `foobar`, son nom deviendra
`leNomFoobar`) et leur pattern par la valeur de `prefix` sauf si
celui-ci est précisé lors du chargement de la ressource, auquel cas il
servira de préfixe. Enfin, si `domain` est précisé, toutes les routes
inclues auront ce pattern de domaine par défaut. Le chemin de la
ressource doit toujours être donné relativement au `basepath` défini
lors de l'initialisation du service.
<br>
<br>
Pour plus de détails, regardez l'objet Route.
<br>
<br>
Attention, Les classes de caractères, `\d \w \s ...` doivent être
échappée : `\\d \\w \\s ...`
<br>
<br>
Attention, les routes seront examinées dans l'ordre du fichier et dans
l'ordre d'inclusion !

#### Arguments
1. `routes` *(Object|String): Objet contenant les routes ou chemin du fichier &#42;(donné par rapport u absepath)*&#42;
2. `[prefix]` *(String)*: Préfixe qui sera ajouté au pattern de toutes les routes
3. `[basename]` *(String)*: Préfixe qui sera ajouté à l'identifiant de toutes les routes
4. `[domain]` *(String)*: Pattern de domaine par défaut

---

<!-- /div -->

<!-- div -->

<h3 id="unroute"><a href="#unroute">#</a>&nbsp;<code>unroute(id, [params={}], [domain=''], [https=false])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L191 "View in source") [&#x24C9;][1]

Reconstruit une route à partir de paramètres

#### Arguments
1. `id` *(String)*: Identifiant de la route
2. `[params={}]` *(Object)*: Valeur des variables
3. `[domain='']` *(String)*: Domaine courant (permettra de déterminé s'il y a besoin d'une URL absolue ou non
4. `[https=false]` *(Boolean)*: L'URL absolue est-elle en HTTPS ?

#### Returns
*(String)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
