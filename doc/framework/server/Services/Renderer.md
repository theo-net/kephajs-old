# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#blockquote">`blockquote`</a>
* <a href="#br">`br`</a>
* <a href="#code">`code`</a>
* <a href="#codespan">`codespan`</a>
* <a href="#del">`del`</a>
* <a href="#em">`em`</a>
* <a href="#footNote">`footNote`</a>
* <a href="#heading">`heading`</a>
* <a href="#html">`html`</a>
* <a href="#link">`link`</a>
* <a href="#list">`list`</a>
* <a href="#listItem">`listItem`</a>
* <a href="#paragraph">`paragraph`</a>
* <a href="#strong">`strong`</a>
* <a href="#table">`table`</a>
* <a href="#tablecell">`tablecell`</a>
* <a href="#tablerow">`tablerow`</a>
* <a href="#taskList">`taskList`</a>
* <a href="#taskListItem">`taskListItem`</a>
* <a href="#text">`text`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="blockquote"><a href="#blockquote">#</a>&nbsp;<code>blockquote(quote)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L31 "View in source") [&#x24C9;][1]

Citation

#### Arguments
1. `quote` *(String)*: Contenu de la citation

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="br"><a href="#br">#</a>&nbsp;<code>br()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L183 "View in source") [&#x24C9;][1]

Retour à la ligne dans un paragraphe

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="code"><a href="#code">#</a>&nbsp;<code>code(code, lang)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L19 "View in source") [&#x24C9;][1]

Portion de code

#### Arguments
1. `code` *(String)*: Portion de code
2. `lang` *(String)*: Langage du code

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="codespan"><a href="#codespan">#</a>&nbsp;<code>codespan(text)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L176 "View in source") [&#x24C9;][1]

Code inline

#### Arguments
1. `text` *(String)*: Code

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="del"><a href="#del">#</a>&nbsp;<code>del(text)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L191 "View in source") [&#x24C9;][1]

Barré *(supprimé)*

#### Arguments
1. `text` *(String)*: Texte

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="em"><a href="#em">#</a>&nbsp;<code>em(text)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L168 "View in source") [&#x24C9;][1]

Emphase faible

#### Arguments
1. `text` *(String)*: Texte à mettre en emphase

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="footNote"><a href="#footNote">#</a>&nbsp;<code>footNote(nb)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L224 "View in source") [&#x24C9;][1]

footNote

#### Arguments
1. `nb` *(Integer)*: Numéro de la note

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="heading"><a href="#heading">#</a>&nbsp;<code>heading(text, level, raw)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L52 "View in source") [&#x24C9;][1]

Titre

#### Arguments
1. `text` *(String)*: Texte du titre
2. `level` *(Integer): Niveau &#42;(entre `1` et `6`)*&#42;
3. `raw` *(String)*: Texte du titre sans transformation

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="html"><a href="#html">#</a>&nbsp;<code>html(html)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L42 "View in source") [&#x24C9;][1]

Code HTML

#### Arguments
1. `html` *(String)*: Code qui sera retourné tel quel

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="link"><a href="#link">#</a>&nbsp;<code>link(href, title, text)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L201 "View in source") [&#x24C9;][1]

Lien

#### Arguments
1. `href` *(String)*: URL du lien
2. `title` *(String): Attribut title du lien &#42;(peut être vide)*&#42;
3. `text` *(String)*: Texte du lien

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="list"><a href="#list">#</a>&nbsp;<code>list(body, ordered)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L66 "View in source") [&#x24C9;][1]

Liste

#### Arguments
1. `body` *(String)*: Contenu de la liste
2. `ordered` *(Boolean)*: Liste ordonnée ou non

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="listItem"><a href="#listItem">#</a>&nbsp;<code>listItem(text)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L78 "View in source") [&#x24C9;][1]

Item d'une liste

#### Arguments
1. `text` *(String)*: Texte de l'item

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="paragraph"><a href="#paragraph">#</a>&nbsp;<code>paragraph(text)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L113 "View in source") [&#x24C9;][1]

Un paragraphe

#### Arguments
1. `text` *(String)*: Texte

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="strong"><a href="#strong">#</a>&nbsp;<code>strong(text)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L160 "View in source") [&#x24C9;][1]

Emphase forte

#### Arguments
1. `text` *(String)*: Texte à mettre en emphase

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="table"><a href="#table">#</a>&nbsp;<code>table(header, body)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L122 "View in source") [&#x24C9;][1]

Tableau

#### Arguments
1. `header` *(String): Contenu du header &#42;(`<thead>`)*&#42;
2. `body` *(String): Contenu du corps &#42;(`<tbody>`)*&#42;

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="tablecell"><a href="#tablecell">#</a>&nbsp;<code>tablecell(content, flags)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L145 "View in source") [&#x24C9;][1]

Cellule d'un tableau

#### Arguments
1. `content` *(String)*: Contenu de la cellule
2. `flags` *(Object): Si la proprité `header` est définie, il s'agit d'une cellule d'un header &#42;(`th`), la proprité `align` (facultative)*&#42; définie l'alignement

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="tablerow"><a href="#tablerow">#</a>&nbsp;<code>tablerow(content)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L134 "View in source") [&#x24C9;][1]

Ligne d'un tableau

#### Arguments
1. `content` *(String)*: Contenu de la ligne

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="taskList"><a href="#taskList">#</a>&nbsp;<code>taskList(body, ordered)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L87 "View in source") [&#x24C9;][1]

Liste de tâche

#### Arguments
1. `body` *(String)*: Contenu de la liste
2. `ordered` *(Boolean)*: Liste ordonnée ou non

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="taskListItem"><a href="#taskListItem">#</a>&nbsp;<code>taskListItem(text, checked)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L100 "View in source") [&#x24C9;][1]

Item d'une liste de tâches

#### Arguments
1. `text` *(String)*: Texte de l'item
2. `checked` *(Boolean)*: Tâche effectuée ou non

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="text"><a href="#text">#</a>&nbsp;<code>text(text)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L216 "View in source") [&#x24C9;][1]

Simple texte

#### Arguments
1. `text` *(String)*: Le texte

#### Returns
*(String)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
