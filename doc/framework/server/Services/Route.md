# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#getCallable">`getCallable`</a>
* <a href="#match">`match`</a>
* <a href="#unroute">`unroute`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(method, pattern, callable, [requirements={}], [defaults={}], [domain='*'])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L57 "View in source") [&#x24C9;][1]

Construit une route à partir de différents éléments.
<br>
<br>
`methode` défini la ou les méthodes pour lesquelles cette route est valide
*(get, post, delete, ...)*. Un `string` suffit pour une méthode, utilisez
un `array` pour plusieurs. Elles sont insensibles à la case.
<br>
<br>
Le pattern de la route est une chaînes permettant de matcher la route :<br> '/index.html'  // ne traitera que celle-ci '/{style}.css' // tous ce qui se termine par `.css` et définie une var
<br>
<br>
Les variables seront traitées et leur valeur retournées après le parsage.
Vous pouvez, avec des objets `nomDeLaVariable: valeur` définir certaintes
choses :<br>
<br> * un objet `defaults` pour les valeurs par défaut *(facultatif)*
<br> * un objet `requirements` pour la RegExp correspondant à la valeur
<br>
<br>
Une variable facultative commence par un `?`, ex : `{?facultatif}
<br>
<br>
On peut spécifier quelle partie de la variable récupérer (très pratique
pour certains patterns. On prend le pattern `/news-{var1}{var2}.html`
et on aura en `requirements` : `var1: \w+` et `var2: -(\d+)` ainsi
les URI `/news-a.html` et `/news-a-2.html` matcheront, mais `var2`
aura comme valeur `2` et non `-2`. Attention, il ne faut qu'une paire de
parenthèses.
<br>
<br>
Mettre un élément dans `defaults` qui n'est pas présent dans le pattern et
ni dans le `requirements` est un bon moyen de forcer le passage d'une
valeur.
<br>
<br>
`domain` est traité comme une RegExp.
<br>
<br>
Enfin, échappez toutes les classes de caractères comme par exemple `\w` en
mettant `\\w`

#### Arguments
1. `method` *(Array|String): Méthode(s)* de la route
2. `pattern` *(String)*: Pattern de la route
3. `callable` *(Array): Tableau &#91;'controller', 'action'&#93; ou &#91;object&#93; avec `object` l'objet dont on appelera la méthode exec()*
4. `[requirements={}]` *(Object)*: Patterns des variables
5. `[defaults={}]` *(Object): Valeur par défaut des variables &#42;(nomVar: val)*&#42;
6. `[domain='*']` *(String)*: Pattern du domain

---

<!-- /div -->

<!-- div -->

<h3 id="getCallable"><a href="#getCallable">#</a>&nbsp;<code>getCallable()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L94 "View in source") [&#x24C9;][1]

Retourne le callable

#### Returns
*(Array)*:

---

<!-- /div -->

<!-- div -->

<h3 id="match"><a href="#match">#</a>&nbsp;<code>match(method, uri, domain)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L108 "View in source") [&#x24C9;][1]

Parse une méthode, une URI et un domaine pour déterminer si la route match

#### Arguments
1. `method` *(String)*: Méthode
2. `uri` *(String)*: URI
3. `domain` *(String)*: Domaine

#### Returns
*(&#42;)*: false si ne match pas, un objet sinon, avec :<br> route: this, vars: variables

---

<!-- /div -->

<!-- div -->

<h3 id="unroute"><a href="#unroute">#</a>&nbsp;<code>unroute(params, [domain=''], [https=false])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L139 "View in source") [&#x24C9;][1]

Reconstruit une URL à partir de paramètres

#### Arguments
1. `params` *(Object)*: Paramètres
2. `[domain='']` *(String)*: Domaine courant (permettra de déterminé s'il y a besoin d'une URL absolue ou non
3. `[https=false]` *(Boolean)*: L'URL absolue est-elle en HTTPS ?

#### Returns
*(String)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
