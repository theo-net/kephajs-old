# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#$getMimeType">`$getMimeType`</a>
* <a href="#$getRoute">`$getRoute`</a>
* <a href="#$getSlot">`$getSlot`</a>
* <a href="#$render">`$render`</a>
* <a href="#$setContent">`$setContent`</a>
* <a href="#$setLayout">`$setLayout`</a>
* <a href="#$setMimeType">`$setMimeType`</a>
* <a href="#constructor">`constructor`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="$getMimeType"><a href="#$getMimeType">#</a>&nbsp;<code>$getMimeType()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L141 "View in source") [&#x24C9;][1]

Retourne le type MIME

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="$getRoute"><a href="#$getRoute">#</a>&nbsp;<code>$getRoute(id, vars)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L240 "View in source") [&#x24C9;][1]

Helper : retourne une URL construite à partir du routeur. Https si le
paramètre `forceHttps` du `$config` vaut true.

#### Arguments
1. `id` *(String)*: Identifiant de la router
2. `vars` *(Object)*: Paramètres

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="$getSlot"><a href="#$getSlot">#</a>&nbsp;<code>$getSlot(controller, action, [vars])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L214 "View in source") [&#x24C9;][1]

Helper : retourne le contenu d'un autre Slot

#### Arguments
1. `controller` *(String)*: Controlleur
2. `action` *(String)*: Action
3. `[vars]` *(Object)*: Variables à transmettre

#### Returns
*(Promise)*:

---

<!-- /div -->

<!-- div -->

<h3 id="$render"><a href="#$render">#</a>&nbsp;<code>$render()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L87 "View in source") [&#x24C9;][1]

Génère le rendu de la vue et renvoit le contenu
Tout le contenu de `$view.helpers` défini dans le service `$config` sera
disponible dans la vue. Cela permet de créer un système d'helpers.

#### Returns
*(Promise)*:

---

<!-- /div -->

<!-- div -->

<h3 id="$setContent"><a href="#$setContent">#</a>&nbsp;<code>$setContent(content, [contentFile])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L177 "View in source") [&#x24C9;][1]

Définie le contenu de la vue

#### Arguments
1. `content` *(String)*: Contenu
2. `[contentFile]` *(String)*: Fichier contenant la vue

---

<!-- /div -->

<!-- div -->

<h3 id="$setLayout"><a href="#$setLayout">#</a>&nbsp;<code>$setLayout(layout)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L153 "View in source") [&#x24C9;][1]

Défini un layout en chargeant le fichier `layout`. Si ce paramètre vaut
`false`, alors aucun layout ne sera chargé.

#### Arguments
1. `layout` *(Boolean|String)*: Chemin vers le layout

---

<!-- /div -->

<!-- div -->

<h3 id="$setMimeType"><a href="#$setMimeType">#</a>&nbsp;<code>$setMimeType(mime)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L127 "View in source") [&#x24C9;][1]

Définie le type Mime

#### Arguments
1. `mime` *(String)*: Type Mime

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor([contentFile], [mime=html])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L47 "View in source") [&#x24C9;][1]

Créé une nouvelle vue. Si `contentFile` est `null`, une vue vide sera
chargée

#### Arguments
1. `[contentFile]` *(String)*: Fichier contenant la vue
2. `[mime=html]` *(String)*: Mime type

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
