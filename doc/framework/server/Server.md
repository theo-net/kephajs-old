# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#_requestListener">`_requestListener`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#get">`get`</a>
* <a href="#setFormBuilders">`setFormBuilders`</a>
* <a href="#setModels">`setModels`</a>
* <a href="#startServer">`startServer`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="_requestListener"><a href="#_requestListener">#</a>&nbsp;<code>_requestListener(request, res)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L163 "View in source") [&#x24C9;][1]

Exécute la requête reçue.

#### Arguments
1. `request` *(http.IncomingMessage)*: Requête reçue
2. `res` *(http.ServerResponse)*: Réponse du serveur

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(config)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L37 "View in source") [&#x24C9;][1]

Initialise le serveur et le démarre.

#### Arguments
1. `config` *(Object)*: Configuration du serveur

---

<!-- /div -->

<!-- div -->

<h3 id="get"><a href="#get">#</a>&nbsp;<code>get(service)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L128 "View in source") [&#x24C9;][1]

Alias vers this.kernel().get()

#### Arguments
1. `service` *(String)*: Nom du service

#### Returns
*(Object)*:

---

<!-- /div -->

<!-- div -->

<h3 id="setFormBuilders"><a href="#setFormBuilders">#</a>&nbsp;<code>setFormBuilders(formBuilders)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L150 "View in source") [&#x24C9;][1]

Définis les formsBuilders dispobibles dans l'application.

#### Arguments
1. `formBuilders` *(Object)*: formBuilders : nom => class

---

<!-- /div -->

<!-- div -->

<h3 id="setModels"><a href="#setModels">#</a>&nbsp;<code>setModels(models)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L138 "View in source") [&#x24C9;][1]

Définis les modèles disponibles dans l'application.

#### Arguments
1. `models` *(Object)*: Modèles : nom => instance

---

<!-- /div -->

<!-- div -->

<h3 id="startServer"><a href="#startServer">#</a>&nbsp;<code>startServer(port)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L102 "View in source") [&#x24C9;][1]

Démarre un serveur

#### Arguments
1. `port` *(Integer)*: Port sur lequel écoute le serveur

#### Returns
*(http.Server)*: Serveur créé

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
