# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#_getSession">`_getSession`</a>
* <a href="#browserInfo">`browserInfo`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#delete">`delete`</a>
* <a href="#deleteCookie">`deleteCookie`</a>
* <a href="#get">`get`</a>
* <a href="#getCookie">`getCookie`</a>
* <a href="#getFlash">`getFlash`</a>
* <a href="#has">`has`</a>
* <a href="#hasCookie">`hasCookie`</a>
* <a href="#hasFlash">`hasFlash`</a>
* <a href="#set">`set`</a>
* <a href="#setCookie">`setCookie`</a>
* <a href="#setFlash">`setFlash`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="_getSession"><a href="#_getSession">#</a>&nbsp;<code>_getSession()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L349 "View in source") [&#x24C9;][1]

Récupère une session, met à jour l'id courant de l'user. On fera
également un coucou au gestionnaire pour mettre à jour le dernier accès

#### Returns
*(&#42;): identifiant de la session ou `false` si aucune session trouvée (pas de cookie ou pas en mémoire)*

---

<!-- /div -->

<!-- div -->

<h3 id="browserInfo"><a href="#browserInfo">#</a>&nbsp;<code>browserInfo()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L280 "View in source") [&#x24C9;][1]

Retourne quelques infos sur le navigateur dans un objet ayant les propriétés
suivantes :<br>
<br> * `user-agent {String}` : User agent du navigateur
<br> * `accept {Array}` : liste des content-types acceptés
<br> * `accept-language {Array}` : liste des langages acceptés
<br> * `accept-encoding {Array}` : lise des encodages autorisés

#### Returns
*(Object)*:

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(request, response, sessions, [config])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L38 "View in source") [&#x24C9;][1]

Initilise l'objet avec la requête et la réponse. sessionCookieName:     'kjs_session_id', sessionCookieDomain:   null, sessionCookiePath:     null, sessionCookieSecure:   false, sessionCookieHttpOnly: false

#### Arguments
1. `request` *(http.ServerRequest)*: La requête
2. `response` *(Resp)*: La réponse
3. `sessions` *(Sessions)*: Service gérant les sessions
4. `[config]` *(Object)*: Configuration

---

<!-- /div -->

<!-- div -->

<h3 id="delete"><a href="#delete">#</a>&nbsp;<code>delete(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L207 "View in source") [&#x24C9;][1]

Supprime un élément

#### Arguments
1. `name` *(String)*: Nom de la variable

---

<!-- /div -->

<!-- div -->

<h3 id="deleteCookie"><a href="#deleteCookie">#</a>&nbsp;<code>deleteCookie(name, domain, path, secure, httpOnly)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L151 "View in source") [&#x24C9;][1]

Supprime un cookie. Attention, on doit passer les mêmes options pour
supprimer le cookie que celles envoyées lors de sa création

#### Arguments
1. `name` *(String)*: Cookie à supprimer
2. `domain` *(String)*: Inclue les sous-domaine
3. `path` *(String)*: Chemin pour lesquels s'applique le cookie
4. `secure` *(Boolean)*: Le cookie ne sera transmis qu'en HTTPS
5. `httpOnly` *(Boolean)*: Le cookie ne sera disponible que dans les requête, pas dans la page à travers les méthodes `Document.cookie`, `XMLHttpRequest` ou `Request`

---

<!-- /div -->

<!-- div -->

<h3 id="get"><a href="#get">#</a>&nbsp;<code>get(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L197 "View in source") [&#x24C9;][1]

Retourne la valeur d'un élément

#### Arguments
1. `name` *(String)*: Nom de la variable

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getCookie"><a href="#getCookie">#</a>&nbsp;<code>getCookie(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L123 "View in source") [&#x24C9;][1]

Retourne la valeur d'un cookie

#### Arguments
1. `name` *(String)*: Nom du cookie

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getFlash"><a href="#getFlash">#</a>&nbsp;<code>getFlash()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L258 "View in source") [&#x24C9;][1]

Retourne la liste des messages flash

#### Returns
*(Array)*: Tableau contenant des objets de la forme {type, content}

---

<!-- /div -->

<!-- div -->

<h3 id="has"><a href="#has">#</a>&nbsp;<code>has(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L186 "View in source") [&#x24C9;][1]

Indique si un élément est défini

#### Arguments
1. `name` *(String)*: Nom de la variable

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="hasCookie"><a href="#hasCookie">#</a>&nbsp;<code>hasCookie(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L134 "View in source") [&#x24C9;][1]

Indique si un cookie existe ou pas

#### Arguments
1. `name` *(String)*: Nom du cookie

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="hasFlash"><a href="#hasFlash">#</a>&nbsp;<code>hasFlash()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L245 "View in source") [&#x24C9;][1]

Indique si des messages falsh sont enregistrés en session

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="set"><a href="#set">#</a>&nbsp;<code>set(name, value)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L175 "View in source") [&#x24C9;][1]

Définie un élément dans la session

#### Arguments
1. `name` *(String)*: Nom de la variable
2. `value` *(&#42;)*: Valeur

---

<!-- /div -->

<!-- div -->

<h3 id="setCookie"><a href="#setCookie">#</a>&nbsp;<code>setCookie(name, value, expires, maxAge, domain, path, secure, httpOnly)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L90 "View in source") [&#x24C9;][1]

Enregistre un cookie. Si expires et maxAge sont `undefined` ou `null`
alors la durée de vie du cookie est celle de la session du navigateur.
Attention, les navigateurs ont tendance à restaurer ces cookies.
Si `domain` n'est pas défini, alors le cookie s'appliquera au domain
courant, mais pas aux sous-domaines.

#### Arguments
1. `name` *(String)*: Nom du cookie
2. `value` *(String)*: Valeur du cookie
3. `expires` *(Integer): Date d'expiration &#42;(timestamp)*&#42;
4. `maxAge` *(Integer)*: Durée de vie du cookie
5. `domain` *(String)*: Inclue les sous-domaine
6. `path` *(String)*: Chemin pour lesquels s'applique le cookie
7. `secure` *(Boolean)*: Le cookie ne sera transmis qu'en HTTPS
8. `httpOnly` *(Boolean)*: Le cookie ne sera disponible que dans les requête, pas dans la page à travers les méthodes `Document.cookie`, `XMLHttpRequest` ou `Request`

---

<!-- /div -->

<!-- div -->

<h3 id="setFlash"><a href="#setFlash">#</a>&nbsp;<code>setFlash(content, type)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L229 "View in source") [&#x24C9;][1]

Enregistre un message flash

#### Arguments
1. `content` *(String)*: Le message
2. `type` *(String): Type &#42;(success, info, wrning, danger)*&#42;

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
