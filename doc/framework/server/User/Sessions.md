# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#delete">`delete`</a>
* <a href="#get">`get`</a>
* <a href="#has">`has`</a>
* <a href="#hasElement">`hasElement`</a>
* <a href="#hello">`hello`</a>
* <a href="#init">`init`</a>
* <a href="#remove">`remove`</a>
* <a href="#set">`set`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(storage)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L21 "View in source") [&#x24C9;][1]

Initialise le service avec un objet gérant le stockage des sessions

#### Arguments
1. `storage` *(Object)*: Objet gérant le stockage des sessions

---

<!-- /div -->

<!-- div -->

<h3 id="delete"><a href="#delete">#</a>&nbsp;<code>delete(id, name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L109 "View in source") [&#x24C9;][1]

Supprime un élément de la sessions

#### Arguments
1. `id` *(String)*: Identifiant de la session
2. `name` *(String)*: Nom de la valeur

---

<!-- /div -->

<!-- div -->

<h3 id="get"><a href="#get">#</a>&nbsp;<code>get(id, name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L98 "View in source") [&#x24C9;][1]

Récupère un élément de la session

#### Arguments
1. `id` *(String)*: Identifiant de la session
2. `name` *(String)*: Nom de la valeur

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="has"><a href="#has">#</a>&nbsp;<code>has(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L59 "View in source") [&#x24C9;][1]

Indique si une session existe ou pas. Retournera également `false` si elle
a expiré.

#### Arguments
1. `id` *(String)*: Identifiant de la session

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="hasElement"><a href="#hasElement">#</a>&nbsp;<code>hasElement(id, name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L86 "View in source") [&#x24C9;][1]

Indique si la session possède ou non un élément

#### Arguments
1. `id` *(String)*: Identifiant de la session
2. `name` *(String)*: Nom de la valeur

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="hello"><a href="#hello">#</a>&nbsp;<code>hello(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L121 "View in source") [&#x24C9;][1]

Indique un accès demandé à la session. Lance le garbage collector s'il y a
plus de `200` sessions en mémoire. Les sessions ont une durée de vie de
1440 s soit `24` min.

#### Arguments
1. `id` *(String)*: Identifiant de la session

---

<!-- /div -->

<!-- div -->

<h3 id="init"><a href="#init">#</a>&nbsp;<code>init()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L31 "View in source") [&#x24C9;][1]

Initialise une nouvelle session, vérifiera que l'identifiant est unique

---

<!-- /div -->

<!-- div -->

<h3 id="remove"><a href="#remove">#</a>&nbsp;<code>remove(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L47 "View in source") [&#x24C9;][1]

Supprime une session

#### Arguments
1. `id` *(String)*: Identifiant de la session à supprimer

---

<!-- /div -->

<!-- div -->

<h3 id="set"><a href="#set">#</a>&nbsp;<code>set(id, name, value)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L74 "View in source") [&#x24C9;][1]

Définit un élément dans une session

#### Arguments
1. `id` *(String)*: Identifiant de la session
2. `name` *(String)*: Nom de la variable
3. `value` *(&#42;)*: Valeur

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
