# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#addClass">`addClass`</a>
* <a href="#build">`build`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#getClass">`getClass`</a>
* <a href="#getErrors">`getErrors`</a>
* <a href="#getGroupClass">`getGroupClass`</a>
* <a href="#getLabelClass">`getLabelClass`</a>
* <a href="#getMsgClass">`getMsgClass`</a>
* <a href="#getValue">`getValue`</a>
* <a href="#hasErrors">`hasErrors`</a>
* <a href="#isSubmit">`isSubmit`</a>
* <a href="#isValid">`isValid`</a>
* <a href="#setValue">`setValue`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="addClass"><a href="#addClass">#</a>&nbsp;<code>addClass(cssObj)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L276 "View in source") [&#x24C9;][1]

Ajoute des classes CSS au champ. Prend comme argument un objet décrivant
la liste des classes à appliquer. Les propriétés de l'objet peuvent
être :<br>
<br> * `field` : classes appliquées au champ
<br> * `label` : classes appliquées au label
<br> * `group` : classes appliquées au groupe *(élément englobant le field)*
<br> * `groupSuccess` : classes, en plus de celles de `group`, appliquées lorsque la validation a eu lieue sans erreur.
<br> * `groupError` : classes, en plus de celles de `group`, appliquées lorsque la validation a eu lieue avec des erreurs.
<br> * `msg` : classes appliquées au message associé au champ
<br> * `msgHelp` : classes, en plus de celles de `msg`,  appliquées lorsque le message est l'indication d'aide
<br> * `msgError` : classes, en plus de celles de `msg`,  appliquées lorsque le message est la description de l'erreur.
Leur valeur est soit une chaîne de caractères soit un tableau de chaînes

#### Arguments
1. `cssObj` *(Object)*: Classes CSS

---

<!-- /div -->

<!-- div -->

<h3 id="build"><a href="#build">#</a>&nbsp;<code>build()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L103 "View in source") [&#x24C9;][1]

Construit le champ *(rendu HTML)*. Vous devrez redéfinir cette méthode

#### Returns
*(String)*: Code HTML

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(attributes, [validator], [validate])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L53 "View in source") [&#x24C9;][1]

Construit un champ. L'argument `attributes` permet de spécifier
différentes choses au champ :<br>
<br> * `name` : attribut `name`
<br> * `[id]` : attribut `id`, vaut `ættributes.name` par défaut
<br> * `[bind]` : lie le `name` avec le nom d'une propriété de l'entité associée au formulaire.
<br> * `[value]` : attribut `value` du champ et appellera `this.setValue()`
<br> * `[label]` : label du champ
<br> * `[required]` : `boolean` le champ est-il requis ? Vaut `false` par défaut
<br> * `[disabled]` : `boolean` le champ est-il desactivé ? Vaut `false` par défaut
<br> * `[class]` : classe CSS à appliquer au champ *(cf méthode `addClass()`)*
<br> * `[help]` : message indicatif *(format du champ, obligatoire, ...)*
<br> * `[error]` : message si valeur non valide *(écrase tous les autres)*
<br>
<br>
Pour information, voici l'arborescence des fields :<br>
<br> * Field |- Input |   |- Button |   |- Reset |   |- Radio |   |   |- Checkbox |   |- Date |   |- String |   |   |- Email |   |   |- Password |   |   |- Search |   |   |- Tel |   |   |- Url |   |- Number |   |   |- Range |   |- Time |   |- Hidden |- Text

#### Arguments
1. `attributes` *(Object)*: Attributs du champ
2. `[validator]` *(&#42;)*: Validateur pour tester les valeurs
3. `[validate]` *(&#42;)*: Service permettant de construire le validator

---

<!-- /div -->

<!-- div -->

<h3 id="getClass"><a href="#getClass">#</a>&nbsp;<code>getClass()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L298 "View in source") [&#x24C9;][1]

Retourne la liste des classes appliquées au champ

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getErrors"><a href="#getErrors">#</a>&nbsp;<code>getErrors()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L206 "View in source") [&#x24C9;][1]

Retourne les erreurs issues du validator. Attention, `isValid()` doit
avoir été exécuté.

#### Returns
*(Array)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getGroupClass"><a href="#getGroupClass">#</a>&nbsp;<code>getGroupClass()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L318 "View in source") [&#x24C9;][1]

Retourne la liste des classes appliquées au groupe

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getLabelClass"><a href="#getLabelClass">#</a>&nbsp;<code>getLabelClass()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L308 "View in source") [&#x24C9;][1]

Retourne la liste des classes appliquées au label

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getMsgClass"><a href="#getMsgClass">#</a>&nbsp;<code>getMsgClass([type=help])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L334 "View in source") [&#x24C9;][1]

Retourne la liste des classes appliquées au message

#### Arguments
1. `[type=help]` *(String): Type du message &#42;(`help` ou `error`)*&#42;

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getValue"><a href="#getValue">#</a>&nbsp;<code>getValue()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L251 "View in source") [&#x24C9;][1]

Retourne la valeur du field

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="hasErrors"><a href="#hasErrors">#</a>&nbsp;<code>hasErrors()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L238 "View in source") [&#x24C9;][1]

Indique s'il y a des erreurs de validations

#### Returns
*(&#42;)*: `null` si non validé/soumis

---

<!-- /div -->

<!-- div -->

<h3 id="isSubmit"><a href="#isSubmit">#</a>&nbsp;<code>isSubmit()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L228 "View in source") [&#x24C9;][1]

Indique si le champ a été testé *(formulaire soumis)*

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="isValid"><a href="#isValid">#</a>&nbsp;<code>isValid()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L176 "View in source") [&#x24C9;][1]

Indique si la valeur associée au champ est valide. Enverra également une
erreur si le champ est requis, mais qu'aucune valeur n'est associée. La
vérification avec les valdiateurs n'a pas lieue pour un champ non requis
vide.
Vous pouvez étendre son comportement.

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="setValue"><a href="#setValue">#</a>&nbsp;<code>setValue(value)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L161 "View in source") [&#x24C9;][1]

Définit la valeur du field. Sera appelé lors de l'initialisation du champ
avec la valeur de `attributes.value` et remplira la propriété
`this._valueAttr` utilisée pour spécifier l'attribut `value` de l'élément
HTML. Peut être personnalisé
ex :<br> setValue *(value) { if (value)* this._checked = true ; else this._checked = false ; this._value = value ; } } // Dans ce cas, il faut étendre le `constructor` pour appliquer la // valeur de `attributes.checked`

#### Arguments
1. `value` *(&#42;)*: Valeur

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
