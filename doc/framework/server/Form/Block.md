# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#add">`add`</a>
* <a href="#addBlock">`addBlock`</a>
* <a href="#addClass">`addClass`</a>
* <a href="#addFieldset">`addFieldset`</a>
* <a href="#build">`build`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#getClass">`getClass`</a>
* <a href="#getEntity">`getEntity`</a>
* <a href="#getId">`getId`</a>
* <a href="#getParent">`getParent`</a>
* <a href="#setEntity">`setEntity`</a>
* <a href="#setId">`setId`</a>
* <a href="#setParent">`setParent`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="add"><a href="#add">#</a>&nbsp;<code>add(type, attributes, validator)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L130 "View in source") [&#x24C9;][1]

Ajoute un champ au block

#### Arguments
1. `type` *(String)*: Type du champ
2. `attributes` *(Object)*: Attributs du champs, doit avoir au moins `name`
3. `validator` *(&#42;): Validator &#42;(cf service)*&#42;

#### Returns
*(this)*:

---

<!-- /div -->

<!-- div -->

<h3 id="addBlock"><a href="#addBlock">#</a>&nbsp;<code>addBlock([css])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L171 "View in source") [&#x24C9;][1]

Ajoute un bloc

#### Arguments
1. `[css]` *(Array|String)*: Classes css à appliquer au bloc

#### Returns
*(Block)*:

---

<!-- /div -->

<!-- div -->

<h3 id="addClass"><a href="#addClass">#</a>&nbsp;<code>addClass(cssObj)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L214 "View in source") [&#x24C9;][1]

Ajoute automatiquement des classes à tous les champs ajoutés au formulaire.
Cf. Field.addClass(). S'il s'agit d'un `Array` ou d'un `String`, les classes
seront ajoutées pour le block courant.

#### Arguments
1. `cssObj` *(Array|Object|String)*: Les classes à ajouter

#### Returns
*(this)*:

---

<!-- /div -->

<!-- div -->

<h3 id="addFieldset"><a href="#addFieldset">#</a>&nbsp;<code>addFieldset([legend=null], classCss)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L191 "View in source") [&#x24C9;][1]

Ajoute un fieldset *(groupe de champs)*.

#### Arguments
1. `[legend=null]` *(String)*: Légende du Fieldset
2. `classCss` *(Object)*: Classes CSS à transmettre aux fields enfants du fieldset

#### Returns
*(Fieldset)*:

---

<!-- /div -->

<!-- div -->

<h3 id="build"><a href="#build">#</a>&nbsp;<code>build()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L251 "View in source") [&#x24C9;][1]

Rendu du bloc

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor([entity=null], [forms], [idPrefix])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L23 "View in source") [&#x24C9;][1]

Initialise un nouveau block.
En lui donnant une entité, on permet l'autoremplissage des champs et
l'autovalidation

#### Arguments
1. `[entity=null]` *(Entity)*: Entité à transmettre
2. `[forms]` *(Forms)*: Service $forms
3. `[idPrefix]` *(String)*: Préfixe de l'id quand celui-ci est défini

---

<!-- /div -->

<!-- div -->

<h3 id="getClass"><a href="#getClass">#</a>&nbsp;<code>getClass()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L241 "View in source") [&#x24C9;][1]

Retourne les classes CSS appliquées au block.

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getEntity"><a href="#getEntity">#</a>&nbsp;<code>getEntity()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L66 "View in source") [&#x24C9;][1]

Retourne l'entité associée au block

#### Returns
*(Entity)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getId"><a href="#getId">#</a>&nbsp;<code>getId()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L116 "View in source") [&#x24C9;][1]

Retourne l'id du block.

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getParent"><a href="#getParent">#</a>&nbsp;<code>getParent()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L88 "View in source") [&#x24C9;][1]

Retourne le block parent

#### Returns
*(Block)*:

---

<!-- /div -->

<!-- div -->

<h3 id="setEntity"><a href="#setEntity">#</a>&nbsp;<code>setEntity([entity=null])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L55 "View in source") [&#x24C9;][1]

Associe une entité au block. On peut spécifier de ne pas en avoir avec
la valeur `null` *(par défaut)*

#### Arguments
1. `[entity=null]` *(Entity)*: Entité à associer

#### Returns
*(this)*:

---

<!-- /div -->

<!-- div -->

<h3 id="setId"><a href="#setId">#</a>&nbsp;<code>setId(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L101 "View in source") [&#x24C9;][1]

Définit l'id du block. Celui-ci sera utilisé pour préfixer tous les noms
et id des champs. Il sera préfixé, en camelCase, par le préfixe défini
lors de la construction de l'objet.

#### Arguments
1. `id` *(String)*: Id du block

#### Returns
*(this)*:

---

<!-- /div -->

<!-- div -->

<h3 id="setParent"><a href="#setParent">#</a>&nbsp;<code>setParent(block)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L77 "View in source") [&#x24C9;][1]

Définie un block parent associé au block

#### Arguments
1. `block` *(Block)*: Block

#### Returns
*(this)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
