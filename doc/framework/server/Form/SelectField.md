# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#getValue">`getValue`</a>
* <a href="#setValue">`setValue`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="getValue"><a href="#getValue">#</a>&nbsp;<code>getValue([list])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L82 "View in source") [&#x24C9;][1]

Retourne la valeur du field

#### Arguments
1. `[list]` *(Array): Liste sur laquelle on applique la valeur &#42;(utile uniquement en interne)*&#42;

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="setValue"><a href="#setValue">#</a>&nbsp;<code>setValue(value, [list])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L57 "View in source") [&#x24C9;][1]

Définie la valeur du field

#### Arguments
1. `value` *(&#42;)*: Valeur ou tableau de valeurs
2. `[list]` *(Array): Liste sur laquelle on applique la valeur &#42;(utile uniquement en interne)*&#42;

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
