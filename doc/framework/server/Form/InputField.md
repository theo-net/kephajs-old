# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#setValue">`setValue`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="setValue"><a href="#setValue">#</a>&nbsp;<code>setValue(value)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L47 "View in source") [&#x24C9;][1]

Étend la méthode de Field en exécutant un `trim` sur la valeur pour
supprimer les espaces en début et fin de chaîne de caractères.

#### Arguments
1. `value` *(&#42;)*: Valeur

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
