# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#action">`action`</a>
* <a href="#color">`color`</a>
* <a href="#command">`command`</a>
* <a href="#description">`description`</a>
* <a href="#fatalError">`fatalError`</a>
* <a href="#help">`help`</a>
* <a href="#newTable">`newTable`</a>
* <a href="#run">`run`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="action"><a href="#action">#</a>&nbsp;<code>action(action)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L138 "View in source") [&#x24C9;][1]

Définie une action pour le programme. Il s'agira de l'action par défaut si
aucune commande spécifique est appelée.

#### Arguments
1. `action` *(Function)*: Action à exécuter

#### Returns
*(Program)*:

---

<!-- /div -->

<!-- div -->

<h3 id="color"><a href="#color">#</a>&nbsp;<code>color()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L77 "View in source") [&#x24C9;][1]

Renvoit le service color

#### Returns
*(Color)*:

---

<!-- /div -->

<!-- div -->

<h3 id="command"><a href="#command">#</a>&nbsp;<code>command(synopsis, description)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L123 "View in source") [&#x24C9;][1]

Créé une nouvelle commande et la retourne

#### Arguments
1. `synopsis` *(String)*: Synopsis de la commande
2. `description` *(String)*: Description de la commande

#### Returns
*(Command)*:

---

<!-- /div -->

<!-- div -->

<h3 id="description"><a href="#description">#</a>&nbsp;<code>description([description])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L105 "View in source") [&#x24C9;][1]

Définit ou donne la description de notre application

#### Arguments
1. `[description]` *(String)*: Description de l'applciation

#### Returns
*(&#42;)*: La description, si aucun paramètre, `this` sinon

---

<!-- /div -->

<!-- div -->

<h3 id="fatalError"><a href="#fatalError">#</a>&nbsp;<code>fatalError(errObj)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L252 "View in source") [&#x24C9;][1]

Renvoit une erreur fatale

#### Arguments
1. `errObj` *(Object)*: Erreur à transmettre

---

<!-- /div -->

<!-- div -->

<h3 id="help"><a href="#help">#</a>&nbsp;<code>help(cmdStr)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L237 "View in source") [&#x24C9;][1]

Affiche l'aide de la commande demandée

#### Arguments
1. `cmdStr` *(String)*: Nom de la commande dont on cherche l'aide (si aucun nom ne correspond, affichera l'aide par défaut

---

<!-- /div -->

<!-- div -->

<h3 id="newTable"><a href="#newTable">#</a>&nbsp;<code>newTable([options], [style])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L90 "View in source") [&#x24C9;][1]

Renvoit un nouveau Table

#### Arguments
1. `[options]` *(Array)*: Options du tableau
2. `[style]` *(String): Fait appel directement à un style &#42;(`noBorder`)*&#42;

---

<!-- /div -->

<!-- div -->

<h3 id="run"><a href="#run">#</a>&nbsp;<code>run([argv])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L164 "View in source") [&#x24C9;][1]

Lance l'application en parsant les arguments de la ligne de commande.
Pour accéder à l'aide, plusieurs manières :<br>
<br> * aide spécifique à une commande prog help <nomCommand>
<br> * aide générale prog help prog --help prog -h ou si la commande demandée n'existe pas

#### Arguments
1. `[argv]` *(Array)*: Arguments à traiter, prendra ceux de la ligne de commande si le paramètre n'est pas défini.

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
