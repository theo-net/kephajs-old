# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#isOptional">`isOptional`</a>
* <a href="#isRequired">`isRequired`</a>
* <a href="#isVariadic">`isVariadic`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(synopsis, description, [validator], [defaultValue], application)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L21 "View in source") [&#x24C9;][1]



#### Arguments
1. `synopsis` *(String)*: Synopsis de l'argument/de l'option Si commence par &#91;, l'argument sera optionel. Si vaut ... alors correspond à un nombre variable de valeurs
2. `description` *(String)*: Description de l'option/ de l'argument
3. `[validator]` *(Function|Number|RegExp|String)*: Validateur
4. `[defaultValue]` *(&#42;)*: Valeur par défaut
5. `application` *(Application)*: Instance du programme

---

<!-- /div -->

<!-- div -->

<h3 id="isOptional"><a href="#isOptional">#</a>&nbsp;<code>isOptional()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L61 "View in source") [&#x24C9;][1]

Indique si l'argument est optionnel

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="isRequired"><a href="#isRequired">#</a>&nbsp;<code>isRequired()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L41 "View in source") [&#x24C9;][1]

Indique si l'argument est requis

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="isVariadic"><a href="#isVariadic">#</a>&nbsp;<code>isVariadic()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L51 "View in source") [&#x24C9;][1]

Indique si l'argument peut posséder un nombre variable de valeur

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
