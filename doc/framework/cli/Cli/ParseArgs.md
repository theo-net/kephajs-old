# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#parse">`parse`</a>
* <a href="#setOpts">`setOpts`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="parse"><a href="#parse">#</a>&nbsp;<code>parse(args)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L55 "View in source") [&#x24C9;][1]

Parse des arguments

#### Arguments
1. `args` *(Array)*: Arguments

---

<!-- /div -->

<!-- div -->

<h3 id="setOpts"><a href="#setOpts">#</a>&nbsp;<code>setOpts(opts)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L40 "View in source") [&#x24C9;][1]

Définie des options, écrase toutes les options déjà enregistrées.

#### Arguments
1. `opts` *(Object)*: Options à ajouter

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
