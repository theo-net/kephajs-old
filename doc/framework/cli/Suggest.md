# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#getBoldDiffString">`getBoldDiffString`</a>
* <a href="#getSuggestions">`getSuggestions`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="getBoldDiffString"><a href="#getBoldDiffString">#</a>&nbsp;<code>getBoldDiffString(from, to)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L37 "View in source") [&#x24C9;][1]

Returne la différence, entre deux chaînes de caractères, en gras.

#### Arguments
1. `from` *(String)*: Chaîne d'origine
2. `to` *(String)*: Chaîne de destination

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="getSuggestions"><a href="#getSuggestions">#</a>&nbsp;<code>getSuggestions(input, possibilities)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L18 "View in source") [&#x24C9;][1]

À partir  d'une liste de possibilités, suggère les éléments correspondant
à l'`input` *(avec une distance de Levenshtein <= `2`)*.

#### Arguments
1. `input` *(String)*: Entrée pour laquelle on cherche des suggestions
2. `possibilities` *(String&#91;&#93;)*: Possibilités

#### Returns
*(Array)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
