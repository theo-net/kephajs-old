# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#$addClass">`$addClass`</a>
* <a href="#$observe">`$observe`</a>
* <a href="#$removeClass">`$removeClass`</a>
* <a href="#$set">`$set`</a>
* <a href="#$updateClass">`$updateClass`</a>
* <a href="#PREFIX_REGEXP">`PREFIX_REGEXP`</a>
* <a href="#_addDirective">`_addDirective`</a>
* <a href="#_collectDirectives">`_collectDirectives`</a>
* <a href="#byPriority">`byPriority`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#directive">`directive`</a>
* <a href="#get">`get`</a>
* <a href="#has">`has`</a>
* <a href="#initializeDirectiveBindings">`initializeDirectiveBindings`</a>
* <a href="#isBooleanAttribute">`isBooleanAttribute`</a>
* <a href="#nodeName">`nodeName`</a>
* <a href="#run">`run`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="$addClass"><a href="#$addClass">#</a>&nbsp;<code>$addClass(classVal)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1252 "View in source") [&#x24C9;][1]

Ajoute la classe classVal à l'élément.

#### Arguments
1. `classVal` *(String)*: Class à ajouter

---

<!-- /div -->

<!-- div -->

<h3 id="$observe"><a href="#$observe">#</a>&nbsp;<code>$observe(key, fn)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1223 "View in source") [&#x24C9;][1]

Enregistre l'observeur `fn` sur l'attribut `key`. À chaque fois que `$set`
sera appelé sur cet attribut, `fn(value)` avec la nouvelle valeur sera
exécutée.
Par rapport à une valeur observée par `$watch` *(cf Scope)*, on exécute pas
de test à chaque digest, on économise donc du CPU. Cependant, si la
valeur d'un attribut est modifié en dehors de `$set`, l'observateur ne
sera pas informé : c'est la limite par rapport à `$watch`.
Un observer est appelé au prochain `$digest` après sa déclaration.
La fonction retourne une fonction de désenregistrement qui permet de
détruire l'observeur lorsqu'elle est appelée.

#### Arguments
1. `key` *(String)*: Attribut à observer
2. `fn` *(Function)*: Observer

#### Returns
*(Function)*:

---

<!-- /div -->

<!-- div -->

<h3 id="$removeClass"><a href="#$removeClass">#</a>&nbsp;<code>$removeClass(classVal)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1262 "View in source") [&#x24C9;][1]

Retire la classe classVal de l'élément.

#### Arguments
1. `classVal` *(String)*: Class à retirer

---

<!-- /div -->

<!-- div -->

<h3 id="$set"><a href="#$set">#</a>&nbsp;<code>$set(key, value, writeAttr, attrName)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1177 "View in source") [&#x24C9;][1]

Définie un attribue

#### Arguments
1. `key` *(String)*: Nom
2. `value` *(&#42;)*: valeur
3. `writeAttr` *(Boolean)*: Écrit la valeur de l'attribut dans l'élément
4. `attrName` *(String)*: Nom de l'attribut dans l'élément

---

<!-- /div -->

<!-- div -->

<h3 id="$updateClass"><a href="#$updateClass">#</a>&nbsp;<code>$updateClass(newClassVal, oldClassVal)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1277 "View in source") [&#x24C9;][1]

Met à jour les classes de l'élément :<br>
<br> * ajoute toutes les classes présentes dans newClassVal mais pas dans oldClassVal
<br> * retire toutes les classes présentes dans oldClassVal mais pas dans newClassVal

#### Arguments
1. `newClassVal` *(Array)*: classes à ajouter
2. `oldClassVal` *(Array)*: classes à supprimer

---

<!-- /div -->

<!-- div -->

<h3 id="PREFIX_REGEXP"><a href="#PREFIX_REGEXP">#</a>&nbsp;<code>PREFIX_REGEXP(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1351 "View in source") [&#x24C9;][1]

Normalise le nom de la directive name

#### Arguments
1. `name` *(String)*: Nom de la directive à normaliser

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="_addDirective"><a href="#_addDirective">#</a>&nbsp;<code>_addDirective(directives, name, mode, maxPriority, attrStartName, attrEndName)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L409 "View in source") [&#x24C9;][1]

Reçoit une liste de directives *(array directives)* et le nom d'une
directive pour vérifier si le tableau local this._directives possède des
directives avec le nom name. S'il en trouve, les directives
correspondantes seront obtenues à et ajoutée au tableau directives.
Mais la méthode regarde aussi la propriété restrict : la directive ne
sera ajouté que si celle-ci correspind au mode *(E, A)*
Retourne une valeur indiquant si une directive a été ajoutée ou non

#### Arguments
1. `directives` *(Object)*: Listes des directives
2. `name` *(String)*: Nom de la directive
3. `mode` *(&#42;)*: ?
4. `maxPriority` *(Integer)*: ?
5. `attrStartName` *(String)*: Nom de l'attribut du premier élément
6. `attrEndName` *(String)*: Nom de l'attribut du dernier élément

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="_collectDirectives"><a href="#_collectDirectives">#</a>&nbsp;<code>_collectDirectives(node, attrs, maxPriority)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L324 "View in source") [&#x24C9;][1]

Applique les directives à node et récupère les attributs.

#### Arguments
1. `node` *(Node)*: Nœud DOM
2. `attrs` *(Object)*: Attributs
3. `maxPriority` *(&#42;)*: ?

#### Returns
*(Array)*:

---

<!-- /div -->

<!-- div -->

<h3 id="byPriority"><a href="#byPriority">#</a>&nbsp;<code>byPriority(a, b)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1366 "View in source") [&#x24C9;][1]

Fonction de trie selon l'attribut priority des directives a et b ou selon
l'attribut name si les priorités sont identiques. Enfin, on prendra l'index
si le reste est identique.

#### Arguments
1. `a` *(Object)*: Directive a
2. `b` *(Object)*: Directive b

#### Returns
*(Integer)*:

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(element, rootScope)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1162 "View in source") [&#x24C9;][1]

Initilise l'objet

#### Arguments
1. `element` *(Node)*: Élément
2. `rootScope` *(Scope)*: Scope principal

---

<!-- /div -->

<!-- div -->

<h3 id="directive"><a href="#directive">#</a>&nbsp;<code>directive(name, directiveFactory)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L72 "View in source") [&#x24C9;][1]

Enregistre la directive `name` que retourne `directiveFactory`.
Plusieurs directives peuvent avoir le même nom, mais elles ne peuvent pas
s'appeler hasOwnProperty.
Si un objet est transmis à la place de `name`, la méthode créera une série
de directives ayant pour nom les propriétés de l'objet et pour factory
les valeurs correspondantes.
La directiveFactory est une fonction, prenant comme paramètre le service
compile *(que définit notre classe)*, retournant un  objet avec les
propriétés suivantes :<br>
<br> * `compile`: `function (element)` fonction appelée sur l'élément lors de la compilation. Celle-ci retourne une fonction qui sera appellée pour le linkage `link(scope, element, attrs, ctrl, transclude)`
<br> * `link`: Fonction de linkage appelée si `compile` n'est pas défini.
<br> * `restrict`: restriction de déclaration *(peuvent être combinés)* La valeur par défaut est `EA` `E` element name `A` attribute name
<br> * `priority`: vaut `0` par défaut. Indique la priorité, plus le nombre est élevé, plus celle-ci est forte.
<br> * `name`: nom de la directive. Si cet attribu n'est pas donné, name vaudra le nom `name` donné à la fonction.
<br> * `index`: attribué automatiquement, correspond à l'index d'enregistrement
<br> * `terminal`: si vaut true, les éléments enfants ne seront pas compilé après la rencontre de la directive. Si un élément possède d'autres directives, elles ne seront compilées que si leur priorité est supérieure ou égale.
<br> * `multiElement`: directive de type multi-elements. Le restrict vaut `A` d'office. `<div my-dir-start></div><span /><div my-dir-end></div>`
<br> * `scope`: si cet attribut vaut true, alors un nouveau scope *(héritant du $rootScope) sera lié à l'élément et toutes les directives attachées à cet élément recevront ce nouveau scope. L'élément se verra ajouté une nouvelle classe (`kjs-scope`) et le scope sera lié à l'élément (`el.$scope`). Si cet attribut est un objet, un scope isolé sera créé (cf Scope)*, celui-ci ne sera pas partagé avec les autres directives, ni avec les éléments enfants sauf si ces derniers ont été créés par la directive durant son exécution. Si une propriété de l'objet à pour valeur '@', alors celle-ci sera bindée avec l'attribut de même nom.
<br> * `require`: ??

#### Arguments
1. `name` *(String)*: Nom de la directive
2. `directiveFactory` *(Function)*: Fonction permettant de créer la directive

---

<!-- /div -->

<!-- div -->

<h3 id="get"><a href="#get">#</a>&nbsp;<code>get(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L139 "View in source") [&#x24C9;][1]

Retourne des directives

#### Arguments
1. `name` *(String)*: Nom de la directive

#### Returns
*(Array)*:

---

<!-- /div -->

<!-- div -->

<h3 id="has"><a href="#has">#</a>&nbsp;<code>has(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L128 "View in source") [&#x24C9;][1]

Indique si une directive est enregistrée

#### Arguments
1. `name` *(String)*: Nom de la directive

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="initializeDirectiveBindings"><a href="#initializeDirectiveBindings">#</a>&nbsp;<code>initializeDirectiveBindings(scope, attrs, destination, bindings, newScope)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L738 "View in source") [&#x24C9;][1]

Initialise le binding des directives

#### Arguments
1. `scope` *(Scope)*: Le scope
2. `attrs` *(Object)*: Attributs
3. `destination` *(&#42;)*: ?
4. `bindings` *(&#42;)*: ?
5. `newScope` *(&#42;)*: ?

---

<!-- /div -->

<!-- div -->

<h3 id="isBooleanAttribute"><a href="#isBooleanAttribute">#</a>&nbsp;<code>isBooleanAttribute(node, attrName)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1329 "View in source") [&#x24C9;][1]

Indique si l'attribut attrName de l'élément node est un attribut de type
boollean.

#### Arguments
1. `node` *(Node)*: Node
2. `attrName` *(String)*: Nom de l'attribut

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="nodeName"><a href="#nodeName">#</a>&nbsp;<code>nodeName(element)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L1340 "View in source") [&#x24C9;][1]

Retourne le nom du nœud DOM element

#### Arguments
1. `element` *(Object)*: Élement DOM

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="run"><a href="#run">#</a>&nbsp;<code>run(compileNodes, maxPriority)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L167 "View in source") [&#x24C9;][1]

Lance la compilation pour `compileNodes`.
Elle retourne une fonction qui permettra d'établir le lien entre les
directives et le scope. `publicLinkFn (scope)` Lance la function link retournée par chaque méthode compile() des directives. On link en premier les éléments enfants. Par défaut il s'agit d'une post-linking function, mais on peut changer cela en retournant, au lieu d'une function, un objet avec comme propriété(s) : `post` ou `pre` indiquant à quel moment à lieu le linkage et comme valeur la fonction. Post-linkage : la fonction est exécuté après celles des éléments enfants. Pre-linkage : la fonction est exécuté avant celles des éléments enfants.

#### Arguments
1. `compileNodes` *(DOM)*: Nœuds DOM
2. `maxPriority` *(Integer)*: ?

#### Returns
*(Function)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
