# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Array`
* <a href="#static remove ">`static remove `</a>

<!-- /div -->

<!-- div -->

## `Collection`
* <a href="#static forEach ">`static forEach `</a>
* <a href="#static forEachRight ">`static forEachRight `</a>
* <a href="#static some ">`static some `</a>

<!-- /div -->

<!-- div -->

## `Lang`
* <a href="#static clone ">`static clone `</a>
* <a href="#static isEqual ">`static isEqual `</a>
* <a href="#static isNumber ">`static isNumber `</a>

<!-- /div -->

<!-- div -->

## `Object`
* <a href="#static forOwn ">`static forOwn `</a>
* <a href="#static transform ">`static transform `</a>

<!-- /div -->

<!-- div -->

## `Util`
* <a href="#static times ">`static times `</a>

<!-- /div -->

<!-- div -->

## `Methods`
* <a href="#static extend ">`static extend `</a>
* <a href="#static levenshtein ">`static levenshtein `</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `“Array” Methods`

<!-- div -->

<h3 id="static remove "><a href="#static remove">#</a>&nbsp;<code>static remove *(array(array, predicate, [thisArg])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L994 "View in source") [&#x24C9;][1]

Suprimmes tous les éléments d'array pour lesquels predicate retourne true
et retourne un tableau des éléments supprimés. predicate est bindé avec
thisArg et appelé avec *(value, index, array)*.

#### Arguments
1. `array` *(Array)*: Tableau
2. `predicate` *(Function|Object)*: Fonction déterminant les entrées à retirer. S'il s'agit d'un objet :
3. `[thisArg]` *(&#42;)*: Context

#### Returns
*(Array)*:

---

<!-- /div -->

<!-- /div -->

<!-- div -->

## `“Collection” Methods`

<!-- div -->

<h3 id="static forEach "><a href="#static forEach">#</a>&nbsp;<code>static forEach *(collection(collection, fn, [thisArg])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L406 "View in source") [&#x24C9;][1]

Appelle fn sur chacun des éléments de la collection.
Si fn renvoit false, stop le forEach

#### Arguments
1. `collection` *(Array|Object)*: Collection
2. `fn` *(Function): Fonction appelée fn(value, index, collection)*
3. `[thisArg]` *(&#42;)*: Contexte

#### Returns
*(&#42;)*: Retourne `collection`

---

<!-- /div -->

<!-- div -->

<h3 id="static forEachRight "><a href="#static forEachRight">#</a>&nbsp;<code>static forEachRight *(collection(collection, fn, [thisArg])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L453 "View in source") [&#x24C9;][1]

Appelle fn sur chacun des éléments de la collection, mais en la parcourant
à partir de la fin. Si fn renvoit false, stop le forEach

#### Arguments
1. `collection` *(Array|Object)*: Collection
2. `fn` *(Function)*: Fontion appellée
3. `[thisArg]` *(&#42;)*: Context

#### Returns
*(&#42;)*: Retourne `collection`

---

<!-- /div -->

<!-- div -->

<h3 id="static some "><a href="#static some">#</a>&nbsp;<code>static some *(collection(collection, predicate, [thisArg])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L897 "View in source") [&#x24C9;][1]

Exécute `callback` sur chacun des éléments. Retourne true, si chaque appel
de callback a retourné true, false sinon.
predicate *(currentValue, index, collection)*

#### Arguments
1. `collection` *(Array|Object)*: Collection
2. `predicate` *(Function)*: Function appelée sur chaque élément. Si non définie : value => { return value ; }
3. `[thisArg]` *(&#42;)*: this binding

#### Returns
*(boolean)*:

---

<!-- /div -->

<!-- /div -->

<!-- div -->

## `“Lang” Methods`

<!-- div -->

<h3 id="static clone "><a href="#static clone">#</a>&nbsp;<code>static clone *(value(value, [isDeep=false], [key], [object], [stackA=[]], [stackB=[]])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L715 "View in source") [&#x24C9;][1]

Créé un clone de `value` et le retourne. Si `isDeep` vaut `true, les
propriétés ayant pour valeur un objet sont également clonée et non passées
par référence. Les propriétés d'`arguments` ou les objets créés par un
constructeur autre que `Object` sont clonés comme des objets `Objects`. Un
objet vide est retourné pour les valeurs non clonables (comme les
fonctions, nœud DOM, ...)

#### Arguments
1. `value` *(&#42;)*: Valeur à cloner
2. `[isDeep=false]` *(Boolean)*: Deep clone
3. `[key]` *(String)*: The key of `value`
4. `[object]` *(Object)*: The object `value ` belongs to.
5. `[stackA=[]]` *(Array)*: Tracks traversed source objects.
6. `[stackB=[]]` *(Array)*: Associates clones with source counterparts

#### Returns
*(&#42;)*:

---

<!-- /div -->

<!-- div -->

<h3 id="static isEqual "><a href="#static isEqual">#</a>&nbsp;<code>static isEqual *(value(value, other, [customizer], [thisArg], [stackA=[]], [stackB=[]])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L295 "View in source") [&#x24C9;][1]

Compare value et other pour déterminer s'ils sont équivalents. Fonctionne
avec des arrays, booleans, Date objects, nombres, Object objects, regexp
et string. Pour cette fonction, NaN est égale à lui-même.

#### Arguments
1. `value` *(&#42;)*: valeur à comparer
2. `other` *(&#42;)*: l'autre valeur à comparer
3. `[customizer]` *(Function)*: fonction de comparaison
4. `[thisArg]` *(&#42;)*: binding pour customizer
5. `[stackA=[]]` *(&#42;)*: Pour détecter la récursivité
6. `[stackB=[]]` *(&#42;)*: Pour détecter la récursivité

#### Returns
*(boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="static isNumber "><a href="#static isNumber">#</a>&nbsp;<code>static isNumber *(value(value, [convert=false])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L119 "View in source") [&#x24C9;][1]

`value` est-il un nombre ?

#### Arguments
1. `value` *(&#42;)*: valeur à tester
2. `[convert=false]` *(Boolean)*: Si `true` `'123'` sera un nombre

---

<!-- /div -->

<!-- /div -->

<!-- div -->

## `“Object” Methods`

<!-- div -->

<h3 id="static forOwn "><a href="#static forOwn">#</a>&nbsp;<code>static forOwn *(obj(obj, fn, [thisArg])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L861 "View in source") [&#x24C9;][1]

Parcours les propriétés de l'objet.

#### Arguments
1. `obj` *(Object)*: Objet à parcourir
2. `fn` *(Function)*: Function callback
3. `[thisArg]` *(&#42;)*: Context

#### Returns
*(Object)*: Retourne `obj`

---

<!-- /div -->

<!-- div -->

<h3 id="static transform "><a href="#static transform">#</a>&nbsp;<code>static transform *(object(object, callback, [accumulator={}], [thisArg])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L949 "View in source") [&#x24C9;][1]

Transforme `object` en un nouveau `accumulator`, chaque propriété a été
transformée par `callback`. S'arrête si une `callback` retourne
explicitement `false`

#### Arguments
1. `object` *(Object)*: Objet
2. `callback` *(Function)*: Fonction callback
3. `[accumulator={}]` *(Object)*: Accumulator
4. `[thisArg]` *(&#42;)*: this binding

#### Returns
*(Object)*:

---

<!-- /div -->

<!-- /div -->

<!-- div -->

## `“Util” Methods`

<!-- div -->

<h3 id="static times "><a href="#static times">#</a>&nbsp;<code>static times *(n(n, [callback], [thisArg])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L256 "View in source") [&#x24C9;][1]

Invoque `n` fois `callback`

#### Arguments
1. `n` *(number)*: nombre de fois que la fonction callback doit être exécutée
2. `[callback]` *(Function): &#42;(i) : fonction appelée avec i qui varie de `0` à n-1, si n'est pas définie ou n'est pas une fonction, remplace par une fonction renvoyant le paramètre (identity)*&#42;
3. `[thisArg]` *(&#42;)*: this binding

#### Returns
*(Array)*: Tableau des résultats

---

<!-- /div -->

<!-- /div -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="static extend "><a href="#static extend">#</a>&nbsp;<code>static extend *(object(object, sources)*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L498 "View in source") [&#x24C9;][1]

Assigne les propriétés énumérables des objets sources à `object`. Si la
propriété existe déjà, elle sera écrasée. De même, une source écrase les
propriétés d'une source précédente.

#### Arguments
1. `object` *(Object)*: Objet qui sera étendus
2. `sources` *(source)*: Les sources

---

<!-- /div -->

<!-- div -->

<h3 id="static levenshtein "><a href="#static levenshtein">#</a>&nbsp;<code>static levenshtein *(str1(str1, str2, [collator=false])*</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L623 "View in source") [&#x24C9;][1]

Calcul la distance de Levenshtein donnant une mesure de similarité entre
deux chaînes de caractères.

#### Arguments
1. `str1` *(String)*: Première chaîne à tester
2. `str2` *(String)*: Deuxième chaîne à tester
3. `[collator=false]` *(Boolean)*: Utilise `IntlCollator` pour une comparaison locale-sensitive

#### Returns
*(Integer)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #array "Jump back to the TOC."
