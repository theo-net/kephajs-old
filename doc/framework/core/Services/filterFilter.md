# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#createPredicateFn">`createPredicateFn`</a>
* <a href="#filterFilter">`filterFilter`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="createPredicateFn"><a href="#createPredicateFn">#</a>&nbsp;<code>createPredicateFn(expression, comparator)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L32 "View in source") [&#x24C9;][1]

createPredicateFn *(expression, comparator)*
<br>
<br>
Créé la fonction de comparaison qui est appliqué au tableau.
Si comparator vaut true, alors la fonction utilisée sera Utils.isEqual,
si comparator est une fonction, alors on utilisera celle-ci.

#### Arguments
1. `expression` *(String)*: Expression
2. `comparator` *(Boolean|Function)*: Si vaut `true`, fonction de comparaison on utilise Utils.isEqal

#### Returns
*(Function)*:

---

<!-- /div -->

<!-- div -->

<h3 id="filterFilter"><a href="#filterFilter">#</a>&nbsp;<code>filterFilter(array, filterExpr, comparator)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L18 "View in source") [&#x24C9;][1]

Est la fonction exportée. Elle retourne le filtre 'filter' qui filtre un
tableau. filter:filterExpr:comparator

#### Arguments
1. `array` *(Array)*: Tableau à filtrer
2. `filterExpr` *(String)*: expression qui sert de filtre
3. `comparator` *(Function)*: fonction de comparaison

#### Returns
*(Array)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
