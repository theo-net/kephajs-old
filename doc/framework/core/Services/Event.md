# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#attach">`attach`</a>
* <a href="#constructor">`constructor`</a>
* <a href="#detach">`detach`</a>
* <a href="#notify">`notify`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="attach"><a href="#attach">#</a>&nbsp;<code>attach(id, callback)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L30 "View in source") [&#x24C9;][1]

Attache un observer, écrasera un précédent portant le même nom.

#### Arguments
1. `id` *(String)*: Identifiant, nécessaire pour désenregister l'observer
2. `callback` *(function)*: Fonction de callback à exécuter

---

<!-- /div -->

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L18 "View in source") [&#x24C9;][1]

Initialise le canal

#### Arguments
1. `id` *(String)*: Identifiant du canal

---

<!-- /div -->

<!-- div -->

<h3 id="detach"><a href="#detach">#</a>&nbsp;<code>detach(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L40 "View in source") [&#x24C9;][1]

Détache un observer

#### Arguments
1. `id` *(String)*: Identifiant de l'observer à détacher

---

<!-- /div -->

<!-- div -->

<h3 id="notify"><a href="#notify">#</a>&nbsp;<code>notify(data)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L50 "View in source") [&#x24C9;][1]

Notifie tous les observers

#### Arguments
1. `data` *(&#42;)*: Données à transmettre

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
