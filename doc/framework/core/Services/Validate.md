# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#getWithName">`getWithName`</a>
* <a href="#make">`make`</a>
* <a href="#setValidator">`setValidator`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="getWithName"><a href="#getWithName">#</a>&nbsp;<code>getWithName(name, [args])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L65 "View in source") [&#x24C9;][1]

Retourne un nouveau validateur à partir de son nom

#### Arguments
1. `name` *(String)*: Nom du validateur
2. `[args]` *(Object)*: Arguments

#### Returns
*(Validator)*:

---

<!-- /div -->

<!-- div -->

<h3 id="make"><a href="#make">#</a>&nbsp;<code>make(validator, args)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L49 "View in source") [&#x24C9;][1]

Construit un nouveau validateur

#### Arguments
1. `validator` *(Function|RegExp|String): Voir ValidateValidator.add()*
2. `args` *(Object|String): Voir ValidateValidator.add()*

#### Returns
*(Validator)*:

---

<!-- /div -->

<!-- div -->

<h3 id="setValidator"><a href="#setValidator">#</a>&nbsp;<code>setValidator(name, validator)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L90 "View in source") [&#x24C9;][1]

Définit un nouveau validateur
<br>
<br> validator = { class: require('..'),   // créer à partir d'une class object: new *Validator  // on s'occupe nous-même de la création }

#### Arguments
1. `name` *(String)*: Nom du validateur
2. `validator` *(Object)*: Description du validateur

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
