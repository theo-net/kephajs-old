# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#attach">`attach`</a>
* <a href="#has">`has`</a>
* <a href="#notify">`notify`</a>
* <a href="#register">`register`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="attach"><a href="#attach">#</a>&nbsp;<code>attach(canal, id, callback)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L42 "View in source") [&#x24C9;][1]

Attache un callback à un canal

#### Arguments
1. `canal` *(String)*: Identifiant du canal
2. `id` *(String)*: Identifiant de l'observer canal
3. `callback` *(Function)*: Le callback

---

<!-- /div -->

<!-- div -->

<h3 id="has"><a href="#has">#</a>&nbsp;<code>has(canal)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L66 "View in source") [&#x24C9;][1]

Indique si un canal est enregistré

#### Arguments
1. `canal` *(String)*: Identidiant du canal à tester

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="notify"><a href="#notify">#</a>&nbsp;<code>notify(canal, data)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L54 "View in source") [&#x24C9;][1]

Notifie un évènement

#### Arguments
1. `canal` *(String)*: Identifiant du canal à notifier
2. `data` *(&#42;)*: Données à transmettre

---

<!-- /div -->

<!-- div -->

<h3 id="register"><a href="#register">#</a>&nbsp;<code>register(id)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L30 "View in source") [&#x24C9;][1]

Enregistre un nouveau canal d'évènement. Si l'id est déjà défini, on
effacera l'accès au canal précédent.

#### Arguments
1. `id` *(String)*: Identifiant du canal

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
