# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#_getLogLevel">`_getLogLevel`</a>
* <a href="#log">`log`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="_getLogLevel"><a href="#_getLogLevel">#</a>&nbsp;<code>_getLogLevel()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L75 "View in source") [&#x24C9;][1]

Retourne le logLevel

#### Returns
*(String)*:

---

<!-- /div -->

<!-- div -->

<h3 id="log"><a href="#log">#</a>&nbsp;<code>log(message, [type='info'])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L35 "View in source") [&#x24C9;][1]

Traite un message de log

#### Arguments
1. `message` *(String)*: Message
2. `[type='info']` *(String): Type &#42;(`info`, `success`, `warning`, `error`, `debug`)*&#42;

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
