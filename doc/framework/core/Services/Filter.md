# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#get">`get`</a>
* <a href="#has">`has`</a>
* <a href="#register">`register`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="get"><a href="#get">#</a>&nbsp;<code>get(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L51 "View in source") [&#x24C9;][1]

Retourne un filtre

#### Arguments
1. `name` *(String)*: Nom du filtre

#### Returns
*(Function)*:

---

<!-- /div -->

<!-- div -->

<h3 id="has"><a href="#has">#</a>&nbsp;<code>has(name)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L27 "View in source") [&#x24C9;][1]

Indique si un filtre est enregistré

#### Arguments
1. `name` *(String)*: Nom du filtre

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="register"><a href="#register">#</a>&nbsp;<code>register(name, filter)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L40 "View in source") [&#x24C9;][1]

Enregistre le filtre.
ATTENTION : `filter` doit être de la forme `function () {}` et non de la forme `() => {}` pour que le parser puisse fonctionner.

#### Arguments
1. `name` *(String)*: Nom du filtre
2. `filter` *(Function)*: Filtre à enregistrer

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
