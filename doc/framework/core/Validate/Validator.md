# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#constructor">`constructor`</a>
* <a href="#getOccurredErrors">`getOccurredErrors`</a>
* <a href="#hasError">`hasError`</a>
* <a href="#isValid">`isValid`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="constructor"><a href="#constructor">#</a>&nbsp;<code>constructor([args={}])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L31 "View in source") [&#x24C9;][1]

Construit le validateur

#### Arguments
1. `[args={}]` *(Object)*: Arguments nécessaires à la construction du validateur

---

<!-- /div -->

<!-- div -->

<h3 id="getOccurredErrors"><a href="#getOccurredErrors">#</a>&nbsp;<code>getOccurredErrors()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L62 "View in source") [&#x24C9;][1]

Renvoit la liste des erreurs si la validation ne passe pas, un tableau
vide sinon.

#### Returns
*(Array)*:

---

<!-- /div -->

<!-- div -->

<h3 id="hasError"><a href="#hasError">#</a>&nbsp;<code>hasError()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L51 "View in source") [&#x24C9;][1]

Regarde si une valeur a été détectée

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- div -->

<h3 id="isValid"><a href="#isValid">#</a>&nbsp;<code>isValid([value=null])</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L73 "View in source") [&#x24C9;][1]

À chaque exécution de la méthode, la liste des erreurs est réinitialisée

#### Arguments
1. `[value=null]` *(&#42;)*: Valeur à tester

#### Returns
*(Boolean)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
