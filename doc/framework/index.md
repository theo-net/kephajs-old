# <a href="https://kepha.theo-net.org/kephajs">KephaJs</a> <span>v0.7.0</span>

<!-- div class="toc-container" -->

<!-- div -->

## `Methods`
* <a href="#cli">`cli`</a>
* <a href="#server">`server`</a>

<!-- /div -->

<!-- /div -->

<!-- div class="doc-container" -->

<!-- div -->

## `Methods`

<!-- div -->

<h3 id="cli"><a href="#cli">#</a>&nbsp;<code>cli()</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L31 "View in source") [&#x24C9;][1]

Créé une nouvelle application CLI

#### Returns
*(Cli)*:

---

<!-- /div -->

<!-- div -->

<h3 id="server"><a href="#server">#</a>&nbsp;<code>server(staticPath)</code></h3>
[&#x24C8;](https//framagit.com/theonet/kephajs/blob/0.7.0/kephajs.js#L43 "View in source") [&#x24C9;][1]

Créé un nouveau serveur

#### Arguments
1. `staticPath` *(String)*: Répertoire de fichiers statiques

#### Returns
*(Server)*:

---

<!-- /div -->

<!-- /div -->

<!-- /div -->

 [1]: #methods "Jump back to the TOC."
