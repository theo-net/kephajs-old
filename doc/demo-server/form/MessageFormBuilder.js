'use strict' ;

const FormBuilder = require('../../../').FormBuilder ;

class MessageFormBuilder extends FormBuilder {

  build () {

    this.a = 'ok' ;

    this.getForm()
      .add(
        'string', {
          label: 'Titre du message',
          name: 'titre',
          required: true
        })
      .add(
        'string', {
          label: 'E-Mail de l\'auteur',
          name: 'auteur'
        }, 'mail')
      .add(
        'text', {
          label: 'Message',
          name: 'message',
          rows: 8,
          cols: 60,
          required: true
        })
      .add(
        'submit', {
          name: 'submit',
          value: 'Envoyer'
        }) ;
  }
}

module.exports = MessageFormBuilder ;

